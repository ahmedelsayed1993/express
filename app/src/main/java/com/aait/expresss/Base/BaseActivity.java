package com.aait.expresss.Base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (fullScreen()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(getLayoutRes());

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //TODO SET LOCALE
        //setLocale(GlobalPreference.getInstance(this).getAppLanguage());

        if (hideInputType()){
            hideInputtype();

        }
        init(savedInstanceState);



    }
    protected abstract void init (Bundle savedInstanceState);
    protected abstract int getLayoutRes();


    protected abstract boolean fullScreen();
    protected abstract boolean hideInputType();
    protected abstract void startNotificationActivity();
    protected abstract void startSearchActivity();
    protected abstract void navigate();


    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(Utils.changeLang(new GlobalPreference(newBase).getAppLanguage(),newBase));
    }

    public void hideInputtype() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            assert inputMethodManager != null;
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

    }
}
