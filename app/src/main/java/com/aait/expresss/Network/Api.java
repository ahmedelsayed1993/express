package com.aait.expresss.Network;

import com.aait.expresss.Models.ActivateCodeModel;
import com.aait.expresss.Models.AllDataModel;
import com.aait.expresss.Models.CarAddUpdateModel;
import com.aait.expresss.Models.CarsModel;
import com.aait.expresss.Models.ChangePasswordModel;
import com.aait.expresss.Models.CheckPhoneModel;
import com.aait.expresss.Models.ConditionsModel;
import com.aait.expresss.Models.ContactInfoModel;
import com.aait.expresss.Models.FollowOrderModel;
import com.aait.expresss.Models.LocationResponse.LocationResponse;
import com.aait.expresss.Models.LoginModel;
import com.aait.expresss.Models.LogoutModel;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Models.NewOrderModel;
import com.aait.expresss.Models.NewOrderShowModel;
import com.aait.expresss.Models.OptionalServiceModel;
import com.aait.expresss.Models.OrderNowModel;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.Models.SendContactModel;
import com.aait.expresss.Models.SendNotificationModel;
import com.aait.expresss.Models.SetNewPasswordModel;
import com.aait.expresss.Models.TransferModel;
import com.aait.expresss.Models.UserSettingsModel;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {
    @GET("geocode/json?sensor=true")
    Call<LocationResponse> getLocation(
            @Query("latlng") String lng,
            @Query("language") String language
    );

    @FormUrlEncoded
    @POST("condition")
    Call<ConditionsModel> getConditions(@Field("lang") String lang);

    @FormUrlEncoded
    @POST("register")
    Call<LoginModel> register(@Field("name") String name,
                              @Field("phone") String phone,
                              @Field("email") String email,
                              @Field("password") String password,
                              @Field("delegate") int isDelegate,
                              @Field("address") String address,
                              @Field("lat") double lat,
                              @Field("lng") double lng,
                              @Field("lang") String lang);

    @GET("contact_info")
    Call<ContactInfoModel> getContacts();

    @FormUrlEncoded
    @POST("check_code")
    Call<ActivateCodeModel> activateAccount(@Field("user_id") int userId,
                                            @Field("code") String code,
                                            @Field("lang") String lang);

    @FormUrlEncoded
    @POST("check_phone")
    Call<CheckPhoneModel> checkPhone(@Field("phone") String phone,
                                     @Field("lang") String lang);

    @FormUrlEncoded
    @POST("Set_new_password")
    Call<SetNewPasswordModel> resetPassword(@Field("user_id") int userId,
                                            @Field("password") String password,
                                            @Field("lang") String lang);


    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> login(@Field("phone") String phone,
                           @Field("password") String password,
                           @Field("delegate") int isDelegate,
                           @Field("device_id") String deviceId,
                           @Field("lang") String lang,
                           @Field("device_type") int device_type);

    @FormUrlEncoded
    @POST("contact/send")
    Call<SendContactModel> sendMessageToUs(@Field("name") String name,
                                           @Field("email") String email,
                                           @Field("message") String message,
                                           @Field("lang") String lang);

    @FormUrlEncoded
    @POST("User_setting")
    Call<UserSettingsModel> settings(@Field("user_id") int userId,
                                     @Field("app_lang") String appLang,
                                     @Field("notifications") int notification,
                                     @Field("lang") String lang);

    @FormUrlEncoded
    @POST("User_new_password")
    Call<ChangePasswordModel> changePassword(@Field("user_id") int userId,
                                             @Field("old_password") String oldPass,
                                             @Field("new_password") String newPass,
                                             @Field("lang") String lang);

    @FormUrlEncoded
    @POST("delegate/orders/new/show")
    Call<OrdersModel> getDelegateOrders(@Field("delegate_id") int delegateId,
                                        @Field("lang") String lang);

    @FormUrlEncoded
    @POST("delegate/orders/finish/show")
    Call<OrdersModel> getDelegateOldOrders(@Field("delegate_id") int delegateId,
                                           @Field("lang") String lang);

    @FormUrlEncoded
    @POST("logout")
    Call<LogoutModel> logout(@Field("user_id") int userId,
                             @Field("lang") String lang);


    @FormUrlEncoded
    @POST("delegate/orders/new/refused")
    Call<MainResponseModel> refuseOrder(@Field("delegate_id") int delegateId,
                                        @Field("order_id") int orderId,
                                        @Field("reason") String reason,
                                        @Field("lang") String lang);

    @FormUrlEncoded
    @POST("delegate/orders/new/agree")
    Call<MainResponseModel> acceptOrder(@Field("delegate_id") int delegateId,
                                        @Field("order_id") int orderId,
                                        @Field("lang") String lang);

    @FormUrlEncoded
    @POST("delegate/order/follow")
    Call<FollowOrderModel> delegateFollowOrder(@Field("order_id") int orderId,
                                               @Field("lang") String lang);

    @FormUrlEncoded
    @POST("delegate/order/cancel")
    Call<MainResponseModel> cancelOrder(@Field("order_id") int orderId,
                                        @Field("reason") String reason,
                                        @Field("content") String content,
                                        @Field("lang") String lang);

    @FormUrlEncoded
    @POST("User_profile/update")
    Call<LoginModel> updateUserProfile(@Field("user_id") int userId,
                                       @Field("name") String name,
                                       @Field("phone") String phone,
                                       @Field("lat") double lat,
                                       @Field("lng") double lng,
                                       @Field("address") String address,
                                       @Field("avatar") String image,
                                       @Field("lang") String lang);

    @FormUrlEncoded
    @POST("orders/new/show")
    Call<NewOrderModel> newOrders(@Field("user_id") int userId,
                                  @Field("lang") String lang);

    @FormUrlEncoded
    @POST("cars/all")
    Call<CarsModel> getMyCars(@Field("user_id") int userId,
                              @Field("lang") String lang);

    @FormUrlEncoded
    @POST("data/all")
    Call<AllDataModel> getUtils(@Field("lang") String lang);

    @FormUrlEncoded
    @POST("car/store")
    Call<CarAddUpdateModel> addCar(@Field("user_id") int userId,
                                   @Field("brand_id") int brandId,
                                   @Field("model_car") String modelCar,
                                   @Field("number") String plateNumber,
                                   @Field("oil_id") int oilId,
                                   @Field("engine_id") int engineId,
                                   @Field("num_containers") String numContainers,
                                   @Field("viscosity") String viscosity,
                                   @Field("oil_change_dates") String oilChangeDates,
                                   @Field("lang") String lang);

    @FormUrlEncoded
    @POST("car/update")
    Call<CarAddUpdateModel> updateCar(@Field("car_id") int carId,
                                      @Field("brand_id") int brandId,
                                      @Field("model_car") String modelCar,
                                      @Field("number") String plateNumber,
                                      @Field("oil_id") int oilId,
                                      @Field("engine_id") int engineId,
                                      @Field("num_containers") String numContainers,
                                      @Field("viscosity") String viscosity,
                                      @Field("oil_change_dates") String oilChangeDates,
                                      @Field("lang") String lang);

    @FormUrlEncoded
    @POST("car/delete")
    Call<MainResponseModel> deleteCar(@Field("car_id") int carId,
                                      @Field("lang") String lang);


    @FormUrlEncoded
    @POST("orders/not_finish/show")
    Call<OrdersModel> currentOrders(@Field("user_id") int userId,
                                    @Field("lang") String lang);

    @FormUrlEncoded
    @POST("order/follow")
    Call<FollowOrderModel> clientFollowOrder(@Field("order_id") int orderId,
                                             @Field("lang") String lang);

    @FormUrlEncoded
    @POST("order/cancel")
    Call<MainResponseModel> clientCancelOrder(@Field("order_id") int orderId,
                                              @Field("reason") String reason,
                                              @Field("notes") String notes,
                                              @Field("lang") String lang);

    @FormUrlEncoded
    @POST("order/finish")
    Call<MainResponseModel> finishOrder(@Field("order_id") int orderId,
                                        @Field("lang") String lang);


    @FormUrlEncoded
    @POST("delegate/order/rate")
    Call<MainResponseModel> delegateRate(@Field("delegate_id") int delegateId,
                                         @Field("order_id") int orderId,
                                         @Field("rate") int rate,
                                         @Field("content") String content,
                                         @Field("lang") String lang);

    @FormUrlEncoded
    @POST("order/rate")
    Call<MainResponseModel> clientRate(@Field("user_id") int userId,
                                       @Field("order_id") int orderId,
                                       @Field("rate") int rate,
                                       @Field("content") String content,
                                       @Field("lang") String lang);

    @FormUrlEncoded
    @POST("orders/finish/show")
    Call<OrdersModel> getClientOldOrders(@Field("user_id") int userId,
                                         @Field("lang") String lang);

    @FormUrlEncoded
    @POST("orders/new/store")
    Call<OrderNowModel> storeNewOrder(@Field("user_id") int userId,
                                      @Field("car_id") String carId,
                                      @Field("lang") String lang);

    //late order
    @FormUrlEncoded
    @POST("orders/new/confirm")
    Call<MainResponseModel> confirmOrder(@Field("order_id") int orderId,
                                         @Field("order_date") String orderDate,
                                         @Field("order_time") String orderTime,
                                         @Field("lang") String lang);

    //order now
    @FormUrlEncoded
    @POST("orders/new/confirm")
    Call<MainResponseModel> confirmOrder(@Field("order_id") int orderId,
                                         @Field("lang") String lang);

    @FormUrlEncoded
    @POST("notification/show")
    Call<SendNotificationModel> getNotifications(@Field("user_id") int userId,
                                                 @Field("lang") String lang);

    @FormUrlEncoded
    @POST("notification/delete")
    Call<MainResponseModel> deleteNotifications(@Field("notification_id") int notifyId,
                                                @Field("lang") String lang);

    @FormUrlEncoded
    @POST("update/device_id")
    Call<MainResponseModel> sendTokenToServer(@Field("user_id") int userId,
                                              @Field("device_id") String token,
                                              @Field("lang") String lang);

    @FormUrlEncoded
    @POST("order/bill")
    Call<OrdersModel> getBills(@Field("user_id") int userId,
                               @Field("lang") String lang);


    @FormUrlEncoded
    @POST("delegate/order/bill")
    Call<OrdersModel> getDelegateBills(@Field("delegate_id") int delegateId,
                                       @Field("lang") String lang);

    @FormUrlEncoded
    @POST("resendCode")
    Call<MainResponseModel> resendCode(@Field("user_id") int userId,
                                       @Field("lang") String lang);


    @FormUrlEncoded
    @POST("orders/new/continue")
    Single<OptionalServiceModel> optionalServiceModel(@Field("lang") String lang);

    @GET("transferService")
    Single<TransferModel> transferData();

    @FormUrlEncoded
    @POST("transferServicePost")
    Observable<MainResponseModel> transferDataPost(@Field("lang") String lang,
                                                   @Field("user_id") int userId,
                                                   @Field("upload_lat") double upLat,
                                                   @Field("upload_lng") double upLng,
                                                   @Field("upload_address") String upAdrs,
                                                   @Field("download_lat") double downLat,
                                                   @Field("download_lng") double downLng,
                                                   @Field("download_address") String downAdrs,
                                                   @Field("services") String services,
                                                   @Field("type") String type,
                                                   @Field("note") String note);

    @FormUrlEncoded
    @POST("storage")
    Observable<MainResponseModel> storage(@Field("download_place_id") int placeId,
                                          @Field("upload_lat") double upLat,
                                          @Field("upload_lng") double upLng,
                                          @Field("upload_address") String upAdrs,
                                          @Field("storage_time_id") int timeId,
                                          @Field("storage_space_id") int spaceId,
                                          @Field("lang") String lang,
                                          @Field("user_id") int userId,
                                          @Field("note") String note);

    @FormUrlEncoded
    @POST("update/latlng")
    Call<MainResponseModel> updateLatLng(@Field("user_id") int userId,
                                         @Field("lat") double lat,
                                         @Field("lng") double lng);


    @FormUrlEncoded
    @POST("orders/show/newOrder")
    Observable<NewOrderShowModel> showNewOrder( @Field("lang") String lang,
                                                @Field("user_id") int userId,
                                                @Field("order_id") String orderId);


}
