package com.aait.expresss.Callbacks.navigation;

public interface NavigationCloseDrawerListener {

    void onDrawerClose(Boolean b);
}
