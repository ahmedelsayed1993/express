package com.aait.expresss.Callbacks;

public interface NewOrderListener {
    void onNewOrderClickAdd(int id);
    void onNewOrderClickRemove(int id);
}
