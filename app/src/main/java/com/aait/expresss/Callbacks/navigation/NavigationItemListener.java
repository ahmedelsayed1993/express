package com.aait.expresss.Callbacks.navigation;

public interface NavigationItemListener {
    void onHomeClick();
    void onProfileClick();
    void onCurrentOrderClick();
    void onOldOrderClick();
    void onNotificationClick();
    void onBillsClick();
    void onSettingsClick();
    void onShareAppClick();
    void onContactUsClick();
    void onConditionsClick();
    void onLogoutClick();
}
