package com.aait.expresss.Callbacks;

import com.aait.expresss.Models.CarsModel;

public interface MyCarListener {
    void myCarClick(CarsModel.CarsData carsData);
    void myCarDelete(CarsModel.CarsData carsData);
}
