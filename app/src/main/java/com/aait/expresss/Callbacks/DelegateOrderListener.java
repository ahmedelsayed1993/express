package com.aait.expresss.Callbacks;

import com.aait.expresss.Models.OrdersModel;

public interface DelegateOrderListener {
    void onDelegateOrderClick(OrdersModel.OrderData currentOrderData);
}
