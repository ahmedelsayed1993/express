package com.aait.expresss.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.LocaleList;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import com.aait.expresss.BuildConfig;
import com.aait.expresss.R;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static void onPrintLog(Object o) {

        if(BuildConfig.DEBUG){
            Log.e("Response >>>>", new Gson().toJson(o));
        }

    }

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        assert connectivityManager != null;
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static boolean isValid(String input) {
        return input != null && !input.trim().isEmpty();
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static final boolean checkEmail(EditText email) {
        if (!isEmailValid(email.getText().toString())) {
            email.setError(email.getContext().getString(R.string.check_mail));
            return false;
        }
        return true;
    }
    public static final boolean checkError(EditText editText) {
        if (!isValid(editText.getText().toString())) {
            editText.setError(editText.getContext().getString(R.string.check_error));
            return false;
        }
        return true;
    }

    public static boolean checkMatch(EditText pass, EditText confirm_pass) {
        if (!pass.getText().toString().matches(confirm_pass.getText().toString())) {
            confirm_pass
                    .setError(confirm_pass.getContext().getString(R.string.check_match));
        } else {
            return true;
        }
        return false;
    }

    public static boolean checkPhoneSize(EditText mPhone) {
        if (mPhone.getText().toString().length() < 6) {
            mPhone.setError(mPhone.getContext().getString(R.string.check_phone_size));
            return false;
        }
        return true;
    }

    public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public static String nullChecker(Object  check){
        if (check==null||check.toString().equals("null")){
            return "( )";
        }
        return check.toString();
    }
    public static boolean checkPasswordSize(EditText mPassword) {
        if (mPassword.getText().toString().length() < 6) {
            mPassword.setError(mPassword.getContext().getString(R.string.check_pass_size));
            return false;
        }
        return true;
    }

    public static String encodedBase64(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return Base64.encodeToString(stream.toByteArray(), Base64.NO_WRAP);
    }

    public static Bitmap decodedBase64(String img64) {
        byte[] bytes = Base64.decode(img64, Base64.NO_WRAP);
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 120, 120, false);
        return bitmap;
    }

    //image dialog for denying permissions
    public static void goToImageSettings(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle)
                .setMessage(R.string.pix_permission)
                .setPositiveButton(context.getString(R.string.settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", context.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }).setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.show();
    }


    //location dialog for denying permission
    public static void goToLocationPermissionSettings(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle)
                .setMessage(R.string.location_permission_asking)
                .setPositiveButton(context.getString(R.string.settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", context.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }).setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.show();
    }

    public static void goToLocationSettings(final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.Gps_title);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.Gps_Description);

        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                if (context instanceof Activity) {
                    ((Activity)context).startActivityForResult(intent, 300);
                } else {

                }


            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static void share_face(Context context,String app_package_name){
        String url = "https://facebook.com/sharer/sharer.php?u=" + app_package_name;
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
    public static void share_app_twitter(Context context,String app_package_name){
        String url = "https://twitter.com/intent/tweet?url="+ app_package_name;
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public static void share_app_gplus(Context context,String app_package_name){
        String url = "https://plus.google.com/share?url="+ app_package_name;
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
    public static Context changeLang( String language,Context context) {
        Resources res = context.getResources();
        Configuration configuration = res.getConfiguration();
        Locale newLocale = new Locale(language);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(newLocale);
            LocaleList localeList = new LocaleList(newLocale);
            LocaleList.setDefault(localeList);
            configuration.setLocales(localeList);
            context = context.createConfigurationContext(configuration);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(newLocale);
            context = context.createConfigurationContext(configuration);

        } else {
            configuration.locale = newLocale;
            res.updateConfiguration(configuration, res.getDisplayMetrics());
        }
        return context;

    }
}
