package com.aait.expresss.Utils;



public class Constant {

    public static int GPSEnabling = 300;
    public static int isBroadcast = 1;
  //  public static int GPSEnabling = 300;



    public static final String SEARCH = "searchquery";
    public static final String user_provide = "provider";
    public static final String user_client = "user";

    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String ID = "id";
    public static final String SCANMODEL = "scan_model";
    public static final String MODEL = "model";
    public static final String LOCATION = "location";
    public static final String SELECTEDITEM = "selected_item";
    public static final String provider = "provider";
    public static final String Time = "Time";
    public static final String Date = "Date";
    public static final String visitor = "visitor";
    public static final String NOTIFICAION = "Notification";

    public static final String REGULAR_FONT = "fonts/cairo_regular.ttf";

    public static final class PermissionCode {
        public static final int STORAGE = 1;
        public static final int CAMERA = 8;

    }

    public static final class Notification {
        public static final int Chat = 1;
        public static final int Base = 8;

    }
    public static class RequestCode {
        public static int GETLOCATION = 500;
        public static int GETLOCATION2 = 510;
        public static int GETLOCATION3 = 520;

        public static final int GPSEnabling = 300;

        public static final int CHOOSE_SCHOOL = 500;

        public static final int OPEN_ACCOUNT_KI = 100;

        public static final int Add_EVENT = 200;

    }
        public static final class RequestPermission {

        public static int REQUEST_GPS_LOCATION = 800;

        public static int REQUEST_IMAGES = 200;

        public static int REQUEST_CALL = 300;

        public static int CALL_PHONE = 310;
    }



}

