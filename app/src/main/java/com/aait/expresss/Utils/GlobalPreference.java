package com.aait.expresss.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.aait.expresss.Models.LoginModel;
import com.google.gson.Gson;

public class GlobalPreference {
    private static GlobalPreference mInstance;
    private Context mCtx;

    //Shared Pref Key
    private static final String SHARED_PREF_KEY="shared_key";
    //Keys
    private static final String USER_LOGGED="user_logged";
    private static final String USER_TOKEN="user_token";
    private static final String APP_LANGUAGE="app_language";
    private static final String USER_TYPE="user_type";
    private static final String APP_NOTIFICATION="app_notification";
    public static final String INTRO_SCREEN="INTRO_SCREEN";

    public GlobalPreference(Context context){
        mCtx=context;
    }

    public static synchronized GlobalPreference getInstance(Context context){
        if (mInstance==null){
            mInstance=new GlobalPreference(context);
        }
        return mInstance;
    }


    //Language Handling
    public void setAppLanguage(String lang){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(APP_LANGUAGE,lang);
        editor.apply();
    }
    public String getAppLanguage(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);

        return sharedPreferences.getString(APP_LANGUAGE, "ar");
    }
    public int getAppLanguage(String lang){
        if (lang.equalsIgnoreCase("en")){
            return 1;
        }else{
            return 0;
        }
    }

    //Login/SignUp Handling


    //TODO SetUp Login Preference
    public void login(LoginModel user){
        Gson gson=new Gson();
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(USER_LOGGED,gson.toJson(user));
        editor.apply();
    }
    public void logout(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);

        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.remove(USER_LOGGED);
        editor.remove(USER_TOKEN);
        editor.remove(USER_TYPE);
        editor.apply();
    }
    public boolean isUserLogged(){
        Gson gson=new Gson();
        LoginModel user;
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        user=gson.fromJson(sharedPreferences.getString(USER_LOGGED,null),LoginModel.class);

        return user != null;

    }
    public LoginModel getUser(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        return new Gson().fromJson(sharedPreferences.getString(USER_LOGGED,null),LoginModel.class);
    }


    //Token Handling
    public void addUserToken(String token){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(USER_TOKEN,token);
        editor.apply();
    }
    public String getUserToken(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        return sharedPreferences.getString(USER_TOKEN,null);
    }

    //intro screen
    public Boolean getIntroScreen(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        return sharedPreferences.getBoolean(INTRO_SCREEN,true);
    }

    public void setIntroScreen(Boolean intro){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(INTRO_SCREEN,intro);
        editor.apply();
    }
    //User Type Handling

    //0 -> visitor
    //1 -> customer or provider
    public void setUserType(int type){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt(USER_TYPE,type);
        editor.apply();
    }
    public int getUserType(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        return sharedPreferences.getInt(USER_TYPE,1);
    }

    //App Notification
    public void setNotificationAccess(Boolean isEnabled){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(APP_NOTIFICATION,isEnabled);
        editor.apply();
    }
    public boolean isNotificationEnabled(){
        SharedPreferences sharedPreferences=mCtx.getSharedPreferences(SHARED_PREF_KEY,0);
        return sharedPreferences.getBoolean(APP_NOTIFICATION,true);
    }

    public int getNotificationAccess(boolean isNotificationAccess){
        if (isNotificationAccess){
            return 1;
        }
        return 0;
    }
}
