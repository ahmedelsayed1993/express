package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DelegateOrderViewModel extends ViewModel {
    private MutableLiveData<OrdersModel> delegateCurrentOrdersModelMutableLiveData;

    public MutableLiveData<OrdersModel> getDelegateCurrentOrdersModelMutableLiveData(int delegateId, String lang) {
        if (delegateCurrentOrdersModelMutableLiveData==null){
            delegateCurrentOrdersModelMutableLiveData=new MutableLiveData<>();
            getDelegateOrders(delegateId,lang);
        }
        return delegateCurrentOrdersModelMutableLiveData;
    }

    private void getDelegateOrders(int delegateId, String lang) {
        RetrofitCall.getInstance().getApi()
                .getDelegateOrders(delegateId,lang)
                .enqueue(new Callback<OrdersModel>() {
                    @Override
                    public void onResponse(Call<OrdersModel> call, Response<OrdersModel> response) {
                        if (response.body().getKey().equals("1")){
                            delegateCurrentOrdersModelMutableLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<OrdersModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
