package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.OrderNowModel;
import com.aait.expresss.Network.RetrofitCall;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class StoreNewOrderViewModel extends ViewModel {
    private MutableLiveData<OrderNowModel> ordersModelMutableLiveData;
    private CompositeDisposable compositeDisposable=new CompositeDisposable();

    public MutableLiveData<OrderNowModel> getOrdersModelMutableLiveData(int userId,String carId,String lang) {
        if (ordersModelMutableLiveData==null){
            ordersModelMutableLiveData=new MutableLiveData<>();
            loadData(userId,carId,lang);
        }
        return ordersModelMutableLiveData;
    }

    private void loadData(int userId, String carId, String lang) {
        RetrofitCall.getInstance().getApi()
                .storeNewOrder(userId, carId, lang)
                .enqueue(new Callback<OrderNowModel>() {
                    @Override
                    public void onResponse(Call<OrderNowModel> call, Response<OrderNowModel> response) {
                        if (response.body().getKey().equals("1")){
                            ordersModelMutableLiveData.setValue(response.body());
                        }

                    }

                    @Override
                    public void onFailure(Call<OrderNowModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });



    }


    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
