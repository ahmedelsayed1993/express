package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.FollowOrderModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DelegateFollowViewModel extends ViewModel{
    private MutableLiveData<FollowOrderModel> followOrderModelMutableLiveData;

    public MutableLiveData<FollowOrderModel> getFollowOrderModelMutableLiveData(int orderId,String lang) {

        if (followOrderModelMutableLiveData==null){
            followOrderModelMutableLiveData=new MutableLiveData<>();
            loadData(orderId,lang);
        }

        return followOrderModelMutableLiveData;
    }

    private void loadData(int orderId, String lang) {
        RetrofitCall.getInstance().getApi()
                .delegateFollowOrder(orderId,lang)
                .enqueue(new Callback<FollowOrderModel>() {
                    @Override
                    public void onResponse(Call<FollowOrderModel> call, Response<FollowOrderModel> response) {
                        followOrderModelMutableLiveData.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<FollowOrderModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
