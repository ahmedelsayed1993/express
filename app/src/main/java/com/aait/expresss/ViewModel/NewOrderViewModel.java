package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.NewOrderModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class NewOrderViewModel extends ViewModel {
    private MutableLiveData<NewOrderModel> newOrderModelMutableLiveData;

    public MutableLiveData<NewOrderModel> getNewOrderModelMutableLiveData(int userId,String lang) {
        if (newOrderModelMutableLiveData==null){
            newOrderModelMutableLiveData=new MutableLiveData<>();
            loadData(userId,lang);
        }
        return newOrderModelMutableLiveData;
    }

    private void loadData(int userId, String lang) {
        RetrofitCall.getInstance().getApi()
                .newOrders(userId, lang)
                .enqueue(new Callback<NewOrderModel>() {
                    @Override
                    public void onResponse(Call<NewOrderModel> call, Response<NewOrderModel> response) {
                        newOrderModelMutableLiveData.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<NewOrderModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }


}
