package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.AllDataModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AllDataViewModel extends ViewModel {
    private MutableLiveData<AllDataModel> allDataModelMutableLiveData;

    public MutableLiveData<AllDataModel> getAllDataModelMutableLiveData(String lang) {
        if (allDataModelMutableLiveData==null){
            allDataModelMutableLiveData=new MutableLiveData<>();
            loadData(lang);
        }
        return allDataModelMutableLiveData;
    }

    private void loadData(String lang) {
        RetrofitCall.getInstance().getApi()
                .getUtils(lang)
                .enqueue(new Callback<AllDataModel>() {
                    @Override
                    public void onResponse(Call<AllDataModel> call, Response<AllDataModel> response) {
                        if (response.body().getKey().equals("1")){
                            allDataModelMutableLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<AllDataModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
