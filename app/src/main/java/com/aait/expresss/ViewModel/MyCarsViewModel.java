package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.CarsModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MyCarsViewModel extends ViewModel {
    private MutableLiveData<CarsModel> carsModelMutableLiveData;

    public MutableLiveData<CarsModel> getCarsModelMutableLiveData(int userId,String lang) {
        if (carsModelMutableLiveData==null){
            carsModelMutableLiveData=new MutableLiveData<>();
            loadData(userId,lang);
        }
        return carsModelMutableLiveData;
    }

    private void loadData(int userId, String lang) {
        RetrofitCall.getInstance().getApi()
                .getMyCars(userId,lang)
                .enqueue(new Callback<CarsModel>() {
                    @Override
                    public void onResponse(Call<CarsModel> call, Response<CarsModel> response) {
                        if (response.body().getKey().equals("1")){
                            carsModelMutableLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<CarsModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
