package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.SendNotificationModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class NotificationsViewModel extends ViewModel {
    private MutableLiveData<SendNotificationModel> sendNotificationModelMutableLiveData;

    public MutableLiveData<SendNotificationModel> getSendNotificationModelMutableLiveData(int userId,String lang) {

        if (sendNotificationModelMutableLiveData==null){
            sendNotificationModelMutableLiveData=new MutableLiveData<>();
            loadData(userId,lang);
        }

        return sendNotificationModelMutableLiveData;
    }

    private void loadData(int userId, String lang) {
        RetrofitCall.getInstance().getApi()
                .getNotifications(userId,lang)
                .enqueue(new Callback<SendNotificationModel>() {
                    @Override
                    public void onResponse(Call<SendNotificationModel> call, Response<SendNotificationModel> response) {
                        if (response.body().getKey().equals("1")){
                            sendNotificationModelMutableLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<SendNotificationModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
