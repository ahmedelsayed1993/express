package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ClientOldOrderViewModel extends ViewModel {
    private MutableLiveData<OrdersModel> ordersModelMutableLiveData;

    public MutableLiveData<OrdersModel> getOrdersModelMutableLiveData(int userId,String lang) {
        if (ordersModelMutableLiveData==null){
            ordersModelMutableLiveData=new MutableLiveData<>() ;
            loadData(userId,lang);
        }

        return ordersModelMutableLiveData;
    }

    private void loadData(int userId, String lang) {
        RetrofitCall.getInstance().getApi()
                .getClientOldOrders(userId, lang)
                .enqueue(new Callback<OrdersModel>() {
                    @Override
                    public void onResponse(Call<OrdersModel> call, Response<OrdersModel> response) {
                        if (response.body().getKey().equals("1")){
                            ordersModelMutableLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<OrdersModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
