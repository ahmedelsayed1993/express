package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.TransferModel;
import com.aait.expresss.Network.RetrofitCall;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TranserServiceViewModel extends ViewModel {

    private MutableLiveData<TransferModel> transferModelMutableLiveData =new MutableLiveData<TransferModel>();


    public MutableLiveData<TransferModel> getTranserData(){
        RetrofitCall.getInstance().getApi()
                .transferData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<TransferModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(TransferModel transferModel) {
                        transferModelMutableLiveData.postValue(transferModel);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

        return transferModelMutableLiveData;
    }

}
