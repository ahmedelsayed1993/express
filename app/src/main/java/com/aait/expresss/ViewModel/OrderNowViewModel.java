package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.NewOrderShowModel;
import com.aait.expresss.Network.RetrofitCall;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OrderNowViewModel extends ViewModel {

    private Disposable disposable;
    private MutableLiveData<NewOrderShowModel> ordersModelMutableLiveData;


    public MutableLiveData<NewOrderShowModel> getOrdersModelMutableLiveData(int userId,String carId,String lang) {
        if (ordersModelMutableLiveData==null){
            ordersModelMutableLiveData=new MutableLiveData<>();
            loadData(lang,userId,carId);
        }
        return ordersModelMutableLiveData;
    }



    private void loadData(String lang,int userId,String carId){


        RetrofitCall.getInstance().getApi()
                .showNewOrder(lang,userId,carId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewOrderShowModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable=d;
                    }

                    @Override
                    public void onNext(NewOrderShowModel newOrderShowModel) {
                        ordersModelMutableLiveData.setValue(newOrderShowModel);

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.dispose();
    }
}
