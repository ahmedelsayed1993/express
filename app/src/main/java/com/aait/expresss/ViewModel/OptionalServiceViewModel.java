package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.OptionalServiceModel;
import com.aait.expresss.Models.OrderNowModel;
import com.aait.expresss.Network.RetrofitCall;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class OptionalServiceViewModel extends ViewModel {

    private CompositeDisposable compositeDisposable=new CompositeDisposable();
    private MutableLiveData<OrderNowModel> ordersModelMutableLiveData;
    private MutableLiveData<OptionalServiceModel> optionalServiceViewModelMutableLiveData = new MutableLiveData<OptionalServiceModel>();


    public MutableLiveData<OrderNowModel> getOrdersModelMutableLiveData(int userId,String carId,String lang) {
        if (ordersModelMutableLiveData==null){
            ordersModelMutableLiveData=new MutableLiveData<>();
            loadData(userId,carId,lang);
        }
        return ordersModelMutableLiveData;
    }



    private void loadData(int userId, String carId, String lang) {
        RetrofitCall.getInstance().getApi()
                .storeNewOrder(userId, carId, lang)
                .enqueue(new Callback<OrderNowModel>() {
                    @Override
                    public void onResponse(Call<OrderNowModel> call, Response<OrderNowModel> response) {
                        if (response.body().getKey().equals("1")){
                            ordersModelMutableLiveData.setValue(response.body());
                        }

                    }

                    @Override
                    public void onFailure(Call<OrderNowModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
    public MutableLiveData<OptionalServiceModel> getOptionalData(String lang){
        RetrofitCall.getInstance().getApi().optionalServiceModel(lang)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<OptionalServiceModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(OptionalServiceModel optionalServiceModel) {
                        optionalServiceViewModelMutableLiveData.postValue(optionalServiceModel);


                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

        return optionalServiceViewModelMutableLiveData;
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
