package com.aait.expresss.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.Network.RetrofitCall;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class BillViewModel extends ViewModel {
    private MutableLiveData<OrdersModel> delegateBillsLiveData;
    private MutableLiveData<OrdersModel> billsLiveData;

    public MutableLiveData<OrdersModel> getDelegateBillsLiveData(int delegateId, String lang) {
        if (delegateBillsLiveData==null){
            delegateBillsLiveData=new MutableLiveData<>();
            getDelegateBills(delegateId,lang);
        }
        return delegateBillsLiveData;
    }

    public MutableLiveData<OrdersModel> getBillsLiveData(int userId, String lang) {
        if (billsLiveData==null){
            billsLiveData=new MutableLiveData<>();
            getClientBills(userId,lang);
        }
        return billsLiveData;
    }

    private void getClientBills(int userId, String lang) {
        RetrofitCall.getInstance().getApi().getBills(userId, lang)
                .enqueue(new Callback<OrdersModel>() {
                    @Override
                    public void onResponse(Call<OrdersModel> call, Response<OrdersModel> response) {
                        if (response.body().getKey().equals("1")){
                            billsLiveData.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<OrdersModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }

    private void getDelegateBills(int delegateId, String lang) {
        RetrofitCall.getInstance().getApi().getDelegateBills(delegateId, lang)
        .enqueue(new Callback<OrdersModel>() {
            @Override
            public void onResponse(Call<OrdersModel> call, Response<OrdersModel> response) {
                if (response.body().getKey().equals("1")){
                    delegateBillsLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<OrdersModel> call, Throwable t) {
                Timber.wtf(t);
            }
        });
    }
}
