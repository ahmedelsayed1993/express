package com.aait.expresss.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class LatLngModel implements Parcelable {
    private double lat;
    private double lng;

    public LatLngModel(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public LatLngModel() {
    }

    protected LatLngModel(Parcel in) {
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public static final Creator<LatLngModel> CREATOR = new Creator<LatLngModel>() {
        @Override
        public LatLngModel createFromParcel(Parcel in) {
            return new LatLngModel(in);
        }

        @Override
        public LatLngModel[] newArray(int size) {
            return new LatLngModel[size];
        }
    };

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }
}
