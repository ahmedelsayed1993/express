package com.aait.expresss.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginModel {

    /**
     * key : 1
     * massage : تم تسجيل الدخول بنجاح
     * data : {"id":55,"name":"Abdelrahman10100","email":"loveday_ar10001@yahoo.com","phone":"0968547123001602","lat":"12.554555","lng":"14.526565","address":"Egypt","delegate":1,"code":"9591","app_lang":"ar","notifications":"1","avatar":"default.png","arrears":"0","active":1,"role":0,"device_id":"111222333","avatar_path":"https://no3manpro.arabsdesign.com/zeat/public/1.png"}
     */

    private String key;
    private String massage;
    private LoginData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public static class LoginData implements Parcelable {
        /**
         * id : 55
         * name : Abdelrahman10100
         * email : loveday_ar10001@yahoo.com
         * phone : 0968547123001602
         * lat : 12.554555
         * lng : 14.526565
         * address : Egypt
         * delegate : 1
         * code : 9591
         * app_lang : ar
         * notifications : 1
         * avatar : default.png
         * arrears : 0
         * active : 1
         * role : 0
         * device_id : 111222333
         * avatar_path : https://no3manpro.arabsdesign.com/zeat/public/1.png
         */

        private int id;
        private String name;
        private String email;
        private String phone;
        private String lat;
        private String lng;
        private String address;
        private int delegate;
        private String code;
        private String app_lang;
        private String notifications;
        private String avatar;
        private String arrears;
        private int active;
        private int role;
        private String device_id;
        private String avatar_path;

        protected LoginData(Parcel in) {
            id = in.readInt();
            name = in.readString();
            email = in.readString();
            phone = in.readString();
            lat = in.readString();
            lng = in.readString();
            address = in.readString();
            delegate = in.readInt();
            code = in.readString();
            app_lang = in.readString();
            notifications = in.readString();
            avatar = in.readString();
            arrears = in.readString();
            active = in.readInt();
            role = in.readInt();
            device_id = in.readString();
            avatar_path = in.readString();
        }

        public static final Creator<LoginData> CREATOR = new Creator<LoginData>() {
            @Override
            public LoginData createFromParcel(Parcel in) {
                return new LoginData(in);
            }

            @Override
            public LoginData[] newArray(int size) {
                return new LoginData[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getDelegate() {
            return delegate;
        }

        public void setDelegate(int delegate) {
            this.delegate = delegate;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getApp_lang() {
            return app_lang;
        }

        public void setApp_lang(String app_lang) {
            this.app_lang = app_lang;
        }

        public String getNotifications() {
            return notifications;
        }

        public void setNotifications(String notifications) {
            this.notifications = notifications;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getArrears() {
            return arrears;
        }

        public void setArrears(String arrears) {
            this.arrears = arrears;
        }

        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getAvatar_path() {
            return avatar_path;
        }

        public void setAvatar_path(String avatar_path) {
            this.avatar_path = avatar_path;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(email);
            dest.writeString(phone);
            dest.writeString(lat);
            dest.writeString(lng);
            dest.writeString(address);
            dest.writeInt(delegate);
            dest.writeString(code);
            dest.writeString(app_lang);
            dest.writeString(notifications);
            dest.writeString(avatar);
            dest.writeString(arrears);
            dest.writeInt(active);
            dest.writeInt(role);
            dest.writeString(device_id);
            dest.writeString(avatar_path);
        }
    }
}
