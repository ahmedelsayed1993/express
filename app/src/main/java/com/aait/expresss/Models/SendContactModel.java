package com.aait.expresss.Models;

public class SendContactModel {

    /**
     * key : 1
     * massage : تم الحفظ بنجاح
     */

    private String key;
    private String massage;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
