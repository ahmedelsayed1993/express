package com.aait.expresss.Models;

import java.util.List;

public class TransferModel {

    /**
     * key : 1
     * massage : تم الحفظ بنجاح
     * data : {"services":[{"id":19,"name":"مبرد او مجمد"},{"id":18,"name":"مواد غذائية"},{"id":17,"name":"اثاث منزلي او مكتبي"},{"id":16,"name":"دراجة نارية"},{"id":15,"name":"سيارة رياضية"},{"id":14,"name":"ادوات ومواد"},{"id":13,"name":"مواد بترولية"},{"id":12,"name":"معدات ثقيلة"},{"id":11,"name":"معدة متوسطة"},{"id":10,"name":"معدة صغيرة"},{"id":9,"name":"طرد من 5 الى 10 kg"},{"id":8,"name":"سيارة كبيرة"},{"id":7,"name":"سيارة متوسطة"},{"id":6,"name":"شاحنة"},{"id":5,"name":"سيارة صغيرة"},{"id":3,"name":"طرد اقل من 5 k"},{"id":2,"name":"كونتينر"}],"download_places":[{"id":9,"name":"الاحساء"},{"id":8,"name":"الجبيل"},{"id":7,"name":"الدمام"},{"id":6,"name":"المدينة"},{"id":5,"name":"الطائف"},{"id":4,"name":"مكة"},{"id":3,"name":"الرياض"},{"id":2,"name":"جدة"}],"storage_space":[{"id":10,"name":"500 متر"},{"id":9,"name":"450 متر"},{"id":8,"name":"400 متر"},{"id":7,"name":"350 متر"},{"id":6,"name":"300 متر"},{"id":5,"name":"250 متر"},{"id":4,"name":"200 متر"},{"id":3,"name":"150 متر"},{"id":2,"name":"100 متر"},{"id":1,"name":"50 متر"}],"storage_times":[{"id":10,"name":"3 اشهر"},{"id":9,"name":"75 يوم"},{"id":8,"name":"شهرين"},{"id":7,"name":"45 يوم"},{"id":6,"name":"شهر"},{"id":5,"name":"21 يوم"},{"id":4,"name":"14 يوم"},{"id":3,"name":"7 ايام"}]}
     */

    private String key;
    private String massage;
    private TransferData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public TransferData getData() {
        return data;
    }

    public void setData(TransferData data) {
        this.data = data;
    }

    public static class TransferData {
        private List<Services> services;
        private List<DownloadPlaces> download_places;
        private List<StorageSpace> storage_space;
        private List<StorageTimes> storage_times;

        public List<Services> getServices() {
            return services;
        }

        public void setServices(List<Services> services) {
            this.services = services;
        }

        public List<DownloadPlaces> getDownload_places() {
            return download_places;
        }

        public void setDownload_places(List<DownloadPlaces> download_places) {
            this.download_places = download_places;
        }

        public List<StorageSpace> getStorage_space() {
            return storage_space;
        }

        public void setStorage_space(List<StorageSpace> storage_space) {
            this.storage_space = storage_space;
        }

        public List<StorageTimes> getStorage_times() {
            return storage_times;
        }

        public void setStorage_times(List<StorageTimes> storage_times) {
            this.storage_times = storage_times;
        }

        public static class Services {
            /**
             * id : 19
             * name : مبرد او مجمد
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class DownloadPlaces {
            /**
             * id : 9
             * name : الاحساء
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class StorageSpace {
            /**
             * id : 10
             * name : 500 متر
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class StorageTimes {
            /**
             * id : 10
             * name : 3 اشهر
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
