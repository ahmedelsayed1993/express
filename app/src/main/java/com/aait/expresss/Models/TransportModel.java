package com.aait.expresss.Models;

public class TransportModel {

    /**
     * service_id : 19
     * count : 5
     */

    private int service_id;
    private String count;


    public TransportModel(int service_id, String count) {
        this.service_id = service_id;
        this.count = count;
    }

    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
