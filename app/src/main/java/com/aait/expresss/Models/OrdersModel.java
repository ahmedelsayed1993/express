package com.aait.expresss.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class OrdersModel {


    /**
     * key : 1
     * massage : تم  بنجاح
     * data : [{"order_number":522,"order_car_count":1,"order_time":"01:23 PM","order_date":"2019-03-07","car_owner_name":"noaman","car_owner_address":"","car_owner_lat":"31.4391688","car_owner_lng":"31.6489738","cars_data":[{"car_number":607,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"كسترول 5000 km","car_oil_price":"14","car_model_year":2016,"car_plate_number":"2426","car_viscosity_degree":"123326"}],"services":[{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"}],"services_price":30,"order_total_price":44},{"order_number":320,"order_car_count":1,"order_time":"10:39 AM","order_date":"2019-01-15","car_owner_name":"noaman","car_owner_address":"","car_owner_lat":"31.4391688","car_owner_lng":"31.6489738","cars_data":[{"car_number":606,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"كسترول 5000 km","car_oil_price":"14","car_model_year":2016,"car_plate_number":"2426","car_viscosity_degree":"123326"}],"services":[{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"}],"services_price":30,"order_total_price":44}]
     */

    private String key;
    private String massage;
    private List<OrderData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<OrderData> getData() {
        return data;
    }

    public void setData(List<OrderData> data) {
        this.data = data;
    }

    public static class OrderData implements Parcelable {
        /**
         * order_number : 522
         * order_car_count : 1
         * order_time : 01:23 PM
         * order_date : 2019-03-07
         * car_owner_name : noaman
         * car_owner_address :
         * car_owner_lat : 31.4391688
         * car_owner_lng : 31.6489738
         * cars_data : [{"car_number":607,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"كسترول 5000 km","car_oil_price":"14","car_model_year":2016,"car_plate_number":"2426","car_viscosity_degree":"123326"}]
         * services : [{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"}]
         * services_price : 30
         * order_total_price : 44
         */

        private int order_number;
        private int order_car_count;
        private String order_time;
        private String order_date;
        private String car_owner_name;
        private String car_owner_address;
        private String car_owner_lat;
        private String car_owner_lng;
        private int services_price;
        private int order_total_price;
        private List<CarsData> cars_data;
        private List<ServicesData> services;

        protected OrderData(Parcel in) {
            order_number = in.readInt();
            order_car_count = in.readInt();
            order_time = in.readString();
            order_date = in.readString();
            car_owner_name = in.readString();
            car_owner_address = in.readString();
            car_owner_lat = in.readString();
            car_owner_lng = in.readString();
            services_price = in.readInt();
            order_total_price = in.readInt();
            cars_data = in.createTypedArrayList(CarsData.CREATOR);
            services = in.createTypedArrayList(ServicesData.CREATOR);
        }

        public static final Creator<OrderData> CREATOR = new Creator<OrderData>() {
            @Override
            public OrderData createFromParcel(Parcel in) {
                return new OrderData(in);
            }

            @Override
            public OrderData[] newArray(int size) {
                return new OrderData[size];
            }
        };

        public int getOrder_number() {
            return order_number;
        }

        public void setOrder_number(int order_number) {
            this.order_number = order_number;
        }

        public int getOrder_car_count() {
            return order_car_count;
        }

        public void setOrder_car_count(int order_car_count) {
            this.order_car_count = order_car_count;
        }

        public String getOrder_time() {
            return order_time;
        }

        public void setOrder_time(String order_time) {
            this.order_time = order_time;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getCar_owner_name() {
            return car_owner_name;
        }

        public void setCar_owner_name(String car_owner_name) {
            this.car_owner_name = car_owner_name;
        }

        public String getCar_owner_address() {
            return car_owner_address;
        }

        public void setCar_owner_address(String car_owner_address) {
            this.car_owner_address = car_owner_address;
        }

        public String getCar_owner_lat() {
            return car_owner_lat;
        }

        public void setCar_owner_lat(String car_owner_lat) {
            this.car_owner_lat = car_owner_lat;
        }

        public String getCar_owner_lng() {
            return car_owner_lng;
        }

        public void setCar_owner_lng(String car_owner_lng) {
            this.car_owner_lng = car_owner_lng;
        }

        public int getServices_price() {
            return services_price;
        }

        public void setServices_price(int services_price) {
            this.services_price = services_price;
        }

        public int getOrder_total_price() {
            return order_total_price;
        }

        public void setOrder_total_price(int order_total_price) {
            this.order_total_price = order_total_price;
        }

        public List<CarsData> getCars_data() {
            return cars_data;
        }

        public void setCars_data(List<CarsData> cars_data) {
            this.cars_data = cars_data;
        }

        public List<ServicesData> getServices() {
            return services;
        }

        public void setServices(List<ServicesData> services) {
            this.services = services;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(order_number);
            dest.writeInt(order_car_count);
            dest.writeString(order_time);
            dest.writeString(order_date);
            dest.writeString(car_owner_name);
            dest.writeString(car_owner_address);
            dest.writeString(car_owner_lat);
            dest.writeString(car_owner_lng);
            dest.writeInt(services_price);
            dest.writeInt(order_total_price);
            dest.writeTypedList(cars_data);
            dest.writeTypedList(services);
        }

        public static class CarsData implements Parcelable {
            /**
             * car_number : 607
             * car_brand_name : هيونداي
             * car_engine_name : محرك ديزل
             * car_oil_name : كسترول 5000 km
             * car_oil_price : 14
             * car_model_year : 2016
             * car_plate_number : 2426
             * car_viscosity_degree : 123326
             */

            private int car_number;
            private String car_brand_name;
            private String car_engine_name;
            private String car_oil_name;
            private String car_oil_price;
            private int car_model_year;
            private String car_plate_number;
            private String car_viscosity_degree;

            protected CarsData(Parcel in) {
                car_number = in.readInt();
                car_brand_name = in.readString();
                car_engine_name = in.readString();
                car_oil_name = in.readString();
                car_oil_price = in.readString();
                car_model_year = in.readInt();
                car_plate_number = in.readString();
                car_viscosity_degree = in.readString();
            }

            public static final Creator<CarsData> CREATOR = new Creator<CarsData>() {
                @Override
                public CarsData createFromParcel(Parcel in) {
                    return new CarsData(in);
                }

                @Override
                public CarsData[] newArray(int size) {
                    return new CarsData[size];
                }
            };

            public int getCar_number() {
                return car_number;
            }

            public void setCar_number(int car_number) {
                this.car_number = car_number;
            }

            public String getCar_brand_name() {
                return car_brand_name;
            }

            public void setCar_brand_name(String car_brand_name) {
                this.car_brand_name = car_brand_name;
            }

            public String getCar_engine_name() {
                return car_engine_name;
            }

            public void setCar_engine_name(String car_engine_name) {
                this.car_engine_name = car_engine_name;
            }

            public String getCar_oil_name() {
                return car_oil_name;
            }

            public void setCar_oil_name(String car_oil_name) {
                this.car_oil_name = car_oil_name;
            }

            public String getCar_oil_price() {
                return car_oil_price;
            }

            public void setCar_oil_price(String car_oil_price) {
                this.car_oil_price = car_oil_price;
            }

            public int getCar_model_year() {
                return car_model_year;
            }

            public void setCar_model_year(int car_model_year) {
                this.car_model_year = car_model_year;
            }

            public String getCar_plate_number() {
                return car_plate_number;
            }

            public void setCar_plate_number(String car_plate_number) {
                this.car_plate_number = car_plate_number;
            }

            public String getCar_viscosity_degree() {
                return car_viscosity_degree;
            }

            public void setCar_viscosity_degree(String car_viscosity_degree) {
                this.car_viscosity_degree = car_viscosity_degree;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(car_number);
                dest.writeString(car_brand_name);
                dest.writeString(car_engine_name);
                dest.writeString(car_oil_name);
                dest.writeString(car_oil_price);
                dest.writeInt(car_model_year);
                dest.writeString(car_plate_number);
                dest.writeString(car_viscosity_degree);
            }
        }

        public static class ServicesData implements Parcelable {
            /**
             * id : 13
             * name : فلتر هواء
             * price : 10
             */

            private int id;
            private String name;
            private String price;

            protected ServicesData(Parcel in) {
                id = in.readInt();
                name = in.readString();
                price = in.readString();
            }

            public static final Creator<ServicesData> CREATOR = new Creator<ServicesData>() {
                @Override
                public ServicesData createFromParcel(Parcel in) {
                    return new ServicesData(in);
                }

                @Override
                public ServicesData[] newArray(int size) {
                    return new ServicesData[size];
                }
            };

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(id);
                dest.writeString(name);
                dest.writeString(price);
            }
        }
    }
}
