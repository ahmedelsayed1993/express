package com.aait.expresss.Models;

import java.util.List;

public class BillsModel {

    /**
     * key : 1
     * massage : تم  بنجاح
     * data : [{"order_number":48,"order_delegate_name":"نعمان","order_car_count":1,"order_total_price":110,"order_time":"06:16 PM","order_date":"2018-07-03","car_owner_name":"اوامر الشبكه","car_owner_address":"","car_owner_lat":"123456789","car_owner_lng":"123456789","cars_data":[{"car_number":1,"car_brand_name":"تويوتا","car_model_year":2018,"car_plate_number":"258963","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":12}]},{"order_number":63,"order_delegate_name":"اوامر الشبكه","order_car_count":2,"order_total_price":220,"order_time":"11:57 AM","order_date":"2018-07-15","car_owner_name":"اوامر الشبكه","car_owner_address":"","car_owner_lat":"123456789","car_owner_lng":"123456789","cars_data":[{"car_number":1,"car_brand_name":"هيونداي","car_model_year":2013,"car_plate_number":"5245452","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":45542},{"car_number":2,"car_brand_name":"هيونداي","car_model_year":2013,"car_plate_number":"5245452","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":45542}]},{"order_number":66,"order_delegate_name":"نعمان","order_car_count":3,"order_total_price":330,"order_time":"07:47 PM","order_date":"2018-07-16","car_owner_name":"اوامر الشبكه","car_owner_address":"","car_owner_lat":"123456789","car_owner_lng":"123456789","cars_data":[{"car_number":1,"car_brand_name":"هيونداي","car_model_year":2013,"car_plate_number":"5245452","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":45542},{"car_number":2,"car_brand_name":"هيونداي","car_model_year":2013,"car_plate_number":"5245452","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":45542},{"car_number":3,"car_brand_name":"هيونداي","car_model_year":2013,"car_plate_number":"5245452","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":45542}]}]
     */

    private String key;
    private String massage;
    private List<BillsData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<BillsData> getData() {
        return data;
    }

    public void setData(List<BillsData> data) {
        this.data = data;
    }

    public static class BillsData {
        /**
         * order_number : 48
         * order_delegate_name : نعمان
         * order_car_count : 1
         * order_total_price : 110
         * order_time : 06:16 PM
         * order_date : 2018-07-03
         * car_owner_name : اوامر الشبكه
         * car_owner_address :
         * car_owner_lat : 123456789
         * car_owner_lng : 123456789
         * cars_data : [{"car_number":1,"car_brand_name":"تويوتا","car_model_year":2018,"car_plate_number":"258963","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_viscosity_degree":12}]
         */

        private int order_number;
        private String order_delegate_name;
        private int order_car_count;
        private int order_total_price;
        private String order_time;
        private String order_date;
        private String car_owner_name;
        private String car_owner_address;
        private String car_owner_lat;
        private String car_owner_lng;
        private List<CarsDataBean> cars_data;

        public int getOrder_number() {
            return order_number;
        }

        public void setOrder_number(int order_number) {
            this.order_number = order_number;
        }

        public String getOrder_delegate_name() {
            return order_delegate_name;
        }

        public void setOrder_delegate_name(String order_delegate_name) {
            this.order_delegate_name = order_delegate_name;
        }

        public int getOrder_car_count() {
            return order_car_count;
        }

        public void setOrder_car_count(int order_car_count) {
            this.order_car_count = order_car_count;
        }

        public int getOrder_total_price() {
            return order_total_price;
        }

        public void setOrder_total_price(int order_total_price) {
            this.order_total_price = order_total_price;
        }

        public String getOrder_time() {
            return order_time;
        }

        public void setOrder_time(String order_time) {
            this.order_time = order_time;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getCar_owner_name() {
            return car_owner_name;
        }

        public void setCar_owner_name(String car_owner_name) {
            this.car_owner_name = car_owner_name;
        }

        public String getCar_owner_address() {
            return car_owner_address;
        }

        public void setCar_owner_address(String car_owner_address) {
            this.car_owner_address = car_owner_address;
        }

        public String getCar_owner_lat() {
            return car_owner_lat;
        }

        public void setCar_owner_lat(String car_owner_lat) {
            this.car_owner_lat = car_owner_lat;
        }

        public String getCar_owner_lng() {
            return car_owner_lng;
        }

        public void setCar_owner_lng(String car_owner_lng) {
            this.car_owner_lng = car_owner_lng;
        }

        public List<CarsDataBean> getCars_data() {
            return cars_data;
        }

        public void setCars_data(List<CarsDataBean> cars_data) {
            this.cars_data = cars_data;
        }

        public static class CarsDataBean {
            /**
             * car_number : 1
             * car_brand_name : تويوتا
             * car_model_year : 2018
             * car_plate_number : 258963
             * car_oil_name : زيت ماتور
             * car_oil_price : 110
             * car_engine_name : محرك ديزل
             * car_viscosity_degree : 12
             */

            private int car_number;
            private String car_brand_name;
            private int car_model_year;
            private String car_plate_number;
            private String car_oil_name;
            private String car_oil_price;
            private String car_engine_name;
            private int car_viscosity_degree;

            public int getCar_number() {
                return car_number;
            }

            public void setCar_number(int car_number) {
                this.car_number = car_number;
            }

            public String getCar_brand_name() {
                return car_brand_name;
            }

            public void setCar_brand_name(String car_brand_name) {
                this.car_brand_name = car_brand_name;
            }

            public int getCar_model_year() {
                return car_model_year;
            }

            public void setCar_model_year(int car_model_year) {
                this.car_model_year = car_model_year;
            }

            public String getCar_plate_number() {
                return car_plate_number;
            }

            public void setCar_plate_number(String car_plate_number) {
                this.car_plate_number = car_plate_number;
            }

            public String getCar_oil_name() {
                return car_oil_name;
            }

            public void setCar_oil_name(String car_oil_name) {
                this.car_oil_name = car_oil_name;
            }

            public String getCar_oil_price() {
                return car_oil_price;
            }

            public void setCar_oil_price(String car_oil_price) {
                this.car_oil_price = car_oil_price;
            }

            public String getCar_engine_name() {
                return car_engine_name;
            }

            public void setCar_engine_name(String car_engine_name) {
                this.car_engine_name = car_engine_name;
            }

            public int getCar_viscosity_degree() {
                return car_viscosity_degree;
            }

            public void setCar_viscosity_degree(int car_viscosity_degree) {
                this.car_viscosity_degree = car_viscosity_degree;
            }
        }
    }
}
