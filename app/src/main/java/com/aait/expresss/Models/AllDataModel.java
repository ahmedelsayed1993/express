package com.aait.expresss.Models;

import java.util.List;

public class AllDataModel {

    /**
     * key : 1
     * massage : Success Send
     * data : {"engine_data":[{"id":1,"name":"Desal"},{"id":2,"name":"aut"},{"id":3,"name":"sterlaengh"},{"id":4,"name":"TURBINO"}],"brand_data":[{"id":1,"name":"Hyundai"},{"id":2,"name":"Toyota"},{"id":3,"name":"Lancer"},{"id":4,"name":"NissaN"},{"id":5,"name":"GMC"},{"id":6,"name":"audi"},{"id":7,"name":"bmw"},{"id":8,"name":"opel"},{"id":9,"name":"ford"},{"id":10,"name":"kia"},{"id":11,"name":"Chevrolet"},{"id":12,"name":"mercedes"},{"id":13,"name":"Volkswagen"},{"id":14,"name":"Ferrari"}],"oil_data":[{"id":1,"name":"Motor Oil"},{"id":2,"name":"Total Motor Oil"},{"id":3,"name":"Pennzoil Motor Oil"},{"id":4,"name":"valvoline"},{"id":5,"name":"castrol"},{"id":6,"name":"quaker state"},{"id":7,"name":"royal purple"},{"id":8,"name":"shell rotella"},{"id":9,"name":"peak"},{"id":10,"name":"Mobil 1"}]}
     */

    private String key;
    private String massage;
    private AllData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public AllData getData() {
        return data;
    }

    public void setData(AllData data) {
        this.data = data;
    }

    public static class AllData {
        private List<EngineData> engine_data;
        private List<BrandData> brand_data;
        private List<OilData> oil_data;

        public List<EngineData> getEngine_data() {
            return engine_data;
        }

        public void setEngine_data(List<EngineData> engine_data) {
            this.engine_data = engine_data;
        }

        public List<BrandData> getBrand_data() {
            return brand_data;
        }

        public void setBrand_data(List<BrandData> brand_data) {
            this.brand_data = brand_data;
        }

        public List<OilData> getOil_data() {
            return oil_data;
        }

        public void setOil_data(List<OilData> oil_data) {
            this.oil_data = oil_data;
        }

        public static class EngineData {
            /**
             * id : 1
             * name : Desal
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class BrandData {
            /**
             * id : 1
             * name : Hyundai
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class OilData {
            /**
             * id : 1
             * name : Motor Oil
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
