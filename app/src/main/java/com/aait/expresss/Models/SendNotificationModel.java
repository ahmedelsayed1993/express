package com.aait.expresss.Models;

import java.util.List;

public class SendNotificationModel {

    /**
     * key : 1
     * massage : تم  بنجاح
     * data : [{"message_id":118,"message":"لديك سيارات بحاجة إلى غيار الزيت","message_status":6,"order_id":null},{"message_id":114,"message":"المندوب فى الطريق","message_status":4,"order_id":41},{"message_id":113,"message":"المندوب فى الطريق","message_status":4,"order_id":48},{"message_id":112,"message":"تم قبول الطلب من الإدارة ستحصل على إشعار قبل وصول المندوب","message_status":3,"order_id":72},{"message_id":109,"message":"تم قبول الطلب من الإدارة ستحصل على إشعار قبل وصول المندوب","message_status":3,"order_id":61},{"message_id":107,"message":"تم قبول الطلب من الإدارة ستحصل على إشعار قبل وصول المندوب","message_status":3,"order_id":41},{"message_id":103,"message":"المندوب فى الطريق","message_status":4,"order_id":48},{"message_id":100,"message":"تم قبول الطلب من الإدارة ستحصل على إشعار قبل وصول المندوب","message_status":3,"order_id":73},{"message_id":99,"message":"المندوب فى الطريق","message_status":4,"order_id":66},{"message_id":95,"message":"المندوب فى الطريق","message_status":4,"order_id":48},{"message_id":94,"message":"المندوب فى الطريق","message_status":4,"order_id":48},{"message_id":89,"message":"تم قبول الطلب من الإدارة ستحصل على إشعار قبل وصول المندوب","message_status":3,"order_id":66}]
     */

    private String key;
    private String massage;
    private List<NotificationData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<NotificationData> getData() {
        return data;
    }

    public void setData(List<NotificationData> data) {
        this.data = data;
    }

    public static class NotificationData {
        /**
         * message_id : 118
         * message : لديك سيارات بحاجة إلى غيار الزيت
         * message_status : 6
         * order_id : null
         */

        private int message_id;
        private String message;
        private int message_status;
        private int order_id;

        public int getMessage_id() {
            return message_id;
        }

        public void setMessage_id(int message_id) {
            this.message_id = message_id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getMessage_status() {
            return message_status;
        }

        public void setMessage_status(int message_status) {
            this.message_status = message_status;
        }

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }
    }
}
