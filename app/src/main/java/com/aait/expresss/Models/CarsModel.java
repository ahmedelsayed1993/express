package com.aait.expresss.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class CarsModel {


    /**
     * key : 1
     * massage : تم  بنجاح
     * data : [{"id":540,"car_brand_id":1,"car_brand_name":"هيونداي","car_model_year":"64888","car_plate_number":"نسخ نة","car_oil_id":1,"car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_id":1,"car_engine_name":"محرك ديزل","car_num_containers":"88","car_viscosity_degree":"رامي ٦٦٦","number_of_month_to_change_oil":"21","car_oil_expired_date":"2018/10/12","car_oil_expired_time":"05:58:53","car_oil_expired_date_time":"2018-10-12 05:58:53"}]
     */

    private String key;
    private String massage;
    private List<CarsData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<CarsData> getData() {
        return data;
    }

    public void setData(List<CarsData> data) {
        this.data = data;
    }

    public static class CarsData implements Parcelable {
        /**
         * id : 540
         * car_brand_id : 1
         * car_brand_name : هيونداي
         * car_model_year : 64888
         * car_plate_number : نسخ نة
         * car_oil_id : 1
         * car_oil_name : زيت ماتور
         * car_oil_price : 110
         * car_engine_id : 1
         * car_engine_name : محرك ديزل
         * car_num_containers : 88
         * car_viscosity_degree : رامي ٦٦٦
         * number_of_month_to_change_oil : 21
         * car_oil_expired_date : 2018/10/12
         * car_oil_expired_time : 05:58:53
         * car_oil_expired_date_time : 2018-10-12 05:58:53
         */

        private int id;
        private int car_brand_id;
        private String car_brand_name;
        private String car_model_year;
        private String car_plate_number;
        private int car_oil_id;
        private String car_oil_name;
        private String car_oil_price;
        private int car_engine_id;
        private String car_engine_name;
        private String car_num_containers;
        private String car_viscosity_degree;
        private String number_of_month_to_change_oil;
        private String car_oil_expired_date;
        private String car_oil_expired_time;
        private String car_oil_expired_date_time;

        protected CarsData(Parcel in) {
            id = in.readInt();
            car_brand_id = in.readInt();
            car_brand_name = in.readString();
            car_model_year = in.readString();
            car_plate_number = in.readString();
            car_oil_id = in.readInt();
            car_oil_name = in.readString();
            car_oil_price = in.readString();
            car_engine_id = in.readInt();
            car_engine_name = in.readString();
            car_num_containers = in.readString();
            car_viscosity_degree = in.readString();
            number_of_month_to_change_oil = in.readString();
            car_oil_expired_date = in.readString();
            car_oil_expired_time = in.readString();
            car_oil_expired_date_time = in.readString();
        }

        public static final Creator<CarsData> CREATOR = new Creator<CarsData>() {
            @Override
            public CarsData createFromParcel(Parcel in) {
                return new CarsData(in);
            }

            @Override
            public CarsData[] newArray(int size) {
                return new CarsData[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getCar_brand_id() {
            return car_brand_id;
        }

        public void setCar_brand_id(int car_brand_id) {
            this.car_brand_id = car_brand_id;
        }

        public String getCar_brand_name() {
            return car_brand_name;
        }

        public void setCar_brand_name(String car_brand_name) {
            this.car_brand_name = car_brand_name;
        }

        public String getCar_model_year() {
            return car_model_year;
        }

        public void setCar_model_year(String car_model_year) {
            this.car_model_year = car_model_year;
        }

        public String getCar_plate_number() {
            return car_plate_number;
        }

        public void setCar_plate_number(String car_plate_number) {
            this.car_plate_number = car_plate_number;
        }

        public int getCar_oil_id() {
            return car_oil_id;
        }

        public void setCar_oil_id(int car_oil_id) {
            this.car_oil_id = car_oil_id;
        }

        public String getCar_oil_name() {
            return car_oil_name;
        }

        public void setCar_oil_name(String car_oil_name) {
            this.car_oil_name = car_oil_name;
        }

        public String getCar_oil_price() {
            return car_oil_price;
        }

        public void setCar_oil_price(String car_oil_price) {
            this.car_oil_price = car_oil_price;
        }

        public int getCar_engine_id() {
            return car_engine_id;
        }

        public void setCar_engine_id(int car_engine_id) {
            this.car_engine_id = car_engine_id;
        }

        public String getCar_engine_name() {
            return car_engine_name;
        }

        public void setCar_engine_name(String car_engine_name) {
            this.car_engine_name = car_engine_name;
        }

        public String getCar_num_containers() {
            return car_num_containers;
        }

        public void setCar_num_containers(String car_num_containers) {
            this.car_num_containers = car_num_containers;
        }

        public String getCar_viscosity_degree() {
            return car_viscosity_degree;
        }

        public void setCar_viscosity_degree(String car_viscosity_degree) {
            this.car_viscosity_degree = car_viscosity_degree;
        }

        public String getNumber_of_month_to_change_oil() {
            return number_of_month_to_change_oil;
        }

        public void setNumber_of_month_to_change_oil(String number_of_month_to_change_oil) {
            this.number_of_month_to_change_oil = number_of_month_to_change_oil;
        }

        public String getCar_oil_expired_date() {
            return car_oil_expired_date;
        }

        public void setCar_oil_expired_date(String car_oil_expired_date) {
            this.car_oil_expired_date = car_oil_expired_date;
        }

        public String getCar_oil_expired_time() {
            return car_oil_expired_time;
        }

        public void setCar_oil_expired_time(String car_oil_expired_time) {
            this.car_oil_expired_time = car_oil_expired_time;
        }

        public String getCar_oil_expired_date_time() {
            return car_oil_expired_date_time;
        }

        public void setCar_oil_expired_date_time(String car_oil_expired_date_time) {
            this.car_oil_expired_date_time = car_oil_expired_date_time;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(car_brand_id);
            dest.writeString(car_brand_name);
            dest.writeString(car_model_year);
            dest.writeString(car_plate_number);
            dest.writeInt(car_oil_id);
            dest.writeString(car_oil_name);
            dest.writeString(car_oil_price);
            dest.writeInt(car_engine_id);
            dest.writeString(car_engine_name);
            dest.writeString(car_num_containers);
            dest.writeString(car_viscosity_degree);
            dest.writeString(number_of_month_to_change_oil);
            dest.writeString(car_oil_expired_date);
            dest.writeString(car_oil_expired_time);
            dest.writeString(car_oil_expired_date_time);
        }
    }
}
