package com.aait.expresss.Models;

public class CarAddUpdateModel {

    /**
     * key : 1
     * massage : تم الحفظ بنجاح
     * data : {"id":394,"car_owner_id":"34","car_owner_name":"التجربة","car_brand_id":"1","car_brand_name":"هيونداي","car_model_year":"2018","car_plate_number":"123321","car_oil_id":"1","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_id":"1","car_engine_name":"محرك ديزل","car_num_containers":"1231","car_viscosity_degree":"1231","number_of_month_to_change_oil":"5","car_oil_expired":"2018/07/29 03:16:24"}
     */

    private String key;
    private String massage;
    private CarAddData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public CarAddData getData() {
        return data;
    }

    public void setData(CarAddData data) {
        this.data = data;
    }

    public static class CarAddData {
        /**
         * id : 394
         * car_owner_id : 34
         * car_owner_name : التجربة
         * car_brand_id : 1
         * car_brand_name : هيونداي
         * car_model_year : 2018
         * car_plate_number : 123321
         * car_oil_id : 1
         * car_oil_name : زيت ماتور
         * car_oil_price : 110
         * car_engine_id : 1
         * car_engine_name : محرك ديزل
         * car_num_containers : 1231
         * car_viscosity_degree : 1231
         * number_of_month_to_change_oil : 5
         * car_oil_expired : 2018/07/29 03:16:24
         */

        private int id;
        private String car_owner_id;
        private String car_owner_name;
        private String car_brand_id;
        private String car_brand_name;
        private String car_model_year;
        private String car_plate_number;
        private String car_oil_id;
        private String car_oil_name;
        private String car_oil_price;
        private String car_engine_id;
        private String car_engine_name;
        private String car_num_containers;
        private String car_viscosity_degree;
        private String number_of_month_to_change_oil;
        private String car_oil_expired;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCar_owner_id() {
            return car_owner_id;
        }

        public void setCar_owner_id(String car_owner_id) {
            this.car_owner_id = car_owner_id;
        }

        public String getCar_owner_name() {
            return car_owner_name;
        }

        public void setCar_owner_name(String car_owner_name) {
            this.car_owner_name = car_owner_name;
        }

        public String getCar_brand_id() {
            return car_brand_id;
        }

        public void setCar_brand_id(String car_brand_id) {
            this.car_brand_id = car_brand_id;
        }

        public String getCar_brand_name() {
            return car_brand_name;
        }

        public void setCar_brand_name(String car_brand_name) {
            this.car_brand_name = car_brand_name;
        }

        public String getCar_model_year() {
            return car_model_year;
        }

        public void setCar_model_year(String car_model_year) {
            this.car_model_year = car_model_year;
        }

        public String getCar_plate_number() {
            return car_plate_number;
        }

        public void setCar_plate_number(String car_plate_number) {
            this.car_plate_number = car_plate_number;
        }

        public String getCar_oil_id() {
            return car_oil_id;
        }

        public void setCar_oil_id(String car_oil_id) {
            this.car_oil_id = car_oil_id;
        }

        public String getCar_oil_name() {
            return car_oil_name;
        }

        public void setCar_oil_name(String car_oil_name) {
            this.car_oil_name = car_oil_name;
        }

        public String getCar_oil_price() {
            return car_oil_price;
        }

        public void setCar_oil_price(String car_oil_price) {
            this.car_oil_price = car_oil_price;
        }

        public String getCar_engine_id() {
            return car_engine_id;
        }

        public void setCar_engine_id(String car_engine_id) {
            this.car_engine_id = car_engine_id;
        }

        public String getCar_engine_name() {
            return car_engine_name;
        }

        public void setCar_engine_name(String car_engine_name) {
            this.car_engine_name = car_engine_name;
        }

        public String getCar_num_containers() {
            return car_num_containers;
        }

        public void setCar_num_containers(String car_num_containers) {
            this.car_num_containers = car_num_containers;
        }

        public String getCar_viscosity_degree() {
            return car_viscosity_degree;
        }

        public void setCar_viscosity_degree(String car_viscosity_degree) {
            this.car_viscosity_degree = car_viscosity_degree;
        }

        public String getNumber_of_month_to_change_oil() {
            return number_of_month_to_change_oil;
        }

        public void setNumber_of_month_to_change_oil(String number_of_month_to_change_oil) {
            this.number_of_month_to_change_oil = number_of_month_to_change_oil;
        }

        public String getCar_oil_expired() {
            return car_oil_expired;
        }

        public void setCar_oil_expired(String car_oil_expired) {
            this.car_oil_expired = car_oil_expired;
        }
    }
}
