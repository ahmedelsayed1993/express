package com.aait.expresss.Models;

public class ContactInfoModel {

    /**
     * key : 1
     * massage : تم  بنجاح
     * data : {"site_email":"tets@info.com","site_phone":"0961234578","site_address":"السعودية","site_facebook":"https://www.facebook.com","site_twitter":"https://www.twitter.com","site_google":"https://plus.google.com"}
     */

    private String key;
    private String massage;
    private ContactData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public ContactData getData() {
        return data;
    }

    public void setData(ContactData data) {
        this.data = data;
    }

    public static class ContactData {
        /**
         * site_email : tets@info.com
         * site_phone : 0961234578
         * site_address : السعودية
         * site_facebook : https://www.facebook.com
         * site_twitter : https://www.twitter.com
         * site_google : https://plus.google.com
         */

        private String site_email;
        private String site_phone;
        private String site_address;
        private String site_facebook;
        private String site_twitter;
        private String site_google;

        public String getSite_email() {
            return site_email;
        }

        public void setSite_email(String site_email) {
            this.site_email = site_email;
        }

        public String getSite_phone() {
            return site_phone;
        }

        public void setSite_phone(String site_phone) {
            this.site_phone = site_phone;
        }

        public String getSite_address() {
            return site_address;
        }

        public void setSite_address(String site_address) {
            this.site_address = site_address;
        }

        public String getSite_facebook() {
            return site_facebook;
        }

        public void setSite_facebook(String site_facebook) {
            this.site_facebook = site_facebook;
        }

        public String getSite_twitter() {
            return site_twitter;
        }

        public void setSite_twitter(String site_twitter) {
            this.site_twitter = site_twitter;
        }

        public String getSite_google() {
            return site_google;
        }

        public void setSite_google(String site_google) {
            this.site_google = site_google;
        }
    }
}
