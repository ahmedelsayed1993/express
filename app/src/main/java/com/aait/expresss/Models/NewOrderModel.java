package com.aait.expresss.Models;

import java.util.List;

public class NewOrderModel {

    /**
     * key : 1
     * massage : تم  بنجاح
     * data : [{"id":540,"car_owner_id":"92","car_owner_name":"محمد","car_brand_name":"هيونداي","car_model_year":"64888","car_plate_number":"نسخ نة","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"محرك ديزل","car_num_containers":"88","car_viscosity_degree":"رامي ٦٦٦","number_of_month_to_change_oil":"21","car_oil_expired":"2018/10/12 05:58:53"},{"id":541,"car_owner_id":"92","car_owner_name":"محمد","car_brand_name":"هيونداي","car_model_year":"5555","car_plate_number":"ثنتين ٤٤","car_oil_name":"زيت ماتور","car_oil_price":"110","car_engine_name":"توربينو","car_num_containers":"5553","car_viscosity_degree":"راسلني ٩٨","number_of_month_to_change_oil":"12","car_oil_expired":"2018/10/12 06:33:05"}]
     */

    private String key;
    private String massage;
    private List<NewOrderData> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<NewOrderData> getData() {
        return data;
    }

    public void setData(List<NewOrderData> data) {
        this.data = data;
    }

    public static class NewOrderData {
        /**
         * id : 540
         * car_owner_id : 92
         * car_owner_name : محمد
         * car_brand_name : هيونداي
         * car_model_year : 64888
         * car_plate_number : نسخ نة
         * car_oil_name : زيت ماتور
         * car_oil_price : 110
         * car_engine_name : محرك ديزل
         * car_num_containers : 88
         * car_viscosity_degree : رامي ٦٦٦
         * number_of_month_to_change_oil : 21
         * car_oil_expired : 2018/10/12 05:58:53
         */

        private int id;
        private String car_owner_id;
        private String car_owner_name;
        private String car_brand_name;
        private String car_model_year;
        private String car_plate_number;
        private String car_oil_name;
        private String car_oil_price;
        private String car_engine_name;
        private String car_num_containers;
        private String car_viscosity_degree;
        private String number_of_month_to_change_oil;
        private String car_oil_expired;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCar_owner_id() {
            return car_owner_id;
        }

        public void setCar_owner_id(String car_owner_id) {
            this.car_owner_id = car_owner_id;
        }

        public String getCar_owner_name() {
            return car_owner_name;
        }

        public void setCar_owner_name(String car_owner_name) {
            this.car_owner_name = car_owner_name;
        }

        public String getCar_brand_name() {
            return car_brand_name;
        }

        public void setCar_brand_name(String car_brand_name) {
            this.car_brand_name = car_brand_name;
        }

        public String getCar_model_year() {
            return car_model_year;
        }

        public void setCar_model_year(String car_model_year) {
            this.car_model_year = car_model_year;
        }

        public String getCar_plate_number() {
            return car_plate_number;
        }

        public void setCar_plate_number(String car_plate_number) {
            this.car_plate_number = car_plate_number;
        }

        public String getCar_oil_name() {
            return car_oil_name;
        }

        public void setCar_oil_name(String car_oil_name) {
            this.car_oil_name = car_oil_name;
        }

        public String getCar_oil_price() {
            return car_oil_price;
        }

        public void setCar_oil_price(String car_oil_price) {
            this.car_oil_price = car_oil_price;
        }

        public String getCar_engine_name() {
            return car_engine_name;
        }

        public void setCar_engine_name(String car_engine_name) {
            this.car_engine_name = car_engine_name;
        }

        public String getCar_num_containers() {
            return car_num_containers;
        }

        public void setCar_num_containers(String car_num_containers) {
            this.car_num_containers = car_num_containers;
        }

        public String getCar_viscosity_degree() {
            return car_viscosity_degree;
        }

        public void setCar_viscosity_degree(String car_viscosity_degree) {
            this.car_viscosity_degree = car_viscosity_degree;
        }

        public String getNumber_of_month_to_change_oil() {
            return number_of_month_to_change_oil;
        }

        public void setNumber_of_month_to_change_oil(String number_of_month_to_change_oil) {
            this.number_of_month_to_change_oil = number_of_month_to_change_oil;
        }

        public String getCar_oil_expired() {
            return car_oil_expired;
        }

        public void setCar_oil_expired(String car_oil_expired) {
            this.car_oil_expired = car_oil_expired;
        }
    }
}
