package com.aait.expresss.Models;

public class SetNewPasswordModel {

    /**
     * key : 1
     * massage : تم الحفظ بنجاح
     * data : {"id":34,"name":"التجربة","email":"admin@admin.com","phone":"0963658598","lat":"24.7106567590028","lng":"46.67541461328119","address":"","delegate":0,"code":"1111","app_lang":"ar","notifications":"1","avatar":"default.png","arrears":"0","active":1,"role":0,"device_id":"","avatar_path":"https://no3manpro.arabsdesign.com/zeat/public/1.png"}
     */

    private String key;
    private String massage;
    private DataBean data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 34
         * name : التجربة
         * email : admin@admin.com
         * phone : 0963658598
         * lat : 24.7106567590028
         * lng : 46.67541461328119
         * address :
         * delegate : 0
         * code : 1111
         * app_lang : ar
         * notifications : 1
         * avatar : default.png
         * arrears : 0
         * active : 1
         * role : 0
         * device_id :
         * avatar_path : https://no3manpro.arabsdesign.com/zeat/public/1.png
         */

        private int id;
        private String name;
        private String email;
        private String phone;
        private String lat;
        private String lng;
        private String address;
        private int delegate;
        private String code;
        private String app_lang;
        private String notifications;
        private String avatar;
        private String arrears;
        private int active;
        private int role;
        private String device_id;
        private String avatar_path;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getDelegate() {
            return delegate;
        }

        public void setDelegate(int delegate) {
            this.delegate = delegate;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getApp_lang() {
            return app_lang;
        }

        public void setApp_lang(String app_lang) {
            this.app_lang = app_lang;
        }

        public String getNotifications() {
            return notifications;
        }

        public void setNotifications(String notifications) {
            this.notifications = notifications;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getArrears() {
            return arrears;
        }

        public void setArrears(String arrears) {
            this.arrears = arrears;
        }

        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
        }

        public int getRole() {
            return role;
        }

        public void setRole(int role) {
            this.role = role;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getAvatar_path() {
            return avatar_path;
        }

        public void setAvatar_path(String avatar_path) {
            this.avatar_path = avatar_path;
        }
    }
}
