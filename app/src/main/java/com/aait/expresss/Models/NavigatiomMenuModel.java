package com.aait.expresss.Models;

public class NavigatiomMenuModel {
    String title;
    int imageId;

    public NavigatiomMenuModel(String title, int imageId) {
        this.title = title;
        this.imageId = imageId;
    }

    public NavigatiomMenuModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
