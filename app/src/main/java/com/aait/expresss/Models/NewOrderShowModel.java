package com.aait.expresss.Models;

import java.util.List;

public class NewOrderShowModel {

    /**
     * key : 1
     * massage : تم الحفظ بنجاح
     * data : {"order_number":513,"order_car_count":2,"order_time":"10:02 PM","order_date":"2019-03-01","car_owner_name":"Express2","car_owner_address":"","car_owner_lat":"24.831609200349995","car_owner_lng":"46.613616517578066","cars_data":[{"car_number":775,"car_brand_name":"تويوتا","car_engine_name":"محرك بنزين","car_oil_name":"فوكس","car_oil_price":"165","car_model_year":2018,"car_plate_number":"233","car_viscosity_degree":"34"},{"car_number":777,"car_brand_name":"تويوتا","car_engine_name":"محرك بنزين","car_oil_name":"فوكس","car_oil_price":"165","car_model_year":2018,"car_plate_number":"233","car_viscosity_degree":"34"}],"services":[{"id":16,"name":"خدمة الإطارات","price":"10"},{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"}],"services_price":40,"order_total_price":370}
     */

    private String key;
    private String massage;
    private DataBean data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order_number : 513
         * order_car_count : 2
         * order_time : 10:02 PM
         * order_date : 2019-03-01
         * car_owner_name : Express2
         * car_owner_address :
         * car_owner_lat : 24.831609200349995
         * car_owner_lng : 46.613616517578066
         * cars_data : [{"car_number":775,"car_brand_name":"تويوتا","car_engine_name":"محرك بنزين","car_oil_name":"فوكس","car_oil_price":"165","car_model_year":2018,"car_plate_number":"233","car_viscosity_degree":"34"},{"car_number":777,"car_brand_name":"تويوتا","car_engine_name":"محرك بنزين","car_oil_name":"فوكس","car_oil_price":"165","car_model_year":2018,"car_plate_number":"233","car_viscosity_degree":"34"}]
         * services : [{"id":16,"name":"خدمة الإطارات","price":"10"},{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"}]
         * services_price : 40
         * order_total_price : 370
         */

        private int order_number;
        private int order_car_count;
        private String order_time;
        private String order_date;
        private String car_owner_name;
        private String car_owner_address;
        private String car_owner_lat;
        private String car_owner_lng;
        private int services_price;
        private int order_total_price;
        private List<CarsData> cars_data;
        private List<ServicesData> services;

        public int getOrder_number() {
            return order_number;
        }

        public void setOrder_number(int order_number) {
            this.order_number = order_number;
        }

        public int getOrder_car_count() {
            return order_car_count;
        }

        public void setOrder_car_count(int order_car_count) {
            this.order_car_count = order_car_count;
        }

        public String getOrder_time() {
            return order_time;
        }

        public void setOrder_time(String order_time) {
            this.order_time = order_time;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getCar_owner_name() {
            return car_owner_name;
        }

        public void setCar_owner_name(String car_owner_name) {
            this.car_owner_name = car_owner_name;
        }

        public String getCar_owner_address() {
            return car_owner_address;
        }

        public void setCar_owner_address(String car_owner_address) {
            this.car_owner_address = car_owner_address;
        }

        public String getCar_owner_lat() {
            return car_owner_lat;
        }

        public void setCar_owner_lat(String car_owner_lat) {
            this.car_owner_lat = car_owner_lat;
        }

        public String getCar_owner_lng() {
            return car_owner_lng;
        }

        public void setCar_owner_lng(String car_owner_lng) {
            this.car_owner_lng = car_owner_lng;
        }

        public int getServices_price() {
            return services_price;
        }

        public void setServices_price(int services_price) {
            this.services_price = services_price;
        }

        public int getOrder_total_price() {
            return order_total_price;
        }

        public void setOrder_total_price(int order_total_price) {
            this.order_total_price = order_total_price;
        }

        public List<CarsData> getCars_data() {
            return cars_data;
        }

        public void setCars_data(List<CarsData> cars_data) {
            this.cars_data = cars_data;
        }

        public List<ServicesData> getServices() {
            return services;
        }

        public void setServices(List<ServicesData> services) {
            this.services = services;
        }

        public static class CarsData {
            /**
             * car_number : 775
             * car_brand_name : تويوتا
             * car_engine_name : محرك بنزين
             * car_oil_name : فوكس
             * car_oil_price : 165
             * car_model_year : 2018
             * car_plate_number : 233
             * car_viscosity_degree : 34
             */

            private int car_number;
            private String car_brand_name;
            private String car_engine_name;
            private String car_oil_name;
            private String car_oil_price;
            private int car_model_year;
            private String car_plate_number;
            private String car_viscosity_degree;

            public int getCar_number() {
                return car_number;
            }

            public void setCar_number(int car_number) {
                this.car_number = car_number;
            }

            public String getCar_brand_name() {
                return car_brand_name;
            }

            public void setCar_brand_name(String car_brand_name) {
                this.car_brand_name = car_brand_name;
            }

            public String getCar_engine_name() {
                return car_engine_name;
            }

            public void setCar_engine_name(String car_engine_name) {
                this.car_engine_name = car_engine_name;
            }

            public String getCar_oil_name() {
                return car_oil_name;
            }

            public void setCar_oil_name(String car_oil_name) {
                this.car_oil_name = car_oil_name;
            }

            public String getCar_oil_price() {
                return car_oil_price;
            }

            public void setCar_oil_price(String car_oil_price) {
                this.car_oil_price = car_oil_price;
            }

            public int getCar_model_year() {
                return car_model_year;
            }

            public void setCar_model_year(int car_model_year) {
                this.car_model_year = car_model_year;
            }

            public String getCar_plate_number() {
                return car_plate_number;
            }

            public void setCar_plate_number(String car_plate_number) {
                this.car_plate_number = car_plate_number;
            }

            public String getCar_viscosity_degree() {
                return car_viscosity_degree;
            }

            public void setCar_viscosity_degree(String car_viscosity_degree) {
                this.car_viscosity_degree = car_viscosity_degree;
            }
        }

        public static class ServicesData {
            /**
             * id : 16
             * name : خدمة الإطارات
             * price : 10
             */

            private int id;
            private String name;
            private String price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }
    }
}
