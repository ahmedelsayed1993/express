package com.aait.expresss.Models;

import java.util.List;

public class OptionalServiceModel {

    /**
     * key : 1
     * massage : تم الحفظ بنجاح
     * data : [{"parent":{"id":12,"name":"خدمة الفلاتر"},"checkboxes":[{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"},{"id":19,"name":"فلتر ديزل","price":"35"},{"id":20,"name":"صفاية تانكي الديزل","price":"0000"},{"id":21,"name":"غسيل بالبخار سيارة صغيرة","price":"00000"},{"id":23,"name":"غسيل بالبخار سيارة متوسطة","price":"0000"},{"id":26,"name":"غسيل بالبخار سيارة كبيرة","price":"0000"}],"texts":[{"id":4,"name":"الأمبير","price":"10"}],"dropdowns":[{"id":3,"name":"اندونيسيا","price":"5"},{"id":11,"name":"برجستون","price":"333"}]},{"parent":{"id":16,"name":"خدمة الإطارات"},"checkboxes":[{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"}],"texts":[{"id":1,"name":"المقاس","price":"10"}],"dropdowns":[{"id":2,"name":"الماركة"},[{"id":3,"name":"اندونيسيا","price":"5"},{"id":11,"name":"برجستون","price":"333"},{"id":12,"name":"ميشلان","price":"5555"},{"id":13,"name":"سوموتومو","price":"000000"},{"id":14,"name":"تويو","price":"00000"},{"id":15,"name":"فالكن","price":"00000"},{"id":16,"name":"ماكسس","price":"00000"},{"id":17,"name":"كمهو","price":"00000"},{"id":18,"name":"هنكوك","price":"00000"},{"id":19,"name":"ريديسون","price":"000000"},{"id":20,"name":"ريديسون","price":"000000"},{"id":21,"name":"فايرستون","price":"00000"},{"id":22,"name":"تيتو","price":"00000"},{"id":23,"name":"كونترينتال","price":"000000"},{"id":24,"name":"بريلي","price":"0000"},{"id":25,"name":"فودير","price":"0000"},{"id":26,"name":"بي دي اف قودريش","price":"00000"},{"id":27,"name":"صيني","price":"00000"},{"id":28,"name":"برجستون 210/50-17","price":"3000"}],{"id":3,"name":"الصناعة"},[{"id":3,"name":"اندونيسيا","price":"5"},{"id":11,"name":"برجستون","price":"333"},{"id":12,"name":"ميشلان","price":"5555"},{"id":13,"name":"سوموتومو","price":"000000"},{"id":14,"name":"تويو","price":"00000"},{"id":15,"name":"فالكن","price":"00000"},{"id":16,"name":"ماكسس","price":"00000"},{"id":17,"name":"كمهو","price":"00000"},{"id":18,"name":"هنكوك","price":"00000"},{"id":19,"name":"ريديسون","price":"000000"},{"id":20,"name":"ريديسون","price":"000000"},{"id":21,"name":"فايرستون","price":"00000"},{"id":22,"name":"تيتو","price":"00000"},{"id":23,"name":"كونترينتال","price":"000000"},{"id":24,"name":"بريلي","price":"0000"},{"id":25,"name":"فودير","price":"0000"},{"id":26,"name":"بي دي اف قودريش","price":"00000"},{"id":27,"name":"صيني","price":"00000"},{"id":28,"name":"برجستون 210/50-17","price":"3000"},{"id":1,"name":"ياباني","price":"10"},{"id":2,"name":"صيني","price":"5"},{"id":5,"name":"امريكي","price":"3"},{"id":6,"name":"ماليزي","price":"5"},{"id":7,"name":"الماني","price":"6"},{"id":8,"name":"تايلندي","price":"7"},{"id":9,"name":"فرنسي","price":"8"},{"id":10,"name":"اوربي","price":"6"}]]},{"parent":{"id":17,"name":"خدمة البطاريات"},"checkboxes":[{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"}],"texts":[{"id":4,"name":"الأمبير","price":"10"}],"dropdowns":[{"id":3,"name":"اندونيسيا","price":"5"},{"id":11,"name":"برجستون","price":"333"}]}]
     */

    private String key;
    private String massage;
    private List<DataBean> data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * parent : {"id":12,"name":"خدمة الفلاتر"}
         * checkboxes : [{"id":13,"name":"فلتر هواء","price":"10"},{"id":14,"name":"فلتر زيت","price":"10"},{"id":15,"name":"فلتر مكيف","price":"10"},{"id":19,"name":"فلتر ديزل","price":"35"},{"id":20,"name":"صفاية تانكي الديزل","price":"0000"},{"id":21,"name":"غسيل بالبخار سيارة صغيرة","price":"00000"},{"id":23,"name":"غسيل بالبخار سيارة متوسطة","price":"0000"},{"id":26,"name":"غسيل بالبخار سيارة كبيرة","price":"0000"}]
         * texts : [{"id":4,"name":"الأمبير","price":"10"}]
         * dropdowns : [{"id":3,"name":"اندونيسيا","price":"5"},{"id":11,"name":"برجستون","price":"333"}]
         */

        private ParentBean parent;
        private List<CheckboxesBean> checkboxes;
        private List<TextsBean> texts;
        private List<DropdownsBean> dropdowns;

        public ParentBean getParent() {
            return parent;
        }

        public void setParent(ParentBean parent) {
            this.parent = parent;
        }

        public List<CheckboxesBean> getCheckboxes() {
            return checkboxes;
        }

        public void setCheckboxes(List<CheckboxesBean> checkboxes) {
            this.checkboxes = checkboxes;
        }

        public List<TextsBean> getTexts() {
            return texts;
        }

        public void setTexts(List<TextsBean> texts) {
            this.texts = texts;
        }

        public List<DropdownsBean> getDropdowns() {
            return dropdowns;
        }

        public void setDropdowns(List<DropdownsBean> dropdowns) {
            this.dropdowns = dropdowns;
        }

        public static class ParentBean {
            /**
             * id : 12
             * name : خدمة الفلاتر
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class CheckboxesBean {
            /**
             * id : 13
             * name : فلتر هواء
             * price : 10
             */

            private int id;
            private String name;
            private String price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }

        public static class TextsBean {
            /**
             * id : 4
             * name : الأمبير
             * price : 10
             */

            private int id;
            private String name;
            private String price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }

        public static class DropdownsBean {
            /**
             * id : 3
             * name : اندونيسيا
             * price : 5
             */

            private int id;
            private String name;
            private String price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }
    }
}
