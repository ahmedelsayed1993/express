package com.aait.expresss.Models;

import java.util.List;

public class FollowOrderModel {

    /**
     * key : 1
     * massage : تم  بنجاح
     * data : {"id":306,"client_lat":"16.992580956484225","client_lng":"30.976866111159325","order_status":[{"status_message":"تم أرسال الطلب","status_time":"04:04 PM","status_date":"2018-12-26"}],"order_data":{"order_number":306,"order_car_count":2,"order_total_price":25.6,"order_status":"تم أرسال الطلب","order_time":"04:12 PM","order_date":"2018-12-26","car_owner_name":"ctest1","car_owner_address":"","car_owner_lat":"16.992580956484225","car_owner_lng":"30.976866111159325","cars_data":[{"car_number":1,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"توتال","car_oil_price":"12.80","car_model_year":2015,"car_plate_number":"esr123","car_viscosity_degree":"44"},{"car_number":2,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"توتال","car_oil_price":"12.80","car_model_year":2050,"car_plate_number":"vg2345","car_viscosity_degree":"v70"}]}}
     */

    private String key;
    private String massage;
    private FollowData data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public FollowData getData() {
        return data;
    }

    public void setData(FollowData data) {
        this.data = data;
    }

    public static class FollowData {
        /**
         * id : 306
         * client_lat : 16.992580956484225
         * client_lng : 30.976866111159325
         * order_status : [{"status_message":"تم أرسال الطلب","status_time":"04:04 PM","status_date":"2018-12-26"}]
         * order_data : {"order_number":306,"order_car_count":2,"order_total_price":25.6,"order_status":"تم أرسال الطلب","order_time":"04:12 PM","order_date":"2018-12-26","car_owner_name":"ctest1","car_owner_address":"","car_owner_lat":"16.992580956484225","car_owner_lng":"30.976866111159325","cars_data":[{"car_number":1,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"توتال","car_oil_price":"12.80","car_model_year":2015,"car_plate_number":"esr123","car_viscosity_degree":"44"},{"car_number":2,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"توتال","car_oil_price":"12.80","car_model_year":2050,"car_plate_number":"vg2345","car_viscosity_degree":"v70"}]}
         */

        private int id;
        private String client_lat;
        private String client_lng;
        private OrderData order_data;
        private List<OrderStatus> order_status;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getClient_lat() {
            return client_lat;
        }

        public void setClient_lat(String client_lat) {
            this.client_lat = client_lat;
        }

        public String getClient_lng() {
            return client_lng;
        }

        public void setClient_lng(String client_lng) {
            this.client_lng = client_lng;
        }

        public OrderData getOrder_data() {
            return order_data;
        }

        public void setOrder_data(OrderData order_data) {
            this.order_data = order_data;
        }

        public List<OrderStatus> getOrder_status() {
            return order_status;
        }

        public void setOrder_status(List<OrderStatus> order_status) {
            this.order_status = order_status;
        }

        public static class OrderData {
            /**
             * order_number : 306
             * order_car_count : 2
             * order_total_price : 25.6
             * order_status : تم أرسال الطلب
             * order_time : 04:12 PM
             * order_date : 2018-12-26
             * car_owner_name : ctest1
             * car_owner_address :
             * car_owner_lat : 16.992580956484225
             * car_owner_lng : 30.976866111159325
             * cars_data : [{"car_number":1,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"توتال","car_oil_price":"12.80","car_model_year":2015,"car_plate_number":"esr123","car_viscosity_degree":"44"},{"car_number":2,"car_brand_name":"هيونداي","car_engine_name":"محرك ديزل","car_oil_name":"توتال","car_oil_price":"12.80","car_model_year":2050,"car_plate_number":"vg2345","car_viscosity_degree":"v70"}]
             */

            private int order_number;
            private int order_car_count;
            private double order_total_price;
            private String order_status;
            private String order_time;
            private String order_date;
            private String car_owner_name;
            private String car_owner_address;
            private String car_owner_lat;
            private String car_owner_lng;
            private List<CarsData> cars_data;

            public int getOrder_number() {
                return order_number;
            }

            public void setOrder_number(int order_number) {
                this.order_number = order_number;
            }

            public int getOrder_car_count() {
                return order_car_count;
            }

            public void setOrder_car_count(int order_car_count) {
                this.order_car_count = order_car_count;
            }

            public double getOrder_total_price() {
                return order_total_price;
            }

            public void setOrder_total_price(double order_total_price) {
                this.order_total_price = order_total_price;
            }

            public String getOrder_status() {
                return order_status;
            }

            public void setOrder_status(String order_status) {
                this.order_status = order_status;
            }

            public String getOrder_time() {
                return order_time;
            }

            public void setOrder_time(String order_time) {
                this.order_time = order_time;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }

            public String getCar_owner_name() {
                return car_owner_name;
            }

            public void setCar_owner_name(String car_owner_name) {
                this.car_owner_name = car_owner_name;
            }

            public String getCar_owner_address() {
                return car_owner_address;
            }

            public void setCar_owner_address(String car_owner_address) {
                this.car_owner_address = car_owner_address;
            }

            public String getCar_owner_lat() {
                return car_owner_lat;
            }

            public void setCar_owner_lat(String car_owner_lat) {
                this.car_owner_lat = car_owner_lat;
            }

            public String getCar_owner_lng() {
                return car_owner_lng;
            }

            public void setCar_owner_lng(String car_owner_lng) {
                this.car_owner_lng = car_owner_lng;
            }

            public List<CarsData> getCars_data() {
                return cars_data;
            }

            public void setCars_data(List<CarsData> cars_data) {
                this.cars_data = cars_data;
            }

            public static class CarsData {
                /**
                 * car_number : 1
                 * car_brand_name : هيونداي
                 * car_engine_name : محرك ديزل
                 * car_oil_name : توتال
                 * car_oil_price : 12.80
                 * car_model_year : 2015
                 * car_plate_number : esr123
                 * car_viscosity_degree : 44
                 */

                private int car_number;
                private String car_brand_name;
                private String car_engine_name;
                private String car_oil_name;
                private String car_oil_price;
                private int car_model_year;
                private String car_plate_number;
                private String car_viscosity_degree;

                public int getCar_number() {
                    return car_number;
                }

                public void setCar_number(int car_number) {
                    this.car_number = car_number;
                }

                public String getCar_brand_name() {
                    return car_brand_name;
                }

                public void setCar_brand_name(String car_brand_name) {
                    this.car_brand_name = car_brand_name;
                }

                public String getCar_engine_name() {
                    return car_engine_name;
                }

                public void setCar_engine_name(String car_engine_name) {
                    this.car_engine_name = car_engine_name;
                }

                public String getCar_oil_name() {
                    return car_oil_name;
                }

                public void setCar_oil_name(String car_oil_name) {
                    this.car_oil_name = car_oil_name;
                }

                public String getCar_oil_price() {
                    return car_oil_price;
                }

                public void setCar_oil_price(String car_oil_price) {
                    this.car_oil_price = car_oil_price;
                }

                public int getCar_model_year() {
                    return car_model_year;
                }

                public void setCar_model_year(int car_model_year) {
                    this.car_model_year = car_model_year;
                }

                public String getCar_plate_number() {
                    return car_plate_number;
                }

                public void setCar_plate_number(String car_plate_number) {
                    this.car_plate_number = car_plate_number;
                }

                public String getCar_viscosity_degree() {
                    return car_viscosity_degree;
                }

                public void setCar_viscosity_degree(String car_viscosity_degree) {
                    this.car_viscosity_degree = car_viscosity_degree;
                }
            }
        }

        public static class OrderStatus {
            /**
             * status_message : تم أرسال الطلب
             * status_time : 04:04 PM
             * status_date : 2018-12-26
             */

            private String status_message;
            private String status_time;
            private String status_date;

            public String getStatus_message() {
                return status_message;
            }

            public void setStatus_message(String status_message) {
                this.status_message = status_message;
            }

            public String getStatus_time() {
                return status_time;
            }

            public void setStatus_time(String status_time) {
                this.status_time = status_time;
            }

            public String getStatus_date() {
                return status_date;
            }

            public void setStatus_date(String status_date) {
                this.status_date = status_date;
            }
        }
    }
}
