package com.aait.expresss.Background;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.util.Log;

import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.Utils.GlobalPreference;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduledService extends Service {
    private Timer timer=new Timer();
    private GlobalPreference globalPreference;
    private FusedLocationProviderClient mFusedLocationClient;

    public ScheduledService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        globalPreference=GlobalPreference.getInstance(this);
        mFusedLocationClient= LocationServices.getFusedLocationProviderClient(this);
        timer.scheduleAtFixedRate(new TimerTask() {
            @SuppressLint("MissingPermission")
            @Override
            public void run() {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()){
                            RetrofitCall.getInstance().getApi().updateLatLng(globalPreference.getUser().getData().getId(),
                                    task.getResult().getLatitude(),task.getResult().getLongitude()).enqueue(new Callback<MainResponseModel>() {
                                @Override
                                public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                                    if (response.body().getKey().equals("1")){
                                        Log.e(getClass().getSimpleName(),"location send successfully");
                                    }
                                }

                                @Override
                                public void onFailure(Call<MainResponseModel> call, Throwable t) {

                                }
                            });
                        }
                    }
                });


            }
        }, 0, 1 * 60 * 1000);

    }



}
