package com.aait.expresss.FCM;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aait.expresss.R;
import com.aait.expresss.UI.Activity.MainActivity;
import com.aait.expresss.UI.Activity.NotificationsActivity;
import com.aait.expresss.UI.Activity.OrderWaitConfirmationActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.aait.expresss.UI.Activity.OrderWaitConfirmationActivity.ORDER_STATE;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private LocalBroadcastManager broadcaster;
    //Rec Var
    public static final String FROM_NOTIFICATION="from_notification";
    private static int value = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        broadcaster = LocalBroadcastManager.getInstance(this);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
      //  Util.onPrintLog(remoteMessage.getData());
       // Util.onPrintLog(remoteMessage.getNotification());
        if (remoteMessage.getData()!=null){
            if (remoteMessage.getData().get("user").equals("1")){
                sendBroadcastToDelegate(remoteMessage);
            }else {
                sendNotificationToClient(remoteMessage);
            }

        }

    }


    private void sendBroadcastToDelegate(RemoteMessage remoteMessage){
        Intent intent=new Intent(OrderWaitConfirmationActivity.IS_DONE);
        Bundle bundle=new Bundle();
        bundle.putBoolean(ORDER_STATE,Boolean.valueOf(remoteMessage.getData().get("done")));
        broadcaster.sendBroadcast(intent);

    }
    private void sendNotificationToClient(RemoteMessage  remoteMessage){
        String title=remoteMessage.getData().get("title");
        String msg=remoteMessage.getData().get("msg");
        Intent intent;
        if (MainActivity.allDataModel!=null){
            intent = new Intent(getApplicationContext(), NotificationsActivity.class);
        }else {
            intent = new Intent(getApplicationContext(), MainActivity.class);
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //globalPreference.startNotification(true);
        intent.putExtra(FROM_NOTIFICATION,true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 ,
                intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "NOTIFICATION_CHANNEL_NAME";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.logo)
                        .setContentTitle(title)
                        .setContentText(msg)
                        .setNumber(value)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        value++;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "NOTIFICATION_CHANNEL_NAME",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(true);
            channel.setLightColor(Color.DKGRAY);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(0 , notificationBuilder.build());

    }
}
