package com.aait.expresss.FCM;

import com.aait.expresss.Utils.GlobalPreference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import timber.log.Timber;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();


        GlobalPreference.getInstance(getApplicationContext());


        Timber.e( "sendRegistrationToServer: %s" , refreshedToken);
    }
}
