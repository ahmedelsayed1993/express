package com.aait.expresss.UI.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.os.Bundle;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.databinding.ActivityOrderWaittConfirmationBinding;

public class OrderWaitConfirmationActivity extends BaseActivity {

    ActivityOrderWaittConfirmationBinding waitConfirmationBinding;

    BroadcastReceiver mBroadcastReceiver;


    //Rec Var
    public static final String IS_DONE = "is_DONE";
    public static final String ORDER_STATE = "order_state";
    boolean orderState;

    @Override
    protected void init(Bundle savedInstanceState) {
        waitConfirmationBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getExtras() != null) {
                    orderState = intent.getExtras().getBoolean(ORDER_STATE);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE, true);
                    if (orderState) {

                        startActivity(new Intent(getApplicationContext(), OrderDoneActivity.class)
                                .putExtras(bundle)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    } else {
                        startActivity(new Intent(getApplicationContext(), DeleteOrderActivity.class)
                                .putExtras(bundle)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    }


                }
            }
        };


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mBroadcastReceiver, new IntentFilter(IS_DONE));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_order_waitt_confirmation;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
