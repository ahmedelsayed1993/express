package com.aait.expresss.UI.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.CarAddUpdateModel;
import com.aait.expresss.Models.CarsModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Spinner.BrandSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.EngineSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.MonthsTimeSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.OilSpinnerAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityCarEditBinding;

import java.util.Arrays;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CarEditActivity extends BaseActivity {

    ActivityCarEditBinding carEditBinding;

    GlobalPreference globalPreference;
    SweetAlertDialog progressDialog;


    //Spinners Adapters
    BrandSpinnerAdapter brandSpinnerAdapter;
    OilSpinnerAdapter oilSpinnerAdapter;
    EngineSpinnerAdapter engineSpinnerAdapter;
    MonthsTimeSpinnerAdapter monthsAdapter;
    //Rec Var
    CarsModel.CarsData carsData;
    int brandName;
    int oilType;
    int engineType;
    String monthsTime;

    @Override
    protected void init(Bundle savedInstanceState) {
        carEditBinding = DataBindingUtil.setContentView(this, getLayoutRes());
        if (getIntent().getExtras() != null) {
            carsData = getIntent().getExtras().getParcelable(CarsActivity.CAR_DATA);
        }

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));
        setupToolbarConfig();
        setUpSpinners();
        fetchOldData();

        carEditBinding.btnCarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    startUpdatingCar();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT, true).show();
                }
            }
        });


    }

    @SuppressLint("SetTextI18n")
    private void fetchOldData() {
        carEditBinding.etCarModel.setText(carsData.getCar_model_year() + "");
        carEditBinding.etPackageNumber.setText(carsData.getCar_num_containers() + "");
        //carEditBinding.etChangeOilTime.setText(carsData.getCar_oil_expired_time() + "");
        carEditBinding.etPlateNumber.setText(carsData.getCar_plate_number() + "");
        carEditBinding.etViscostyDegree.setText(carsData.getCar_viscosity_degree() + "");

        //set brand type
        for (int i = 0; i < MainActivity.allDataModel.getData().getBrand_data().size(); i++) {
            if (MainActivity.allDataModel.getData().getBrand_data().get(i).getName().equals(carsData.getCar_brand_name())) {
                carEditBinding.spinnerCarType.setSelection(i);
                break;


            }
        }

        //set oil type
        for (int i = 0; i < MainActivity.allDataModel.getData().getOil_data().size(); i++) {
            if (MainActivity.allDataModel.getData().getOil_data().get(i).getName().equals(carsData.getCar_oil_name())) {
                carEditBinding.spinnerOilType.setSelection(i);
                break;


            }
        }

        //set Engine Type
        for (int i = 0; i < MainActivity.allDataModel.getData().getEngine_data().size(); i++) {
            if (MainActivity.allDataModel.getData().getEngine_data().get(i).getName().equals(carsData.getCar_engine_name())) {
                carEditBinding.spinnerEngineType.setSelection(i);
                break;


            }
        }
        /*
        */
        //set left months
        List<String> months= Arrays.asList(getResources().getStringArray(R.array.months_array));
        String edit=carsData.getCar_oil_expired_time().substring(0,2);
        if (edit.startsWith("0")){
            edit=edit.replace("0","");
        }
        for (int i=0;i<months.size();i++){
            if (months.get(i).equals(edit)){
                carEditBinding.etChangeOilTime.setSelection(i);
            }
        }



    }

    private void startUpdatingCar() {
        if (Utils.checkError(carEditBinding.etCarModel)
                && Utils.checkError(carEditBinding.etPackageNumber)
                && Utils.checkError(carEditBinding.etPlateNumber)
                && Utils.checkError(carEditBinding.etViscostyDegree)) {

            if (monthsTime.equals(getString(R.string.time_to_change_oil))) {
                Toasty.warning(getApplicationContext(), getString(R.string.detect_time), Toast.LENGTH_SHORT, true).show();

            } else {
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi()
                        .updateCar(carsData.getId(), brandName, carEditBinding.etCarModel.getText().toString(),
                                carEditBinding.etPlateNumber.getText().toString(), oilType, engineType,
                                carEditBinding.etPackageNumber.getText().toString(), carEditBinding.etViscostyDegree.getText().toString(),
                                monthsTime, globalPreference.getAppLanguage())
                        .enqueue(new Callback<CarAddUpdateModel>() {
                            @Override
                            public void onResponse(Call<CarAddUpdateModel> call, Response<CarAddUpdateModel> response) {
                                if (response.body().getKey().equals("1")) {
                                    progressDialog.dismissWithAnimation();
                                    Toasty.success(getApplicationContext(), response.body().getMassage(),
                                            Toast.LENGTH_SHORT, true).show();
                                } else {
                                    progressDialog.hide();
                                    Toasty.error(getApplicationContext(), response.body().getMassage(),
                                            Toast.LENGTH_SHORT, true).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CarAddUpdateModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });
            }


        }
    }

    private void setUpSpinners() {


        brandSpinnerAdapter = new BrandSpinnerAdapter(this, R.layout.spinner_text, MainActivity.allDataModel.getData().getBrand_data());
        oilSpinnerAdapter = new OilSpinnerAdapter(this, R.layout.spinner_text, MainActivity.allDataModel.getData().getOil_data());
        engineSpinnerAdapter = new EngineSpinnerAdapter(this, R.layout.spinner_text, MainActivity.allDataModel.getData().getEngine_data());
        monthsAdapter = new MonthsTimeSpinnerAdapter(this, R.layout.spinner_text
                , Arrays.asList(getResources().getStringArray(R.array.months_array)));

        carEditBinding.spinnerCarType.setAdapter(brandSpinnerAdapter);
        carEditBinding.spinnerOilType.setAdapter(oilSpinnerAdapter);
        carEditBinding.spinnerEngineType.setAdapter(engineSpinnerAdapter);
        carEditBinding.etChangeOilTime.setAdapter(monthsAdapter);
        //get Brand Name
        carEditBinding.spinnerCarType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brandName = MainActivity.allDataModel.getData().getBrand_data().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //get Oil Type
        carEditBinding.spinnerOilType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                oilType = MainActivity.allDataModel.getData().getOil_data().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //get Engine type

        carEditBinding.spinnerEngineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                engineType = MainActivity.allDataModel.getData().getEngine_data().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //get months
        carEditBinding.etChangeOilTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthsTime = parent.getItemAtPosition(position).toString();
                //Toast.makeText(CarEditActivity.this, "" + monthsTime, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setupToolbarConfig() {
        setSupportActionBar(carEditBinding.toolbar.titleToolbar);
        carEditBinding.toolbar.tvTitle.setText(getString(R.string.update_car));
        carEditBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        carEditBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_car_edit;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        startActivity(new Intent(this, CarsActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, CarsActivity.class));
        finish();
    }
}
