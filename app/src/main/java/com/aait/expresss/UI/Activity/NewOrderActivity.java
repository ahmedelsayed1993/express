package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.NewOrderListener;
import com.aait.expresss.Models.NewOrderModel;
import com.aait.expresss.Models.OrderNowModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.NewOrderAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.NewOrderViewModel;
import com.aait.expresss.ViewModel.StoreNewOrderViewModel;
import com.aait.expresss.databinding.ActivityNewOrderBinding;
import com.thefinestartist.finestwebview.FinestWebView;
import com.thefinestartist.finestwebview.listeners.WebViewListener;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class NewOrderActivity extends BaseActivity implements NewOrderListener {
    ActivityNewOrderBinding newOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;
    StoreNewOrderViewModel storeNewOrderViewModel;

    NewOrderAdapter newOrderAdapter;

    //Rec Var
    List<String> ordersList;
    String order;
    public static final String CAR_ID="car_id";

    @Override
    protected void init(Bundle savedInstanceState) {

        newOrderBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        ordersList = new ArrayList<>();

        setupToolbarConfig();

        newOrderBinding.rvOrders.setLayoutManager(new GridLayoutManager(this, 2));

        fetchData();

        newOrderBinding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOrdering();
            }
        });

    }

    private void startOrdering() {
        if (ordersList.size()>0){
            order= TextUtils.join(",",ordersList);


            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            storeNewOrderViewModel=ViewModelProviders.of(this).get(StoreNewOrderViewModel.class);
            storeNewOrderViewModel.getOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),order,
                    globalPreference.getAppLanguage())
                    .observe(this, new Observer<OrderNowModel>() {
                        @Override
                        public void onChanged(@Nullable final OrderNowModel orderNowModel) {
                            progressDialog.dismiss();
                            String url="http://replaceexpress.com/complete-order/"+orderNowModel.getData().getOrder_number()+"";
                            final FinestWebView.Builder finestWebView= new FinestWebView.Builder(NewOrderActivity.this)
                                    .addWebViewListener(new WebViewListener() {
                                        @Override
                                        public void onProgressChanged(int progress) {
                                            super.onProgressChanged(progress);
                                        }

                                        @Override
                                        public void onReceivedTitle(String title) {
                                            super.onReceivedTitle(title);
                                        }

                                        @Override
                                        public void onReceivedTouchIconUrl(String url, boolean precomposed) {
                                            super.onReceivedTouchIconUrl(url, precomposed);
                                        }

                                        @Override
                                        public void onPageStarted(String url) {
                                            if (url.equals("http://replaceexpress.com/signIn")){
                                                Bundle bundle=new Bundle();
                                                bundle.putString(CAR_ID,String.valueOf(orderNowModel.getData().getOrder_number()));
                                                startActivity(new Intent(getApplicationContext(),OrderNowActivity.class)
                                                        .putExtras(bundle));
                                            }
                                            super.onPageStarted(url);
                                        }

                                        @Override
                                        public void onPageFinished(String url) {
                                            super.onPageFinished(url);
                                        }

                                        @Override
                                        public void onLoadResource(String url) {
                                            super.onLoadResource(url);
                                        }

                                        @Override
                                        public void onPageCommitVisible(String url) {
                                            super.onPageCommitVisible(url);
                                        }

                                        @Override
                                        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                                            super.onDownloadStart(url, userAgent, contentDisposition, mimeType, contentLength);
                                        }
                                    });

                            finestWebView.show(url);

                        }
                    });





        }else{
            Toasty.warning(getApplicationContext(),getString(R.string.select_one_order)
            ,Toast.LENGTH_SHORT,true).show();
        }
    }

    private void fetchData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();


        NewOrderViewModel viewModel = ViewModelProviders.of(this).get(NewOrderViewModel.class);
        viewModel.getNewOrderModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage())
                .observe(this, new Observer<NewOrderModel>() {
                    @Override
                    public void onChanged(@Nullable NewOrderModel newOrderModel) {
                        progressDialog.dismiss();

                        if (newOrderModel.getData().size() > 0) {

                            //Toast.makeText(NewOrderActivity.this, newOrderModel.getMassage(), Toast.LENGTH_SHORT).show();
                            newOrderAdapter = new NewOrderAdapter(newOrderModel.getData(), NewOrderActivity.this,
                                    NewOrderActivity.this);
                            newOrderBinding.rvOrders.setAdapter(newOrderAdapter);

                        } else {
                            progressDialog.hide();
                            newOrderBinding.btnNext.setVisibility(View.GONE);
                            startActivity(new Intent(NewOrderActivity.this,CarsActivity.class));
                            Toasty.warning(getApplicationContext(), getString(R.string.shld_have_one_car), Toast.LENGTH_SHORT, true).show();

                        }

                    }
                });
    }

    private void setupToolbarConfig() {
        setSupportActionBar(newOrderBinding.toolbar.titleToolbar);
        newOrderBinding.toolbar.tvTitle.setText(getString(R.string.new_order));
        newOrderBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        newOrderBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_new_order;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onNewOrderClickAdd(int id) {
        ordersList.add(String.valueOf(id));
    }

    @Override
    public void onNewOrderClickRemove(int id) {
        ordersList.remove(String.valueOf(id));
    }
}
