package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.CheckPhoneModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivitySendCodeBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SendCodeActivity extends BaseActivity {

    ActivitySendCodeBinding sendCodeBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;


    //Rec Var
    public static final String CHECK_PHONE_DATA="check_phone_data";


    @Override
    protected void init(Bundle savedInstanceState) {
        sendCodeBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        sendCodeBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(getApplicationContext())){
                    startSendActivationCode();
                }else{
                    Toasty.warning(getApplicationContext(),getString(R.string.check_internet_connection),Toast.LENGTH_SHORT,true).show();
                }

            }
        });
    }

    private void startSendActivationCode() {
        if (Utils.checkError(sendCodeBinding.etPhone)&&Utils.checkPhoneSize(sendCodeBinding.etPhone)){
            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi().checkPhone(sendCodeBinding.etPhone.getText().toString(),globalPreference.getAppLanguage())
            .enqueue(new Callback<CheckPhoneModel>() {
                @Override
                public void onResponse(Call<CheckPhoneModel> call, Response<CheckPhoneModel> response) {
                    if (response.body().getKey().equals("1")){
                        progressDialog.dismiss();
                        Bundle bundle=new Bundle();
                        bundle.putParcelable(CHECK_PHONE_DATA,response.body().getData());
                        Toasty.success(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                        startActivity(new Intent(getApplicationContext(),PhoneActivateCodeActivity.class)
                            .putExtras(bundle));


                    }else{
                        progressDialog.hide();
                        Toasty.error(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                    }

                }

                @Override
                public void onFailure(Call<CheckPhoneModel> call, Throwable t) {
                    Timber.wtf(t);
                }
            });
        }

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_send_code;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
