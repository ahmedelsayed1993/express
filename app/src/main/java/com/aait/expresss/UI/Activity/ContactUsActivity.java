package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.ContactInfoModel;
import com.aait.expresss.Models.SendContactModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityContactUsBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ContactUsActivity extends BaseActivity {

    ActivityContactUsBinding contactUsBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    @Override
    protected void init(Bundle savedInstanceState) {
        contactUsBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        contactUsBinding.etName.requestFocus();

        setUpToolbarConfig();

        getContactData();

        contactUsBinding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    setMessageToUs();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();
                }

            }
        });
    }

    private void setUpToolbarConfig() {
        setSupportActionBar(contactUsBinding.toolbar.titleToolbar);
        contactUsBinding.toolbar.tvTitle.setText(getString(R.string.contact_us));
        if (globalPreference.getUser().getData().getDelegate() == 1) {
            contactUsBinding.toolbar.ivNotify.setVisibility(View.GONE);
        } else {
            contactUsBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNotificationActivity();
                }
            });
        }
        contactUsBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });

    }

    private void setMessageToUs() {
        if (Utils.checkError(contactUsBinding.etName) && Utils.checkError(contactUsBinding.etMail) &&
                Utils.checkError(contactUsBinding.etMessage) && Utils.checkEmail(contactUsBinding.etMail)) {

            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi().sendMessageToUs(contactUsBinding.etName.getText().toString(),
                    contactUsBinding.etMail.getText().toString(), contactUsBinding.etMessage.getText().toString(),
                    globalPreference.getAppLanguage())
                    .enqueue(new Callback<SendContactModel>() {
                        @Override
                        public void onResponse(Call<SendContactModel> call, Response<SendContactModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismiss();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                            } else {
                                progressDialog.hide();
                                Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<SendContactModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });
        }
    }

    private void getContactData() {

        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        RetrofitCall.getInstance().getApi().getContacts()
                .enqueue(new Callback<ContactInfoModel>() {
                    @Override
                    public void onResponse(Call<ContactInfoModel> call, final Response<ContactInfoModel> response) {
                        if (response.body().getKey().equals("1")){
                            progressDialog.dismissWithAnimation();

                            contactUsBinding.tvMail.setText(response.body().getData().getSite_email());
                            contactUsBinding.tvPhone.setText(response.body().getData().getSite_phone());
                            contactUsBinding.tvAddress.setText(response.body().getData().getSite_address());

                            contactUsBinding.btnFace.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData().getSite_facebook())));

                                }
                            });
                            contactUsBinding.btnTwitter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData().getSite_twitter())));

                                }
                            });

                            contactUsBinding.btnGplus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getData().getSite_google())));
                                }
                            });

                        }else{
                            progressDialog.hide();
                            Toasty.error(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ContactInfoModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        finish();
        super.onBackPressed();
    }
}
