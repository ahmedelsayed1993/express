package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.OptionalServiceModel;
import com.aait.expresss.Models.OrderNowModel;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.OptionalServiceViewModel;
import com.aait.expresss.databinding.ActivityOptionalServiceBinding;
import com.google.gson.Gson;

import cn.pedant.SweetAlert.SweetAlertDialog;
import timber.log.Timber;

public class OptionalServiceActivity extends BaseActivity {

    private ActivityOptionalServiceBinding optionalServiceBinding;
    private OptionalServiceViewModel optionalServiceViewModel;
    private GlobalPreference globalPreference;
    private SweetAlertDialog progressDialog;

    //Rec Var
    int orderId;
    String carId;


    @Override
    protected void init(Bundle savedInstanceState) {


        initComponent();
        setupToolbarConfig();


        fetchData();







    }

    private void fetchData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        optionalServiceViewModel.getOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),carId,
                globalPreference.getAppLanguage()).observe(this, new Observer<OrderNowModel>() {
            @Override
            public void onChanged(@Nullable OrderNowModel orderNowModel) {
                orderId=orderNowModel.getData().getOrder_number();
                optionalServiceViewModel.getOptionalData(globalPreference.getAppLanguage())
                        .observe(OptionalServiceActivity.this, new Observer<OptionalServiceModel>() {
                            @Override
                            public void onChanged(@Nullable OptionalServiceModel optionalServiceModel) {
                                Timber.wtf(new Gson().toJson(optionalServiceModel) + orderId);
                                progressDialog.dismiss();
                            }
                        });
            }
        });
    }

    private void setupToolbarConfig() {
        setSupportActionBar(optionalServiceBinding.toolbar.titleToolbar);
        optionalServiceBinding.toolbar.ivNotify.setVisibility(View.GONE);
    }

    private void initComponent() {
        optionalServiceBinding= DataBindingUtil.setContentView(this,getLayoutRes());
        optionalServiceViewModel= ViewModelProviders.of(this).get(OptionalServiceViewModel.class);

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        if (getIntent().getExtras() != null) {
            carId = getIntent().getExtras().getString(NewOrderActivity.CAR_ID);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_optional_service;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();

    }
}
