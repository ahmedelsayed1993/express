package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.navigation.NavigationCloseDrawerListener;
import com.aait.expresss.FCM.MyFirebaseMessagingService;
import com.aait.expresss.Models.AllDataModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.NavigationFragment;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.AllDataViewModel;
import com.aait.expresss.databinding.ActivityMainBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends BaseActivity {

    ActivityMainBinding mainBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //Nav Fragment
    NavigationFragment navigationFragment;

    //Rec Var
    public static  AllDataModel allDataModel;
    boolean isNotification;


    @Override
    protected void init(Bundle savedInstanceState) {
        mainBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        if (getIntent().getExtras()!=null){
            isNotification=getIntent().getExtras().getBoolean(MyFirebaseMessagingService.FROM_NOTIFICATION);

        }

        setupToolbarConfig();

        fetchUtilsData();

        //New Order
        mainBinding.newOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewOrderActivity.class));

            }
        });

        //Current Order
        mainBinding.currentOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CurrentOrderActivity.class));
            }
        });

        //My Cars
        mainBinding.myCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CarsActivity.class));
            }
        });


        mainBinding.transport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TransportServiceActivity.class));
            }
        });


    }

    private void fetchUtilsData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        AllDataViewModel viewModel= ViewModelProviders.of(this).get(AllDataViewModel.class);
        viewModel.getAllDataModelMutableLiveData(globalPreference.getAppLanguage())
                .observe(this, new Observer<AllDataModel>() {
                    @Override
                    public void onChanged(@Nullable AllDataModel allDataModel) {
                        MainActivity.allDataModel=allDataModel;
                        if (isNotification){
                            startActivity(new Intent(getApplicationContext(),NotificationsActivity.class));
                            progressDialog.dismissWithAnimation();
                            finish();
                        }
                        progressDialog.dismissWithAnimation();
                    }
                });

    }

    private void setupToolbarConfig() {
        setSupportActionBar(mainBinding.toolbar.mainToolbar);
        setDrawerConfig();
        mainBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });

        navigationFragment.setNavigationCloseDrawerListener(new NavigationCloseDrawerListener() {
            @Override
            public void onDrawerClose(Boolean b) {
                if (b){
                    mainBinding.drawerLayout.closeDrawer(Gravity.START);
                }
            }
        });

        mainBinding.toolbar.ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    private void setDrawerConfig() {
        navigationFragment=new NavigationFragment();
        getSupportFragmentManager().beginTransaction()
                .add(mainBinding.navMenu.getId(),navigationFragment)
                .commit();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        if (mainBinding.drawerLayout.isDrawerOpen(Gravity.START)){
            mainBinding.drawerLayout.closeDrawer(Gravity.START);
        }else{
            mainBinding.drawerLayout.openDrawer(Gravity.START);
        }
    }
}
