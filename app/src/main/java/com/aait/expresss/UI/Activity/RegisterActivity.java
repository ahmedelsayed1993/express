package com.aait.expresss.UI.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.GPS.GPSTracker;
import com.aait.expresss.GPS.GPSTrakerListner;
import com.aait.expresss.Models.LatLngModel;
import com.aait.expresss.Models.LoginModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityRegisterBinding;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class RegisterActivity extends BaseActivity {

    ActivityRegisterBinding registerBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //GPS
    GPSTracker gpsTracker;
    private FusedLocationProviderClient mFusedLocationClient;
    //Rec Var
    private static final int REQUEST_CODE = 30;
    public static final String USER_DATA = "user_data";
    LatLngModel latLngModel;


    @SuppressLint("MissingPermission")
    @Override
    protected void init(Bundle savedInstanceState) {
        registerBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        globalPreference = GlobalPreference.getInstance(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        //GPS
        gpsTracker = new GPSTracker(this, new GPSTrakerListner() {
            @Override
            public void onTrakerSuccess(Double lat, Double log) {

            }

            @Override
            public void onStartTraker() {

            }
        });

        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    latLngModel = new LatLngModel(location.getLatitude(),
                            location.getLongitude());
                } else {
                    latLngModel = new LatLngModel(24.821410,
                            46.737192);
                }


            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Timber.wtf(e);
            }
        });


        ConfigBtns();


    }

    private void ConfigBtns() {
        //Conditions
        registerBinding.btnConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ConditionActivity.class));
            }
        });

        //Location
        registerBinding.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), SelectLocationActivity.class), REQUEST_CODE);
            }
        });
        //Register
        registerBinding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    startRegister();
                   /* Toast.makeText(RegisterActivity.this, latLngModel.getLat()+""+latLngModel.getLng(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(RegisterActivity.this, getCompleteAddressString(latLngModel.getLat(),latLngModel.getLng()), Toast.LENGTH_SHORT).show();
*/
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();
                }

            }
        });
    }

    private void startRegister() {
        if (Utils.checkError(registerBinding.etUserName) &&
                Utils.checkError(registerBinding.etMail) && Utils.checkError(registerBinding.etPhoneNumber)
                && Utils.checkError(registerBinding.etPass) && Utils.checkError(registerBinding.etConfPass)
                && Utils.checkEmail(registerBinding.etMail) && Utils.checkPhoneSize(registerBinding.etPhoneNumber)
                && Utils.checkPasswordSize(registerBinding.etPass)
                && Utils.checkMatch(registerBinding.etPass, registerBinding.etConfPass)) {
            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
            RetrofitCall.getInstance().getApi().register(registerBinding.etUserName.getText().toString(),
                    registerBinding.etPhoneNumber.getText().toString(), registerBinding.etMail.getText().toString(),
                    registerBinding.etPass.getText().toString(), LoginActivity.userType
                    , getCompleteAddressString(latLngModel.getLat(), latLngModel.getLng())
                    , latLngModel.getLat(), latLngModel.getLng(), globalPreference.getAppLanguage())
                    .enqueue(new Callback<LoginModel>() {
                        @Override
                        public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                                Bundle bundle = new Bundle();
                                bundle.putParcelable(USER_DATA, response.body().getData());
                                startActivity(new Intent(getApplicationContext(), MailActivateCodeActivity.class)
                                        .putExtras(bundle)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();

                            } else {
                                progressDialog.hide();
                                Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == SelectLocationActivity.RESULT_CODE) {
            latLngModel = new LatLngModel(data.getDoubleExtra("lat", gpsTracker.getLatitude()),
                    data.getDoubleExtra("lng", gpsTracker.getLongitude()));
            registerBinding.btnLocation.setText(getString(R.string.address_selected));
        }

    }


    private String getCompleteAddressString(double lat, double lng) {
        String regioName = "";
        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addresses = gcd.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                regioName = addresses.get(0).getLocality();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return regioName;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return true;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
