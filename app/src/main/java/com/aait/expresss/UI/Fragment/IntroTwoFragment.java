package com.aait.expresss.UI.Fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.expresss.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroTwoFragment extends Fragment {


    public IntroTwoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro_two, container, false);
    }

}
