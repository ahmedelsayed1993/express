package com.aait.expresss.UI.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.GPS.GPSTracker;
import com.aait.expresss.GPS.GPSTrakerListner;
import com.aait.expresss.Models.LoginModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityLoginBinding;
import com.google.firebase.iid.FirebaseInstanceId;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.aait.expresss.UI.Activity.RegisterActivity.USER_DATA;


public class LoginActivity extends BaseActivity implements GPSTrakerListner {

    ActivityLoginBinding loginBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;


    //Rec Var
    public static int userType;
    GPSTracker gpsTracker;
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;

    @Override
    protected void init(Bundle savedInstanceState) {
        loginBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);
        gpsTracker = new GPSTracker(this, this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        if (getIntent().getExtras() != null) {
            userType = getIntent().getExtras().getInt(DetectUserActivity.USER_TYPE);
        }


        //getLocation();
        //forget password
        loginBinding.tvForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SendCodeActivity.class));
            }
        });

        //Register
        loginBinding.tvNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(LoginActivity.this)
                        .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    if (enableLocation()) {
                                        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                                    }
                                } else if (report.isAnyPermissionPermanentlyDenied()) {
                                    Utils.goToLocationPermissionSettings(LoginActivity.this);
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        })
                        .onSameThread()
                        .check();
            }
        });

        //login

        loginBinding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    Dexter.withActivity(LoginActivity.this)
                            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    if (report.areAllPermissionsGranted()) {
                                        if (enableLocation()) {
                                            startLogin();
                                        }
                                    } else if (report.isAnyPermissionPermanentlyDenied()) {
                                        Utils.goToLocationPermissionSettings(LoginActivity.this);
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            })
                            .onSameThread()
                            .check();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();
                }


            }
        });

    }

    private void startLogin() {
        if (Utils.checkError(loginBinding.etPhoneLogin) && Utils.checkError(loginBinding.etPassLogin)) {

            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi()
                    .login(loginBinding.etPhoneLogin.getText().toString(),
                            loginBinding.etPassLogin.getText().toString(), userType,
                            FirebaseInstanceId.getInstance().getToken(),
                            globalPreference.getAppLanguage(), 0)
                    .enqueue(new Callback<LoginModel>() {
                        @Override
                        public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismiss();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                                globalPreference.login(response.body());

                                if (response.body().getData().getDelegate() == 0) {
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    finish();

                                } else if (response.body().getData().getDelegate() == 1) {
                                    startActivity(new Intent(getApplicationContext(), MainDelegateActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    finish();
                                }

                            }else if(response.body().getKey().equals("2")){
                                progressDialog.dismissWithAnimation();
                                Toasty.warning(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                                Bundle bundle = new Bundle();
                                bundle.putParcelable(USER_DATA, response.body().getData());
                                startActivity(new Intent(getApplicationContext(), MailActivateCodeActivity.class)
                                        .putExtras(bundle)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            } else {
                                progressDialog.hide();
                                Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<LoginModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });
        }
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onTrakerSuccess(Double lat, Double log) {

    }

    @Override
    public void onStartTraker() {

    }


    private boolean enableLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            assert locationManager != null;
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            Timber.wtf(ex);
        }

        try {
            assert locationManager != null;
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            Timber.wtf(ex);
        }

        gpsTracker.getLocation();
        if (!gpsTracker.canGetLocation() && !gps_enabled && !network_enabled) {
            Utils.goToLocationSettings(this);
            return false;
        }
        return true;
    }
}
