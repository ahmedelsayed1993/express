package com.aait.expresss.UI.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Activity.DelegateRefuseOrderActivity;
import com.aait.expresss.UI.Activity.FollowOrderActivity;
import com.aait.expresss.UI.Activity.MainDelegateActivity;
import com.aait.expresss.UI.Adapter.Recycler.CarDetailAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.DialogAcceptRefuseOrderBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class AcceptRefuseOrderDialog extends DialogFragment {


    DialogAcceptRefuseOrderBinding acceptRefuseOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;


    //Rec Var
    OrdersModel.OrderData currentOrderData;
    CarDetailAdapter carDetailAdapter;
    private boolean flag=false;
    public static final String ORDER_ID="order_id";
    public static final String DELEGATE_STATE="delegate_State";

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        acceptRefuseOrderBinding= DataBindingUtil.inflate(inflater, R.layout.dialog_accept_refuse_order,container,false);

        globalPreference=GlobalPreference.getInstance(getActivity());

        progressDialog=new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        if (this.getArguments()!=null){
            currentOrderData=getArguments().getParcelable(MainDelegateActivity.CURRENT_ORDER);
        }

        acceptRefuseOrderBinding.order.tvOrderNumber.setText(getString(R.string.order_number)+" "+currentOrderData.getOrder_number()+"");
        acceptRefuseOrderBinding.order.tvDelegateClientName.setText(currentOrderData.getCar_owner_name());
        acceptRefuseOrderBinding.order.tvAddress.setText(currentOrderData.getCar_owner_address());
        acceptRefuseOrderBinding.order.tvOrderData.setText(currentOrderData.getOrder_date());
        acceptRefuseOrderBinding.order.tvPrize.setText(currentOrderData.getOrder_total_price()+"");

        acceptRefuseOrderBinding.order.rvCars.setLayoutManager(new LinearLayoutManager(getActivity()));
        carDetailAdapter=new CarDetailAdapter(currentOrderData.getCars_data(),getActivity());
        acceptRefuseOrderBinding.order.rvCars.setAdapter(carDetailAdapter);



        acceptRefuseOrderBinding.order.textView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag){
                    flag=false;
                    Animation slideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
                    if (ViewCompat.getLayoutDirection(acceptRefuseOrderBinding.order.textView9)==ViewCompat.LAYOUT_DIRECTION_LTR){
                        acceptRefuseOrderBinding.order.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (0,0,R.drawable.up,0);
                    }else{
                        acceptRefuseOrderBinding.order.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (R.drawable.up,0,0,0);
                    }
                    acceptRefuseOrderBinding.order.textView9.setCompoundDrawablePadding(5);
                    acceptRefuseOrderBinding.order.rvCars.setVisibility(View.GONE);
                    acceptRefuseOrderBinding.order.rvCars.startAnimation(slideUp);

                }else{
                    flag=true;
                    Animation slideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
                    if (ViewCompat.getLayoutDirection(acceptRefuseOrderBinding.order.textView9)==ViewCompat.LAYOUT_DIRECTION_LTR){
                        acceptRefuseOrderBinding.order.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (0,0,R.drawable.down,0);
                    }else{
                        acceptRefuseOrderBinding.order.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (R.drawable.down,0,0,0);
                    }
                    acceptRefuseOrderBinding.order.textView9.setCompoundDrawablePadding(5);
                    acceptRefuseOrderBinding.order.rvCars.setVisibility(View.VISIBLE);
                    acceptRefuseOrderBinding.order.rvCars.startAnimation(slideUp);
                }
            }
        });


        //Refuse Order
        acceptRefuseOrderBinding.btnRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(ORDER_ID,currentOrderData.getOrder_number());
                startActivity(new Intent(getActivity(), DelegateRefuseOrderActivity.class)
                .putExtras(bundle));
            }
        });


        //accept order
        acceptRefuseOrderBinding.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi()
                        .acceptOrder(globalPreference.getUser().getData().getId(),
                                currentOrderData.getOrder_number(),
                                globalPreference.getAppLanguage())
                        .enqueue(new Callback<MainResponseModel>() {
                            @Override
                            public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                                if (response.body().getKey().equals("1")){
                                    progressDialog.dismissWithAnimation();
                                    Toasty.success(getActivity(),response.body().getMassage(), Toast.LENGTH_SHORT,true).show();

                                    Bundle bundle=new Bundle();
                                    bundle.putBoolean(DELEGATE_STATE,true);
                                    bundle.putInt(ORDER_ID,currentOrderData.getOrder_number());
                                    startActivity(new Intent(getActivity(), FollowOrderActivity.class)
                                    .putExtras(bundle));
                                    getActivity().finish();

                                }else{
                                    progressDialog.hide();
                                    Toasty.error(getActivity(),response.body().getMassage(), Toast.LENGTH_SHORT,true).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<MainResponseModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });
            }
        });

        return acceptRefuseOrderBinding.getRoot();
    }
}
