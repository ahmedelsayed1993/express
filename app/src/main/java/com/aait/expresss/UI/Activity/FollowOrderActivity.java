package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.FollowOrderModel;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.UI.Fragment.CancelOrderDialog;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.ViewModel.ClientFollowViewModel;
import com.aait.expresss.ViewModel.DelegateFollowViewModel;
import com.aait.expresss.databinding.ActivityFollowOrderBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class FollowOrderActivity extends BaseActivity {

    ActivityFollowOrderBinding followOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;


    //Rec var
    boolean isDelegate;
    int orderId;
    public static final String LAT="lat";
    public static final String LNG="lng";
    String lat;
    String lng;

    @Override
    protected void init(Bundle savedInstanceState) {
        followOrderBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        if (getIntent().getExtras() != null) {
            isDelegate = getIntent().getExtras().getBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE);
            orderId = getIntent().getExtras().getInt(AcceptRefuseOrderDialog.ORDER_ID);
        }

        setUpToolbarConfig();

        if (isDelegate) {
            fetchDelegateFollowOrder();
        } else {
            fetchClientFollowOrder();
        }

        followOrderBinding.btnOrderComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())){
                    completeOrder();
                }else{
                    Toasty.warning(getApplicationContext(),getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT,true).show();
                }
            }
        });

    }

    private void completeOrder() {

        if (isDelegate){
            delegateCompleteOrder();
        }else{
            clientCompleteOrder();
        }
    }

    private void delegateCompleteOrder() {
        Bundle bundle=new Bundle();
        bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,isDelegate);
        bundle.putInt(AcceptRefuseOrderDialog.ORDER_ID,orderId);

        startActivity(new Intent(getApplicationContext(),OrderWaitConfirmationActivity.class)
                .putExtras(bundle)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    private void clientCompleteOrder() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        RetrofitCall.getInstance().getApi()
                .finishOrder(orderId,globalPreference.getAppLanguage())
                .enqueue(new Callback<MainResponseModel>() {
                    @Override
                    public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                        if (response.body().getKey().equals("1")){
                            progressDialog.dismissWithAnimation();

                            Bundle bundle=new Bundle();
                            bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,isDelegate);
                            bundle.putInt(AcceptRefuseOrderDialog.ORDER_ID,orderId);

                            startActivity(new Intent(getApplicationContext(),OrderDoneActivity.class)
                            .putExtras(bundle)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();


                        }else{
                            progressDialog.hide();
                            Toasty.error(getApplicationContext(),response.body().getMassage(),
                                    Toast.LENGTH_SHORT,true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MainResponseModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }

    private void fetchClientFollowOrder() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ClientFollowViewModel viewModel=ViewModelProviders.of(this).get(ClientFollowViewModel.class);
        viewModel.getFollowOrderModelMutableLiveData(orderId,globalPreference.getAppLanguage())
                .observe(this, new Observer<FollowOrderModel>() {
                    @Override
                    public void onChanged(@Nullable FollowOrderModel followOrderModel) {
                        int status=followOrderModel.getData().getOrder_status().size();
                        for (int i=0;i<status;i++){
                            switch (i){
                                case 0:
                                    followOrderBinding.ivStateOne.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateOne.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateOneTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateOneData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateOneTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 1:
                                    followOrderBinding.divStateTwo.setVisibility(View.VISIBLE);
                                    followOrderBinding.ivStateTwo.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateTwo.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateTwoTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateTwoData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateTwoTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 2:
                                    followOrderBinding.ivStateThree.setVisibility(View.VISIBLE);
                                    followOrderBinding.divStateThree.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateThree.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateThreeTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateThreeData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateThreeTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 3:
                                    followOrderBinding.ivStateFour.setVisibility(View.VISIBLE);
                                    followOrderBinding.divStateFour.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFour.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFourTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateFourData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateFourTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 4:
                                    followOrderBinding.ivStateFive.setVisibility(View.VISIBLE);
                                    followOrderBinding.divStateFive.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFive.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFiveTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateFiveData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateFiveTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;

                            }
                        }
                        if (status==4){
                            followOrderBinding.btnOrderComplete.setVisibility(View.VISIBLE);
                        }

                        progressDialog.dismissWithAnimation();
                        Toasty.success(getApplicationContext(), followOrderModel.getMassage(), Toast.LENGTH_SHORT, true).show();

                    }
                });
    }

    private void fetchDelegateFollowOrder() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();


        DelegateFollowViewModel viewModel= ViewModelProviders.of(this).get(DelegateFollowViewModel.class);
        viewModel.getFollowOrderModelMutableLiveData(orderId,globalPreference.getAppLanguage())
                .observe(this, new Observer<FollowOrderModel>() {
                    @Override
                    public void onChanged(@Nullable FollowOrderModel followOrderModel) {

                        lat=followOrderModel.getData().getClient_lat();
                        lng=followOrderModel.getData().getClient_lng();
                        int status=followOrderModel.getData().getOrder_status().size();
                        for (int i=0;i<status;i++){
                            switch (i){
                                case 0:
                                    followOrderBinding.ivStateOne.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateOne.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateOneTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateOneData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateOneTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 1:
                                    followOrderBinding.divStateTwo.setVisibility(View.VISIBLE);
                                    followOrderBinding.ivStateTwo.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateTwo.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateTwoTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateTwoData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateTwoTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 2:
                                    followOrderBinding.ivStateThree.setVisibility(View.VISIBLE);
                                    followOrderBinding.divStateThree.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateThree.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateThreeTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateThreeData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateThreeTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 3:
                                    followOrderBinding.ivStateFour.setVisibility(View.VISIBLE);
                                    followOrderBinding.divStateFour.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFour.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFourTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateFourData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateFourTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;
                                case 4:
                                    followOrderBinding.ivStateFive.setVisibility(View.VISIBLE);
                                    followOrderBinding.divStateFive.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFive.setVisibility(View.VISIBLE);
                                    followOrderBinding.stateFiveTitle.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_message());
                                    followOrderBinding.stateFiveData.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_date());
                                    followOrderBinding.stateFiveTime.setText(followOrderModel.getData()
                                            .getOrder_status().get(i).getStatus_time());
                                    break;

                            }
                        }
                        if (status==4){
                            followOrderBinding.btnOrderComplete.setVisibility(View.VISIBLE);
                        }

                        progressDialog.dismissWithAnimation();
                        Toasty.success(getApplicationContext(), followOrderModel.getMassage(), Toast.LENGTH_SHORT, true).show();

                    }
                });

    }

    private void setUpToolbarConfig() {
        setSupportActionBar(followOrderBinding.toolbar.followToolbar);

        followOrderBinding.toolbar.textView46.setText(getString(R.string.follow_order));

        followOrderBinding.toolbar.ivCancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelOrder();
            }
        });
        if (!isDelegate) {
            followOrderBinding.toolbar.ivLocation.setVisibility(View.GONE);
        }
        followOrderBinding.toolbar.ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getClientLocation();
            }
        });
    }

    private void getClientLocation() {
        Bundle bundle=new Bundle();
        bundle.putString(LAT,lat);
        bundle.putString(LNG,lng);
        startActivity(new Intent(this,ClientLocationActivity.class)
        .putExtras(bundle));
    }

    private void cancelOrder() {
        Bundle bundle=new Bundle();
        bundle.putInt(AcceptRefuseOrderDialog.ORDER_ID,orderId);
        bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,isDelegate);
        CancelOrderDialog cancelOrderDialog=new CancelOrderDialog();
        cancelOrderDialog.setArguments(bundle);

        cancelOrderDialog.show(getSupportFragmentManager(),"CANCEL ORDER DIALOG");
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_follow_order;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
