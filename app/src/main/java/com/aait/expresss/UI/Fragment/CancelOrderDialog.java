package com.aait.expresss.UI.Fragment;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Activity.DeleteOrderActivity;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.DialogCancelOrderBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CancelOrderDialog extends DialogFragment {

    DialogCancelOrderBinding cancelOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //Rec Var
    int orderId;
    boolean isDelegate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        cancelOrderBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_cancel_order, container, false);

        globalPreference = GlobalPreference.getInstance(getActivity());

        progressDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        if (getArguments() != null) {
            orderId = getArguments().getInt(AcceptRefuseOrderDialog.ORDER_ID);
            isDelegate = getArguments().getBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE);
        }

        cancelOrderBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    CancelOrder();
                } else {
                    Toasty.warning(getActivity(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();
                }
            }
        });

        cancelOrderBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelOrderDialog.this.dismiss();
            }
        });

        return cancelOrderBinding.getRoot();
    }

    private void CancelOrder() {
        if (isDelegate) {
            delegateCancel();
        } else {
            clientCancel();
        }
    }

    private void delegateCancel() {
        if (Utils.checkError(cancelOrderBinding.etCancelNote) &&
                Utils.checkError(cancelOrderBinding.etCancelReson)) {
            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
            RetrofitCall.getInstance().getApi()
                    .cancelOrder(orderId, cancelOrderBinding.etCancelReson.getText().toString(),
                            cancelOrderBinding.etCancelNote.getText().toString(),
                            globalPreference.getAppLanguage())
                    .enqueue(new Callback<MainResponseModel>() {
                        @Override
                        public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getActivity(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                                Bundle bundle = new Bundle();
                                bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE, isDelegate);

                                startActivity(new Intent(getActivity(), DeleteOrderActivity.class)
                                        .putExtras(bundle)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();


                            } else {
                                progressDialog.hide();
                                Toasty.error(getActivity(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MainResponseModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });

        }
    }

    private void clientCancel() {
        if (Utils.checkError(cancelOrderBinding.etCancelNote) &&
                Utils.checkError(cancelOrderBinding.etCancelReson)) {
            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
            RetrofitCall.getInstance().getApi()
                    .clientCancelOrder(orderId, cancelOrderBinding.etCancelReson.getText().toString(),
                            cancelOrderBinding.etCancelNote.getText().toString(),
                            globalPreference.getAppLanguage())
                    .enqueue(new Callback<MainResponseModel>() {
                        @Override
                        public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getActivity(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();


                                Bundle bundle = new Bundle();
                                bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE, isDelegate);

                                startActivity(new Intent(getActivity(), DeleteOrderActivity.class)
                                        .putExtras(bundle)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                getActivity().finish();


                            } else {
                                progressDialog.hide();
                                Toasty.error(getActivity(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MainResponseModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });

        }
    }
}
