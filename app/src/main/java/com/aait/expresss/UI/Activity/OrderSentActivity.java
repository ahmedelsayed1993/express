package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.databinding.ActivityOrderSentBinding;

public class OrderSentActivity extends BaseActivity {

    ActivityOrderSentBinding orderSentBinding;

    @Override
    protected void init(Bundle savedInstanceState) {
        orderSentBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        orderSentBinding.tvBackToMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_order_sent;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
