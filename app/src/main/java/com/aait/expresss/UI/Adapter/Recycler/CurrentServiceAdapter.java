package com.aait.expresss.UI.Adapter.Recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.RecycleServiceBinding;

import java.util.List;

class CurrentServiceAdapter extends RecyclerView.Adapter<CurrentServiceAdapter.ViewHolder> {
    private List<OrdersModel.OrderData.ServicesData> servicesData;
    private Context context;


    public CurrentServiceAdapter(List<OrdersModel.OrderData.ServicesData> servicesData, Context context) {
        this.servicesData = servicesData;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_service,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrdersModel.OrderData.ServicesData item=servicesData.get(position);
        holder.serviceBinding.tvServiceName.setText(item.getName());
        holder.serviceBinding.tvServicePrice.setText(item.getPrice()+" " + context.getString(R.string.sar));
    }

    @Override
    public int getItemCount() {
        return servicesData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleServiceBinding serviceBinding;
        public ViewHolder(View itemView) {
            super(itemView);
            serviceBinding= DataBindingUtil.bind(itemView);
            assert serviceBinding!=null;
            serviceBinding.notifyChange();
        }
    }
}
