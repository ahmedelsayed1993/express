package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityShareAppBinding;

public class ShareAppActivity extends BaseActivity {

    ActivityShareAppBinding shareAppBinding;

    GlobalPreference globalPreference;

    @Override
    protected void init(Bundle savedInstanceState) {
        shareAppBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);


        setUpToolbarConfig();

        setUpShareButtons();
    }

    private void setUpShareButtons() {
        shareAppBinding.btnFaceShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.share_face(getApplicationContext(),getPackageName());
            }
        });

        shareAppBinding.btnTwitterShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.share_app_twitter(getApplicationContext(),getPackageName());
            }
        });

        shareAppBinding.btnGplusShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.share_app_gplus(getApplicationContext(),getPackageName());
            }
        });
    }

    private void setUpToolbarConfig() {
        setSupportActionBar(shareAppBinding.toolbar.titleToolbar);
        shareAppBinding.toolbar.tvTitle.setText(getString(R.string.share_app));
        if (globalPreference.getUser().getData().getDelegate()==1){
            shareAppBinding.toolbar.ivNotify.setVisibility(View.GONE);
        }else{
            shareAppBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNotificationActivity();
                }
            });
        }
        shareAppBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_share_app;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
            startActivity(new Intent(getApplicationContext(),NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        finish();
        super.onBackPressed();
    }
}
