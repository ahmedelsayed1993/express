package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityDelegateRefuseOrderBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DelegateRefuseOrderActivity extends BaseActivity {

    ActivityDelegateRefuseOrderBinding delegateRefuseOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    int orderId;

    @Override
    protected void init(Bundle savedInstanceState) {
        delegateRefuseOrderBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        if (getIntent().getExtras() != null) {
            orderId = getIntent().getExtras().getInt(AcceptRefuseOrderDialog.ORDER_ID);
        }


        setToolbarConfig();

        //refuse order
        delegateRefuseOrderBinding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    refuseOrder();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT, true).show();
                }
            }
        });


    }

    private void setToolbarConfig() {
        setSupportActionBar(delegateRefuseOrderBinding.toolbar.titleToolbar);

        delegateRefuseOrderBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });

        delegateRefuseOrderBinding.toolbar.ivNotify.setVisibility(View.GONE);
        delegateRefuseOrderBinding.toolbar.tvTitle.setText(getString(R.string.the_reason_of_refuse));

    }

    private void refuseOrder() {
        if (Utils.checkError(delegateRefuseOrderBinding.etRefusedReason)) {

            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi()
                    .refuseOrder(globalPreference.getUser().getData().getId(), orderId,
                            delegateRefuseOrderBinding.etRefusedReason.getText().toString(),
                            globalPreference.getAppLanguage())
                    .enqueue(new Callback<MainResponseModel>() {
                        @Override
                        public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                            if (response.body().getKey().equals("1")) {

                                progressDialog.dismissWithAnimation();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                                startActivity(new Intent(getApplicationContext(),MainDelegateActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            } else {
                                progressDialog.hide();
                                Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<MainResponseModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_delegate_refuse_order;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();
    }
}
