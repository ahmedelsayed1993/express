package com.aait.expresss.UI.Activity;


import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;

import com.aait.expresss.Models.ActivateCodeModel;
import com.aait.expresss.Models.LoginModel;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityMailActivateCodeBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MailActivateCodeActivity extends BaseActivity {

    ActivityMailActivateCodeBinding mailActivateCodeBinding;

    SweetAlertDialog progressDialog;

    GlobalPreference globalPreference;

    //Rec Var
    LoginModel.LoginData registerData;

    @Override
    protected void init(Bundle savedInstanceState) {
        mailActivateCodeBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        globalPreference = GlobalPreference.getInstance(this);

        if (getIntent().getExtras() != null) {
            registerData = getIntent().getExtras().getParcelable(RegisterActivity.USER_DATA);
        }

        mailActivateCodeBinding.btnConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivate();
            }
        });


        //resend password
        mailActivateCodeBinding.btnResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi().resendCode(registerData.getId(),
                        globalPreference.getAppLanguage())
                        .enqueue(new Callback<MainResponseModel>() {
                            @Override
                            public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                                if (response.body().getKey().equals("1")) {
                                    progressDialog.dismiss();
                                    Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                                } else {
                                    progressDialog.hide();
                                    Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<MainResponseModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });
            }
        });
    }

    private void startActivate() {
        if (Utils.checkError(mailActivateCodeBinding.etActivateCode)) {
            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi()
                    .activateAccount(registerData.getId(),
                            mailActivateCodeBinding.etActivateCode.getText().toString(),
                            globalPreference.getAppLanguage())
                    .enqueue(new Callback<ActivateCodeModel>() {
                        @Override
                        public void onResponse(Call<ActivateCodeModel> call, Response<ActivateCodeModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();

                            } else {
                                progressDialog.hide();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<ActivateCodeModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_mail_activate_code;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
