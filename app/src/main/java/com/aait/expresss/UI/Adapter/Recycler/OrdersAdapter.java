package com.aait.expresss.UI.Adapter.Recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.aait.expresss.Callbacks.DelegateOrderListener;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.RecycleOrderrBinding;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private List<OrdersModel.OrderData> currentOrderDataList;
    private Context context;
    private boolean flag=false;
    private DelegateOrderListener delegateOrderListener;


    public OrdersAdapter(List<OrdersModel.OrderData> currentOrderDataList, Context context, DelegateOrderListener delegateOrderListener) {
        this.currentOrderDataList = currentOrderDataList;
        this.context = context;
        this.delegateOrderListener = delegateOrderListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_orderr,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final OrdersModel.OrderData currentOrderData=currentOrderDataList.get(position);
        holder.orderBinding.tvOrderNumber.setText(context.getString(R.string.order_number)+" "+currentOrderData.getOrder_number() );
        holder.orderBinding.tvTime.setText(currentOrderData.getOrder_time());
        holder.orderBinding.tvDelegateClientName.setText(currentOrderData.getCar_owner_name());
        holder.orderBinding.tvAddress.setText(Utils.nullChecker(currentOrderData.getCar_owner_address()));

        //Cars Adapter
        holder.orderBinding.rvCars.setLayoutManager(new LinearLayoutManager(context));
        CarDetailAdapter carDetailAdapter=new CarDetailAdapter(currentOrderData.getCars_data(),context);
        holder.orderBinding.rvCars.setAdapter(carDetailAdapter);

        //Service Adapter
        if (currentOrderData.getServices().size()==0){
            holder.orderBinding.tvAddSerLbl.setVisibility(View.GONE);
            holder.orderBinding.rvServices.setVisibility(View.GONE);
        }else{
            holder.orderBinding.rvServices.setLayoutManager(new LinearLayoutManager(context));
            CurrentServiceAdapter serviceAdapter=new CurrentServiceAdapter(currentOrderData.getServices(),context);
            holder.orderBinding.rvServices.setAdapter(serviceAdapter);
        }



        holder.orderBinding.textView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag){
                    flag=false;
                    Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
                    if (ViewCompat.getLayoutDirection(holder.orderBinding.textView9)==ViewCompat.LAYOUT_DIRECTION_LTR){
                        holder.orderBinding.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (0,0,R.drawable.up,0);
                    }else{
                        holder.orderBinding.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (R.drawable.up,0,0,0);
                    }
                    holder.orderBinding.textView9.setCompoundDrawablePadding(5);
                    holder.orderBinding.rvCars.setVisibility(View.GONE);
                    holder.orderBinding.rvCars.startAnimation(slideUp);

                }else{
                    flag=true;
                    if (ViewCompat.getLayoutDirection(holder.orderBinding.textView9)==ViewCompat.LAYOUT_DIRECTION_LTR){
                        holder.orderBinding.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (0,0,R.drawable.down,0);
                    }else{
                        holder.orderBinding.textView9.setCompoundDrawablesWithIntrinsicBounds
                                (R.drawable.down,0,0,0);
                    }



                    holder.orderBinding.textView9.setCompoundDrawablePadding(5);
                    Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);
                    holder.orderBinding.rvCars.setVisibility(View.VISIBLE);
                    holder.orderBinding.rvCars.startAnimation(slideDown);
                }
            }
        });

        holder.orderBinding.tvOrderData.setText(currentOrderData.getOrder_date());
        holder.orderBinding.tvPrize.setText(currentOrderData.getOrder_total_price()+" "+context.getString(R.string.sar));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delegateOrderListener.onDelegateOrderClick(currentOrderData);
            }
        });

    }

    @Override
    public int getItemCount() {
        return currentOrderDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleOrderrBinding orderBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            orderBinding= DataBindingUtil.bind(itemView);
            assert orderBinding!=null;
            orderBinding.notifyChange();
        }
    }
}
