package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.ViewPager.IntroPagerAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.ActivityIntroBinding;

public class IntroActivity extends BaseActivity {
    private ActivityIntroBinding introBinding;
    private GlobalPreference globalPreference;

    @Override
    protected void init(Bundle savedInstanceState) {
        introBinding= DataBindingUtil.setContentView(this,getLayoutRes());
        globalPreference=GlobalPreference.getInstance(this);


        introBinding.vpager.setAdapter(new IntroPagerAdapter(getSupportFragmentManager()));
        introBinding.indicator.setViewPager(introBinding.vpager);
        introBinding.vpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position==2){
                    introBinding.tvNext.setVisibility(View.GONE);
                    introBinding.tvSkip.setText(R.string.enter);
                }else {
                    introBinding.tvNext.setVisibility(View.VISIBLE);
                    introBinding.tvSkip.setText(R.string.skip);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        introBinding.tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                introBinding.vpager.setCurrentItem(introBinding.vpager.getCurrentItem()+1);
            }
        });

        introBinding.tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                globalPreference.setIntroScreen(false);
                startActivity(new Intent(getApplicationContext(),DetectUserActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });


    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_intro;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
