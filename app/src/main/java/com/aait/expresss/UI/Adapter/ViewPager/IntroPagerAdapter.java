package com.aait.expresss.UI.Adapter.ViewPager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aait.expresss.UI.Fragment.IntroOneFragment;
import com.aait.expresss.UI.Fragment.IntroThreeFragment;
import com.aait.expresss.UI.Fragment.IntroTwoFragment;

public class IntroPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;


    public IntroPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return new IntroOneFragment();
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return new IntroTwoFragment();
            case 2: // Fragment # 1 - This will show SecondFragment
                return new IntroThreeFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
