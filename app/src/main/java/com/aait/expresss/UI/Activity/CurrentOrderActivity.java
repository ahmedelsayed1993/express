package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.DelegateOrderListener;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.OrdersAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.ClientCurrentOrderViewModel;
import com.aait.expresss.databinding.ActivityCurrentOrderBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import static com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog.DELEGATE_STATE;
import static com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog.ORDER_ID;

public class CurrentOrderActivity extends BaseActivity  implements DelegateOrderListener {

    ActivityCurrentOrderBinding currentOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    OrdersAdapter ordersAdapter;

    @Override
    protected void init(Bundle savedInstanceState) {
        currentOrderBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        setupToolbar();

        currentOrderBinding.rvCurrentOrders.setLayoutManager(new LinearLayoutManager(this));
        fetchData();


    }

    private void fetchData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ClientCurrentOrderViewModel viewModel= ViewModelProviders.of(this).get(ClientCurrentOrderViewModel.class);
        viewModel.getOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage())
                .observe(this, new Observer<OrdersModel>() {
                    @Override
                    public void onChanged(@Nullable OrdersModel ordersModel) {
                        if (ordersModel.getData().size()>0){
                            ordersAdapter=new OrdersAdapter(ordersModel.getData(),CurrentOrderActivity.this,
                                    CurrentOrderActivity.this);
                            currentOrderBinding.rvCurrentOrders.setAdapter(ordersAdapter);
                        }else{
                            currentOrderBinding.rvCurrentOrders.setVisibility(View.GONE);
                            currentOrderBinding.tvNoOrders.setVisibility(View.VISIBLE);
                        }

                        progressDialog.dismiss();
                    }
                });
    }

    private void setupToolbar() {
        setSupportActionBar(currentOrderBinding.toolbar.titleToolbar);
        currentOrderBinding.toolbar.tvTitle.setText(getString(R.string.current_orders));
        currentOrderBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        currentOrderBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_current_order;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onDelegateOrderClick(OrdersModel.OrderData currentOrderData) {
        Bundle bundle=new Bundle();
        bundle.putBoolean(DELEGATE_STATE,false);
        bundle.putInt(ORDER_ID,currentOrderData.getOrder_number());

        startActivity(new Intent(this,FollowOrderActivity.class)
        .putExtras(bundle));

    }
}
