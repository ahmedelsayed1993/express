package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.ActivitySplashBinding;

public class SplashActivity extends BaseActivity {

    ActivitySplashBinding splashBinding;

    GlobalPreference globalPreference;

    @Override
    protected void init(Bundle savedInstanceState) {
        splashBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);
        Thread thread=new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);

                    if (globalPreference.getIntroScreen()){
                        startActivity(new Intent(getApplicationContext(),IntroActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }else{
                        if (globalPreference.isUserLogged()){
                            if (globalPreference.getUser().getData().getDelegate()==0){
                                startActivity(new Intent(getApplicationContext(),MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }else if (globalPreference.getUser().getData().getDelegate()==1){
                                startActivity(new Intent(getApplicationContext(),MainDelegateActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }

                        }else {
                            startActivity(new Intent(getApplicationContext(),DetectUserActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }
                    }






                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };

        thread.start();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean fullScreen() {
        return true;
    }
    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
