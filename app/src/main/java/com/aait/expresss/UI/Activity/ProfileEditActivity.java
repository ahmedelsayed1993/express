package com.aait.expresss.UI.Activity;

import android.Manifest;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.GPS.GPSTracker;
import com.aait.expresss.GPS.GPSTrakerListner;
import com.aait.expresss.Models.LatLngModel;
import com.aait.expresss.Models.LoginModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.ChangePasswordDialog;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityProfileEditBinding;
import com.fxn.pix.Pix;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ProfileEditActivity extends BaseActivity {

    ActivityProfileEditBinding profileEditBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //GPS
    GPSTracker gpsTracker;

    //Rec Var
    LatLngModel latLngModel;
    private static final int REQUEST_CODE = 30;
    private static final int IMAGE_PIX = 50;

    private static String profilePhoto;

    @Override
    protected void init(Bundle savedInstanceState) {
        profileEditBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);
        latLngModel = new LatLngModel(24.821410, 46.737192);
        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        gpsTracker = new GPSTracker(this, new GPSTrakerListner() {
            @Override
            public void onTrakerSuccess(Double lat, Double log) {

            }

            @Override
            public void onStartTraker() {

            }
        });


        setupToolbarConfig();

        fetchData();

        controls();

        //Change Password
        profileEditBinding.btnChgPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    changePassword();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();

                }
            }
        });

    }

    private void controls() {
        //Image
        profileEditBinding.imageView26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(ProfileEditActivity.this)
                        .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    Pix.start(ProfileEditActivity.this, IMAGE_PIX);
                                } else if (report.isAnyPermissionPermanentlyDenied()) {
                                    Utils.goToImageSettings(ProfileEditActivity.this);
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        })
                        .withErrorListener(new PermissionRequestErrorListener() {
                            @Override
                            public void onError(DexterError error) {
                                Timber.wtf(error.toString());
                            }
                        })
                        .onSameThread()
                        .check();
            }
        });
        //Location
        profileEditBinding.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), SelectLocationActivity.class), REQUEST_CODE);
            }
        });

        //Save
        profileEditBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    updateProfile();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    private void updateProfile() {
        if (Utils.checkError(profileEditBinding.etName) && Utils.checkError(profileEditBinding.etPhone)
                && Utils.checkPhoneSize(profileEditBinding.etPhone)) {
            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();


            RetrofitCall.getInstance().getApi()
                    .updateUserProfile(globalPreference.getUser().getData().getId(), profileEditBinding.etName.getText().toString(),
                            profileEditBinding.etPhone.getText().toString(), latLngModel.getLat(), latLngModel.getLng(),
                            getCompleteAddressString(latLngModel.getLat(),
                                    latLngModel.getLng()),
                            profilePhoto, globalPreference.getAppLanguage())
                    .enqueue(new Callback<LoginModel>() {
                        @Override
                        public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                                globalPreference.login(response.body());
                            } else {
                                progressDialog.hide();
                                Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });

        }

    }

    private void fetchData() {
        Picasso.get().load(globalPreference.getUser().getData().getAvatar_path()).into(profileEditBinding.imageView26);
        profileEditBinding.etName.setText(globalPreference.getUser().getData().getName());
        profileEditBinding.etPhone.setText(globalPreference.getUser().getData().getPhone());
        latLngModel = new LatLngModel(Double.parseDouble(globalPreference.getUser().getData().getLat()),
                Double.parseDouble(globalPreference.getUser().getData().getLng()));
    }

    private void setupToolbarConfig() {
        setSupportActionBar(profileEditBinding.toolbar.titleToolbar);
        profileEditBinding.toolbar.tvTitle.setText(getString(R.string.edit_profile));
        if (globalPreference.getUser().getData().getDelegate() == 1) {
            profileEditBinding.toolbar.ivNotify.setVisibility(View.GONE);
        }
        profileEditBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
        profileEditBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    private void changePassword() {

        ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog();
        changePasswordDialog.show(getSupportFragmentManager(), "CHANGE PASSWORD");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE ) {
            latLngModel.setLat(data.getDoubleExtra("lat", 24.821410));
            latLngModel.setLng(data.getDoubleExtra("lng", 46.737192));
            Log.e(getClass().getSimpleName(),new Gson().toJson(latLngModel));
            profileEditBinding.btnLocation.setText(getString(R.string.address_selected));
        }
        if (requestCode == IMAGE_PIX && resultCode == RESULT_OK) {
            ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            File f = new File(returnValue.get(0));
            profileEditBinding.imageView26.setImageURI(Uri.fromFile(f));

            //decode photo to base64
            profilePhoto = Utils.encodedBase64(BitmapFactory.decodeFile(f.getPath()));


        }

    }


    private String getCompleteAddressString(double lat, double lng) {
        String regioName = "";
        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addresses = gcd.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                regioName = addresses.get(0).getLocality();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return regioName;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_profile_edit;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }
}
