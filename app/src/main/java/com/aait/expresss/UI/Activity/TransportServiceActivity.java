package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Models.TransferModel;
import com.aait.expresss.Models.TransportModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;

import com.aait.expresss.UI.Adapter.Recycler.TransportServiceAdapter;
import com.aait.expresss.UI.Adapter.Spinner.DownloadPlacesSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.StoragePlacesSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.StorageTimesSpinnerAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.TranserServiceViewModel;
import com.aait.expresss.databinding.ActivityTransportServiceBinding;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class TransportServiceActivity extends BaseActivity {

    ActivityTransportServiceBinding transportServiceBinding;
    TranserServiceViewModel transerServiceViewModel;
    private GlobalPreference globalPreference;
    private SweetAlertDialog progressDialog;

    //Adpaters
    TransportServiceAdapter transportServiceAdapter;

    StoragePlacesSpinnerAdapter storagePlacesSpinnerAdapter;
    StorageTimesSpinnerAdapter storageTimesSpinnerAdapter;
    DownloadPlacesSpinnerAdapter downloadPlacesSpinnerAdapter;

    //Rec Var
    private List<TransportModel> transportModelList;
    public static final int DOWN_LOC=100;
    public static final int UP_LOC=200;
    private double downLat =0.0;
    private double downLng =0.0;
    private String downAdrs="";
    private double upLat=0.0;
    private double upLng=0.0;
    private String upadrs="";
    private String type="wild";

    //spinners
    private TransferModel mTransferModel;
    private int placeId=0;
    private int timeId=0;
    private int downloadId=0;


    @Override
    protected void init(Bundle savedInstanceState) {

        initComponent();
        configToolbar();
        configControllers();
        fetchData();

    }

    private void fetchData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        transerServiceViewModel.getTranserData().observe(this, new Observer<TransferModel>() {
            @Override
            public void onChanged(@Nullable TransferModel transferModel) {
                mTransferModel=transferModel;
                transportServiceAdapter.addItems(transferModel.getData().getServices());
                transportServiceAdapter.notifyDataSetChanged();

                storagePlacesSpinnerAdapter=new StoragePlacesSpinnerAdapter(TransportServiceActivity.this,
                        R.layout.spinner_text,transferModel.getData().getStorage_space());
                storageTimesSpinnerAdapter=new StorageTimesSpinnerAdapter(TransportServiceActivity.this,
                        R.layout.spinner_text,transferModel.getData().getStorage_times());
                downloadPlacesSpinnerAdapter=new DownloadPlacesSpinnerAdapter(TransportServiceActivity.this,
                        R.layout.spinner_text,transferModel.getData().getDownload_places());

                transportServiceBinding.spinSpace.setAdapter(storagePlacesSpinnerAdapter);
                transportServiceBinding.spinTime.setAdapter(storageTimesSpinnerAdapter);
                transportServiceBinding.spinUp.setAdapter(downloadPlacesSpinnerAdapter);

                changeToLand();

                progressDialog.dismissWithAnimation();
            }
        });
    }

    private void configControllers() {
        transportServiceBinding.ivLand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToLand();
            }
        });
        transportServiceBinding.ivAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToAir();

            }
        });
        transportServiceBinding.ivSea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToSea();

            }
        });
        transportServiceBinding.ivStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToStorage();
            }
        });
        transportServiceBinding.btnUpLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // startActivityForResult(new Intent(getApplicationContext(), ActivityDetectLocation.class),DOWN_LOC);


            }
        });

        transportServiceBinding.btnDownLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // startActivityForResult(new Intent(getApplicationContext(), ActivityDetectLocation.class),UP_LOC);

            }
        });

        transportServiceBinding.btnSendOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!type.equals("storage")){
                    if (transportModelList.size()!=0){
                        if (upLat!=0.0&&upLng!=0.0){
                            if (downLat!=0.0&&downLng!=0.0){

                                RetrofitCall.getInstance().getApi()
                                        .transferDataPost(globalPreference.getAppLanguage(),globalPreference.getUser().getData().getId(),upLat,
                                                upLng,upadrs,downLat,downLng,downAdrs,new Gson().toJson(transportModelList),type,transportServiceBinding.etNote.getText().toString())
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .doOnSubscribe(new Consumer<Disposable>() {
                                            @Override
                                            public void accept(Disposable disposable) throws Exception {
                                                progressDialog.setContentText(getString(R.string.loading));
                                                progressDialog.setCancelable(false);
                                                progressDialog.show();
                                            }
                                        })
                                        .doOnTerminate(new Action() {
                                            @Override
                                            public void run() throws Exception {
                                                progressDialog.dismiss();
                                            }
                                        })
                                        .subscribe(new io.reactivex.Observer<MainResponseModel>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {

                                            }

                                            @Override
                                            public void onNext(MainResponseModel mainResponseModel) {
                                                if (mainResponseModel.getKey().equals("1")){
                                                    Toasty.success(getApplicationContext(),mainResponseModel.getMassage(), Toast.LENGTH_SHORT,true).show();
                                                    startActivity(new Intent(getApplicationContext(),MainActivity.class).addFlags(
                                                            Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                    ));
                                                    finish();
                                                }else{
                                                    Toasty.warning(getApplicationContext(),mainResponseModel.getMassage(), Toast.LENGTH_SHORT,true).show();

                                                }
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }

                                            @Override
                                            public void onComplete() {

                                            }
                                        });


                            }else {
                                Toasty.warning(getApplicationContext(),getString(R.string.shld_dtct_dow_plc), Toast.LENGTH_SHORT,true).show();

                            }

                        }else{
                            Toasty.warning(getApplicationContext(),getString(R.string.sld_dtct_up_plce), Toast.LENGTH_SHORT,true).show();

                        }


                    }else{
                        Toasty.warning(getApplicationContext(),getString(R.string.sld_dtct_trans_type), Toast.LENGTH_SHORT,true).show();
                    }


                }else{
                    if (upLat!=0.0&&upLng!=0.0){
                        RetrofitCall.getInstance().getApi().storage(downloadId,upLat,upLng,upadrs,timeId,placeId,globalPreference.getAppLanguage(),
                                globalPreference.getUser().getData().getId(),transportServiceBinding.etNote.getText().toString())
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(new Consumer<Disposable>() {
                                    @Override
                                    public void accept(Disposable disposable) throws Exception {
                                        progressDialog.setContentText(getString(R.string.loading));
                                        progressDialog.setCancelable(false);
                                        progressDialog.show();
                                    }
                                })
                                .doOnTerminate(new Action() {
                                    @Override
                                    public void run() throws Exception {
                                        progressDialog.dismiss();
                                    }
                                })
                                .subscribe(new io.reactivex.Observer<MainResponseModel>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {

                                    }

                                    @Override
                                    public void onNext(MainResponseModel mainResponseModel) {
                                        if (mainResponseModel.getKey().equals("1")){
                                            Toasty.success(getApplicationContext(),mainResponseModel.getMassage(), Toast.LENGTH_SHORT,true).show();
                                            startActivity(new Intent(getApplicationContext(),MainActivity.class).addFlags(
                                                    Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            ));
                                            finish();
                                        }else{
                                            Toasty.warning(getApplicationContext(),mainResponseModel.getMassage(), Toast.LENGTH_SHORT,true).show();

                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }

                                    @Override
                                    public void onComplete() {

                                    }
                                });
                    }else{
                        Toasty.warning(getApplicationContext(),getString(R.string.sld_dtct_up_plce), Toast.LENGTH_SHORT,true).show();

                    }
                }
            }
        });

    }

    private void changeToStorage() {
        transportServiceBinding.ivLand.setImageResource(R.drawable.nakl_bary_b);
        transportServiceBinding.tvLand.setTextColor(getResources().getColor(R.color.black));
        transportServiceBinding.ivAir.setImageResource(R.drawable.nakl_gawi_b);
        transportServiceBinding.tvAir.setTextColor(getResources().getColor(R.color.black));
        transportServiceBinding.ivSea.setImageResource(R.drawable.nakl_ba7ry_b);
        transportServiceBinding.tvSea.setTextColor(getResources().getColor(R.color.black));
        transportServiceBinding.ivStorage.setImageResource(R.drawable.box);
        transportServiceBinding.tvStorage.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        transportServiceBinding.groupNakl.setVisibility(View.GONE);
        transportServiceBinding.groupStorage.setVisibility(View.VISIBLE);
        type="storage";
    }

    private void changeToSea() {
        transportServiceBinding.ivLand.setImageResource(R.drawable.nakl_bary_b);
        transportServiceBinding.tvLand.setTextColor(getResources().getColor(R.color.black));
        transportServiceBinding.ivAir.setImageResource(R.drawable.nakl_gawi_b);
        transportServiceBinding.tvAir.setTextColor(getResources().getColor(R.color.black));
        transportServiceBinding.ivSea.setImageResource(R.drawable.nakl_ba7ry);
        transportServiceBinding.tvSea.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        transportServiceBinding.ivStorage.setImageResource(R.drawable.box_b);
        transportServiceBinding.tvStorage.setTextColor(getResources().getColor(android.R.color.black));

        transportServiceBinding.groupNakl.setVisibility(View.VISIBLE);
        transportServiceBinding.groupStorage.setVisibility(View.GONE);

        type="sea";
    }

    private void changeToAir() {
        transportServiceBinding.ivLand.setImageResource(R.drawable.nakl_bary_b);
        transportServiceBinding.tvLand.setTextColor(getResources().getColor(R.color.black));
        transportServiceBinding.ivAir.setImageResource(R.drawable.nakl_gawi);
        transportServiceBinding.tvAir.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        transportServiceBinding.ivSea.setImageResource(R.drawable.nakl_ba7ry_b);
        transportServiceBinding.tvSea.setTextColor(getResources().getColor(android.R.color.black));
        transportServiceBinding.ivStorage.setImageResource(R.drawable.box_b);
        transportServiceBinding.tvStorage.setTextColor(getResources().getColor(android.R.color.black));

        transportServiceBinding.groupNakl.setVisibility(View.VISIBLE);
        transportServiceBinding.groupStorage.setVisibility(View.GONE);
        type="air";
    }

    private void changeToLand() {
        transportServiceBinding.ivLand.setImageResource(R.drawable.nakl_bary);
        transportServiceBinding.tvLand.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        transportServiceBinding.ivAir.setImageResource(R.drawable.nakl_gawi_b);
        transportServiceBinding.tvAir.setTextColor(getResources().getColor(android.R.color.black));
        transportServiceBinding.ivSea.setImageResource(R.drawable.nakl_ba7ry_b);
        transportServiceBinding.tvSea.setTextColor(getResources().getColor(android.R.color.black));
        transportServiceBinding.ivStorage.setImageResource(R.drawable.box_b);
        transportServiceBinding.tvStorage.setTextColor(getResources().getColor(android.R.color.black));

        transportServiceBinding.groupNakl.setVisibility(View.VISIBLE);
        transportServiceBinding.groupStorage.setVisibility(View.GONE);

        type="wild";
    }

    private void configToolbar() {
        setSupportActionBar(transportServiceBinding.toolbar.titleToolbar);
        transportServiceBinding.toolbar.ivNotify.setVisibility(View.GONE);
        transportServiceBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        transportServiceBinding.toolbar.tvTitle.setText(getString(R.string.transportation_and_shipping));
    }

    private void initComponent() {
        transportServiceBinding= DataBindingUtil.setContentView(this,getLayoutRes());
        globalPreference=GlobalPreference.getInstance(this);
        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));
        transportModelList= new ArrayList<>();
        mTransferModel =new TransferModel();

        transportServiceAdapter=new TransportServiceAdapter(new TransportServiceAdapter.TransportListener() {
            @Override
            public void onItemAdded(TransferModel.TransferData.Services services, int position, int value) {
                TransportModel transportModel= new TransportModel(services.getId(),String.valueOf(value));
                transportModelList.add(transportModel);
            }

            @Override
            public void onItemReduce(TransferModel.TransferData.Services services, int position, int value) {
                for(int i =0 ; i<transportModelList.size();i++){
                    if (transportModelList.get(i).getService_id()==services.getId()){
                        transportModelList.remove(i);
                    }
                }
                TransportModel transportModel= new TransportModel(services.getId(),String.valueOf(value));
                transportModelList.add(transportModel);
            }

            @Override
            public void onItemIncrease(TransferModel.TransferData.Services services, int position, int value) {

                for(int i =0 ; i<transportModelList.size();i++){
                    if (transportModelList.get(i).getService_id()==services.getId()){
                        transportModelList.remove(i);
                    }
                }
                TransportModel transportModel= new TransportModel(services.getId(),String.valueOf(value));
                transportModelList.add(transportModel);
            }

            @Override
            public void onItemDeleted(TransferModel.TransferData.Services services, int position, int value) {
                for(int i =0 ; i<transportModelList.size();i++){
                    if (transportModelList.get(i).getService_id()==services.getId()){
                        transportModelList.remove(i);
                    }
                }

            }
        });
        transportServiceBinding.rvTransportType.setLayoutManager(new LinearLayoutManager(this));
        transportServiceBinding.rvTransportType.setAdapter(transportServiceAdapter);

        transerServiceViewModel= ViewModelProviders.of(this).get(TranserServiceViewModel.class);



        transportServiceBinding.spinUp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                downloadId=mTransferModel.getData().getDownload_places().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        transportServiceBinding.spinTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                timeId=mTransferModel.getData().getStorage_times().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        transportServiceBinding.spinSpace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                placeId=mTransferModel.getData().getStorage_space().get(position).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_transport_service;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==DOWN_LOC&&resultCode==RESULT_OK){
            downLat =data.getExtras().getDouble("lat");
            downLng =data.getExtras().getDouble("lng");
            downAdrs=getCompleteAddressString(downLat,downLng);

        }
        if (requestCode==UP_LOC&&resultCode==RESULT_OK){
            upLat =data.getExtras().getDouble("lat");
            upLng =data.getExtras().getDouble("lng");
            upadrs=getCompleteAddressString(upLat,upLng);

        }


    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                strAdd = returnedAddress.getAddressLine(0)+returnedAddress.getAddressLine(1)+returnedAddress.getAddressLine(2);

//                StringBuilder strReturnedAddress = new StringBuilder("");
//
//
//
//                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
//                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
//                }
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }
    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();

    }
}
