package com.aait.expresss.UI.Activity;

import android.content.Intent;
import android.os.Bundle;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;

public class OrderDoneActivity extends BaseActivity {


    //Rec Var
    int orderId;

    boolean isDelegate;


    @Override
    protected void init(Bundle savedInstanceState) {

        if (getIntent().getExtras()!=null){
            isDelegate = getIntent().getExtras().getBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE);
            orderId = getIntent().getExtras().getInt(AcceptRefuseOrderDialog.ORDER_ID);
        }

        Thread thread=new Thread(){
            @Override
            public void run() {
                try {
                    sleep(1000);
                    Bundle bundle=new Bundle();
                    bundle.putInt(AcceptRefuseOrderDialog.ORDER_ID,orderId);
                    bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,isDelegate);
                    startActivity(new Intent(getApplicationContext(),RateActivity.class)
                     .putExtras(bundle)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        thread.start();

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_order_done;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
