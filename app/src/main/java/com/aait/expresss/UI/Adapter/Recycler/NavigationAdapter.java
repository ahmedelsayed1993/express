package com.aait.expresss.UI.Adapter.Recycler;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.expresss.Callbacks.navigation.NavigationItemListener;
import com.aait.expresss.Models.NavigatiomMenuModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.ItemMenuBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.ViewHolder> {

    private List<NavigatiomMenuModel> menuModels;
    private NavigationItemListener navigationActionListener;

    public NavigationAdapter(List<NavigatiomMenuModel> menuModels, NavigationItemListener navigationActionListener) {
        this.menuModels = menuModels;
        this.navigationActionListener = navigationActionListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final NavigatiomMenuModel model = menuModels.get(position);
        Picasso.get().load(model.getImageId()).into(holder.menuBinding.ivMenuPic);
        holder.menuBinding.tvMenuTitle.setText(model.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleMenuItemClick(position, model.getTitle());
            }
        });
    }

    private void handleMenuItemClick(int position, String title) {
        switch (position) {
            case 0:
                navigationActionListener.onHomeClick();
                break;

            case 1:
                navigationActionListener.onProfileClick();
                break;
            case 2:
                navigationActionListener.onCurrentOrderClick();
                break;
            case 3:
                navigationActionListener.onOldOrderClick();
                break;
            case 4:
                navigationActionListener.onNotificationClick();
                break;

            case 5:
                navigationActionListener.onBillsClick();
                break;
            case 6:
                navigationActionListener.onSettingsClick();
                break;
            case 7:
                navigationActionListener.onShareAppClick();
                break;
            case 8:
                navigationActionListener.onContactUsClick();
                break;
            case 9:
                navigationActionListener.onConditionsClick();
                break;
            case 10:
                navigationActionListener.onLogoutClick();
                break;
            default:

                break;
        }

    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ItemMenuBinding menuBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            menuBinding = DataBindingUtil.bind(itemView);
            assert menuBinding != null;
            menuBinding.notifyChange();
        }
    }
}
