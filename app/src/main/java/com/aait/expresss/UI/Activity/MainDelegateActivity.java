package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.DelegateOrderListener;
import com.aait.expresss.Callbacks.navigation.NavigationCloseDrawerListener;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.OrdersAdapter;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.UI.Fragment.NavigationFragment;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.DelegateOrderViewModel;
import com.aait.expresss.databinding.ActivityMainDelegateBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainDelegateActivity extends BaseActivity implements DelegateOrderListener {

    ActivityMainDelegateBinding mainDelegateBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    OrdersAdapter currentOrdersAdapter;

    NavigationFragment navigationFragment;

    //Rec Var
    public static final String CURRENT_ORDER="current_order";

    @Override
    protected void init(Bundle savedInstanceState) {
        mainDelegateBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        setUpToolbar();

        fetchCurrentOrders();

        sendLocationUpdates();



    }

    private void sendLocationUpdates() {
      //  startService(new Intent(this, ScheduledService.class));
    }

    private void fetchCurrentOrders() {

        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        mainDelegateBinding.rvCurrentOrders.setLayoutManager(new LinearLayoutManager(this));
        DelegateOrderViewModel orderViewModel= ViewModelProviders.of(this).get(DelegateOrderViewModel.class);
        orderViewModel.getDelegateCurrentOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage()).observe(this, new Observer<OrdersModel>() {
            @Override
            public void onChanged(@Nullable OrdersModel ordersModel) {
                if (ordersModel.getData().size()>0){
                    currentOrdersAdapter=new OrdersAdapter(ordersModel.getData(),
                            MainDelegateActivity.this,MainDelegateActivity.this);
                    mainDelegateBinding.rvCurrentOrders.setAdapter(currentOrdersAdapter);
                    mainDelegateBinding.rvCurrentOrders.setVisibility(View.VISIBLE);
                    mainDelegateBinding.tvNoCurrentOrder.setVisibility(View.GONE);
                    progressDialog.dismissWithAnimation();
                }else{
                    mainDelegateBinding.rvCurrentOrders.setVisibility(View.GONE);
                    mainDelegateBinding.tvNoCurrentOrder.setVisibility(View.VISIBLE);
                    progressDialog.dismissWithAnimation();
                }



            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(mainDelegateBinding.toolbar.mainToolbar);

        setDrawerConfig();

        mainDelegateBinding.toolbar.textView4.setText(getString(R.string.current_order));
        mainDelegateBinding.toolbar.ivNotify.setVisibility(View.GONE);

        navigationFragment.setNavigationCloseDrawerListener(new NavigationCloseDrawerListener() {
            @Override
            public void onDrawerClose(Boolean b) {
                if (b){
                    mainDelegateBinding.drawerLayout.closeDrawer(Gravity.START);
                }
            }
        });
        mainDelegateBinding.toolbar.ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    private void setDrawerConfig() {
        navigationFragment=new NavigationFragment();
        getSupportFragmentManager().beginTransaction()
                .add(mainDelegateBinding.navMenu.getId(),navigationFragment)
                .commit();

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main_delegate;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(getApplicationContext(),NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        if (mainDelegateBinding.drawerLayout.isDrawerOpen(Gravity.START)){
            mainDelegateBinding.drawerLayout.closeDrawer(Gravity.START);
        }else{
            mainDelegateBinding.drawerLayout.openDrawer(Gravity.START);
        }

    }

    @Override
    public void onDelegateOrderClick(OrdersModel.OrderData currentOrderData) {
        Bundle bundle=new Bundle();
        bundle.putParcelable(CURRENT_ORDER,currentOrderData);

        AcceptRefuseOrderDialog acceptRefuseOrderDialog=new AcceptRefuseOrderDialog();
        acceptRefuseOrderDialog.setArguments(bundle);

        acceptRefuseOrderDialog.show(getSupportFragmentManager(),"ACCEPT_REFUSE_ORDER");

    }
}
