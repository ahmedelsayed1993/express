package com.aait.expresss.UI.Adapter.Spinner;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aait.expresss.R;

import java.util.List;

public class MonthsTimeSpinnerAdapter extends ArrayAdapter<String> {

    private List<String> months;
    private Context context;
    public MonthsTimeSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.context=context;
        this.months=objects;
    }

    @Override
    public int getCount() {
        return months.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return months.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        label.setTextSize(18);
        label.setText(months.get(position));
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(context.getResources().getColor(android.R.color.black));
        label.setText(months.get(position));

        return label;
    }
}
