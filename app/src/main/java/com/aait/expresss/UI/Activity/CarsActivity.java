package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.MyCarListener;
import com.aait.expresss.Models.CarsModel;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.MyCarsAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.MyCarsViewModel;
import com.aait.expresss.databinding.ActivityCarsBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CarsActivity extends BaseActivity implements MyCarListener{

    ActivityCarsBinding carsBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //Rec Var
    MyCarsAdapter myCarsAdapter;
    public static final String CAR_DATA="car_data";


    @Override
    protected void init(Bundle savedInstanceState) {
        carsBinding= DataBindingUtil.setContentView(this,getLayoutRes()) ;

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        setupToolbarConfig();

        carsBinding.rvCars.setLayoutManager(new LinearLayoutManager(this));
        fetchCars();

        //add car
        carsBinding.btnCarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),CarAddActivity.class));
                finish();
            }
        });


    }

    private void fetchCars() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        MyCarsViewModel viewModel= ViewModelProviders.of(this).get(MyCarsViewModel.class);
        viewModel.getCarsModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage())
                .observe(this, new Observer<CarsModel>() {
                    @Override
                    public void onChanged(@Nullable CarsModel carsModel) {
                        progressDialog.dismiss();
                        if (carsModel.getData().size()>0){
                            myCarsAdapter=new MyCarsAdapter(carsModel.getData(),CarsActivity.this,
                                    CarsActivity.this);
                            carsBinding.rvCars.setAdapter(myCarsAdapter);
                        }else{
                            carsBinding.rvCars.setVisibility(View.GONE);
                            carsBinding.tvNoCars.setVisibility(View.VISIBLE);
                        }


                    }
                });
    }

    private void setupToolbarConfig() {
        setSupportActionBar(carsBinding.toolbar.titleToolbar);
        carsBinding.toolbar.tvTitle.setText(getString(R.string.my_car));
        carsBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        carsBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_cars;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        startActivity(new Intent(this,MainActivity.class)
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    @Override
    public void myCarClick(CarsModel.CarsData carsData) {
            Bundle bundle=new Bundle();
            bundle.putParcelable(CAR_DATA,carsData);
            startActivity(new Intent(this,CarEditActivity.class)
            .putExtras(bundle));
            finish();
    }

    @Override
    public void myCarDelete(final CarsModel.CarsData carsData) {

        RetrofitCall.getInstance().getApi()
                .deleteCar(carsData.getId(),globalPreference.getAppLanguage())
                .enqueue(new Callback<MainResponseModel>() {
                    @Override
                    public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                        if (response.body().getKey().equals("1")){
                            myCarsAdapter.deleteCar(carsData);

                            Toasty.success(getApplicationContext(),response.body().getMassage(),
                                    Toast.LENGTH_SHORT,true).show();
                        }else{

                            Toasty.error(getApplicationContext(),response.body().getMassage(),
                                    Toast.LENGTH_SHORT,true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MainResponseModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }
}
