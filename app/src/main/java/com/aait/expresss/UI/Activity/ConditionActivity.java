package com.aait.expresss.UI.Activity;

import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.ConditionsModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.ActivityConditionBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ConditionActivity extends BaseActivity {

    ActivityConditionBinding conditionBinding;

    SweetAlertDialog progressDialog;

    GlobalPreference globalPreference;

    @Override
    protected void init(Bundle savedInstanceState) {
        conditionBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        globalPreference=GlobalPreference.getInstance(this);

        setUpToolbar();




        getConditions();





    }


    private void setUpToolbar() {
        setSupportActionBar(conditionBinding.toolbar.titleToolbar);
        conditionBinding.toolbar.tvTitle.setText(getString(R.string.conditions));
        conditionBinding.toolbar.ivNotify.setVisibility(View.GONE);
        conditionBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    private void getConditions() {
        progressDialog.setContentText("loading ....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        RetrofitCall.getInstance().getApi().getConditions(globalPreference.getAppLanguage())
                .enqueue(new Callback<ConditionsModel>() {
                    @Override
                    public void onResponse(Call<ConditionsModel> call, Response<ConditionsModel> response) {
                        if (response.body().getKey().equals("1")){
                            progressDialog.dismissWithAnimation();
                            conditionBinding.tvConditions.setText(response.body().getData().getText());
                        }else{
                            progressDialog.hide();
                        }
                    }

                    @Override
                    public void onFailure(Call<ConditionsModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });

    }


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_condition;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
    }
}
