package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.CheckPhoneModel;
import com.aait.expresss.Models.ActivateCodeModel;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityPhoneActivateCodeBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PhoneActivateCodeActivity extends BaseActivity {

    ActivityPhoneActivateCodeBinding phoneActivateCodeBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //Rec Var
    CheckPhoneModel.CheckPhoneData checkPhoneData;
    public static final String ACTIVATE_DATA = "activate_data";


    @Override
    protected void init(Bundle savedInstanceState) {
        phoneActivateCodeBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        if (getIntent().getExtras() != null) {
            checkPhoneData = getIntent().getExtras().getParcelable(SendCodeActivity.CHECK_PHONE_DATA);
        }

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        phoneActivateCodeBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    checkCodeActivation();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection), Toast.LENGTH_SHORT, true).show();

                }


            }
        });

        phoneActivateCodeBinding.btnResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi().resendCode(checkPhoneData.getId(),
                        globalPreference.getAppLanguage())
                        .enqueue(new Callback<MainResponseModel>() {
                            @Override
                            public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                                if (response.body().getKey().equals("1")) {
                                    progressDialog.dismiss();
                                    Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();
                                } else {
                                    progressDialog.hide();
                                    Toasty.error(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<MainResponseModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });
            }
        });

    }

    private void checkCodeActivation() {
        if (Utils.checkError(phoneActivateCodeBinding.etActivateCode)) {

            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi()
                    .activateAccount(checkPhoneData.getId(), phoneActivateCodeBinding.etActivateCode.getText().toString(), globalPreference.getAppLanguage())
                    .enqueue(new Callback<ActivateCodeModel>() {
                        @Override
                        public void onResponse(Call<ActivateCodeModel> call, Response<ActivateCodeModel> response) {
                            if (response.body().getKey().equals("1")) {
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                                Bundle bundle = new Bundle();
                                bundle.putParcelable(ACTIVATE_DATA, response.body().getData());

                                startActivity(new Intent(getApplicationContext(), ResetPasswordActivity.class)
                                        .putExtras(bundle)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();

                            } else {
                                progressDialog.hide();
                                Toasty.success(getApplicationContext(), response.body().getMassage(), Toast.LENGTH_SHORT, true).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<ActivateCodeModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });
        }


    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_phone_activate_code;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
