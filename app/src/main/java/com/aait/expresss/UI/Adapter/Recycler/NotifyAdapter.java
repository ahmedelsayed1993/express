package com.aait.expresss.UI.Adapter.Recycler;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Models.SendNotificationModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Activity.FollowOrderActivity;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.RecycleNotifyBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class NotifyAdapter extends RecyclerView.Adapter<NotifyAdapter.ViewHolder> {
    private List<SendNotificationModel.NotificationData> notificationDataList;
    private Context context;


    GlobalPreference globalPreference;
    SweetAlertDialog progressDialog;
    public NotifyAdapter(List<SendNotificationModel.NotificationData> notificationDataList, Context context) {
        this.notificationDataList = notificationDataList;
        this.context = context;

        globalPreference=GlobalPreference.getInstance(context);
        progressDialog=new SweetAlertDialog(context,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(context.getString(R.string.colorDialog)));

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_notify,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final SendNotificationModel.NotificationData notificationData=notificationDataList.get(position);
        holder.notifyBinding.tvNotifyMsg.setText(notificationData.getMessage());
        if (notificationData.getMessage_status()==6){
            Picasso.get().load(R.drawable.refuse).into(holder.notifyBinding.ivNotifyImg);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putInt(AcceptRefuseOrderDialog.ORDER_ID,notificationData.getOrder_id());
                bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,false);
                context.startActivity(new Intent(context, FollowOrderActivity.class)
                .putExtras(bundle));
            }
        });

    }

    public void deleteNotify(final int position){
        progressDialog.setContentText(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        RetrofitCall.getInstance().getApi()
                .deleteNotifications(notificationDataList.get(position).getMessage_id(),
                        globalPreference.getAppLanguage())
        .enqueue(new Callback<MainResponseModel>() {
            @Override
            public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                if (response.body().getKey().equals("1")){
                    progressDialog.dismissWithAnimation();
                    notificationDataList.remove(position);
                    notifyDataSetChanged();
                    Toasty.success(context,response.body().getMassage(), Toast.LENGTH_SHORT,true).show();
                }else{
                    progressDialog.dismiss();
                    Toasty.error(context,response.body().getMassage(), Toast.LENGTH_SHORT,true).show();

                }
            }

            @Override
            public void onFailure(Call<MainResponseModel> call, Throwable t) {
                Timber.wtf(t);
            }
        });

        notificationDataList.remove(position);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return notificationDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleNotifyBinding notifyBinding;
        public ViewHolder(View itemView) {
            super(itemView);
            notifyBinding= DataBindingUtil.bind(itemView);
            assert notifyBinding!=null;
            notifyBinding.notifyChange();
        }
    }
}
