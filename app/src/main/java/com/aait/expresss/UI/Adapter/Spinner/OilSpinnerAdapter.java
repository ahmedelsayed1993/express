package com.aait.expresss.UI.Adapter.Spinner;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aait.expresss.Models.AllDataModel;
import com.aait.expresss.R;

import java.util.List;

public class OilSpinnerAdapter extends ArrayAdapter<AllDataModel.AllData.OilData> {
    private Context context;
    private List<AllDataModel.AllData.OilData> oilDataList;

    public OilSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<AllDataModel.AllData.OilData> objects) {
        super(context, resource, objects);
        this.context=context;
        this.oilDataList=objects;
    }

    @Override
    public int getCount() {
        return oilDataList.size();
    }

    @Nullable
    @Override
    public AllDataModel.AllData.OilData getItem(int position) {
        return oilDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        label.setTextSize(18);
        label.setText(oilDataList.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(context.getResources().getColor(android.R.color.black));
        label.setText(oilDataList.get(position).getName());

        return label;
    }
}
