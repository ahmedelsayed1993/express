package com.aait.expresss.UI.Fragment;

import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aait.expresss.Models.ChangePasswordModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.DialogChangePasswordBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ChangePasswordDialog extends DialogFragment {

    DialogChangePasswordBinding changePasswordBinding;
    GlobalPreference globalPreference;
    SweetAlertDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        changePasswordBinding= DataBindingUtil.inflate(inflater, R.layout.dialog_change_password,container,false);

        globalPreference=GlobalPreference.getInstance(getActivity());

        progressDialog=new SweetAlertDialog(getActivity(),SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        setCancelable(true);

        changePasswordBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getActivity())){
                    changePassword();
                }else{
                    Toasty.warning(getActivity(),getString(R.string.check_internet_connection), Toast.LENGTH_SHORT,true).show();
                }

            }
        });

        return changePasswordBinding.getRoot();
    }

    private void changePassword() {
        if (Utils.checkError(changePasswordBinding.etOldPass)&&Utils.checkError(changePasswordBinding.etNewPass)&&
                Utils.checkError(changePasswordBinding.etConfPass)&&Utils.checkPasswordSize(changePasswordBinding.etNewPass)&&
                Utils.checkMatch(changePasswordBinding.etNewPass,changePasswordBinding.etConfPass)){

            progressDialog.setContentText(getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();

            RetrofitCall.getInstance().getApi()
                    .changePassword(globalPreference.getUser().getData().getId(),
                            changePasswordBinding.etOldPass.getText().toString(),
                            changePasswordBinding.etNewPass.getText().toString(),
                            globalPreference.getAppLanguage())
                    .enqueue(new Callback<ChangePasswordModel>() {
                        @Override
                        public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                            if (response.body().getKey().equals("1")){
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getActivity(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();

                                ChangePasswordDialog.this.dismiss();

                            }else{
                                progressDialog.hide();
                                Toasty.error(getActivity(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ChangePasswordModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });

        }
    }
}
