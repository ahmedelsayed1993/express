package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.BillsAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.BillViewModel;
import com.aait.expresss.databinding.ActivityBillsBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BillsActivity extends BaseActivity {

    ActivityBillsBinding billsBinding;
    GlobalPreference globalPreference;
    SweetAlertDialog progressDialog;

    //Adapter
    BillsAdapter billsAdapter;

    @Override
    protected void init(Bundle savedInstanceState) {
        billsBinding=DataBindingUtil.setContentView(this,getLayoutRes());
        globalPreference=GlobalPreference.getInstance(this);
        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);


        setUpToolbarConfig();

        billsBinding.rvBills.setLayoutManager(new LinearLayoutManager(this));
        fetchData();

    }

    private void fetchData() {
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        BillViewModel viewModel=ViewModelProviders.of(this).get(BillViewModel.class);
//        if (globalPreference.getUser().getData().getDelegate()==1){
//
//            viewModel.getDelegateBillsLiveData(globalPreference.getUser().getData().getId(),
//                    globalPreference.getAppLanguage())
//                    .observe(this, new Observer<OrdersModel>() {
//                        @Override
//                        public void onChanged(@Nullable OrdersModel ordersModel) {
//                            progressDialog.dismiss();
//                            if (ordersModel.getData().size()>0){
//                                billsAdapter=new BillsAdapter(ordersModel.getData(),BillsActivity.this);
//                                billsBinding.rvBills.setAdapter(billsAdapter);
//                            }else{
//                                billsBinding.rvBills.setVisibility(View.GONE);
//                                billsBinding.tvNoBills.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    });
//        }else if(globalPreference.getUser().getData().getDelegate()==0){

            viewModel.getBillsLiveData(globalPreference.getUser().getData().getId(),
                    globalPreference.getAppLanguage())
                    .observe(this, new Observer<OrdersModel>() {
                        @Override
                        public void onChanged(@Nullable OrdersModel ordersModel) {
                            progressDialog.dismiss();
                            if (ordersModel.getData().size()>0){
                                billsAdapter=new BillsAdapter(ordersModel.getData(),BillsActivity.this);
                                billsBinding.rvBills.setAdapter(billsAdapter);
                            }else{
                                billsBinding.rvBills.setVisibility(View.GONE);
                                billsBinding.tvNoBills.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }

    //}

    private void setUpToolbarConfig() {
        setSupportActionBar(billsBinding.toolbar.titleToolbar);
        billsBinding.toolbar.tvTitle.setText(getString(R.string.bills));
        billsBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        billsBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_bills;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
    }
}
