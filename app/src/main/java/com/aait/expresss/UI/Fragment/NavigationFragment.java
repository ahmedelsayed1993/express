package com.aait.expresss.UI.Fragment;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aait.expresss.Callbacks.navigation.NavigationCloseDrawerListener;
import com.aait.expresss.Callbacks.navigation.NavigationItemListener;
import com.aait.expresss.Models.LogoutModel;
import com.aait.expresss.Models.NavigatiomMenuModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Activity.ConditionActivity;
import com.aait.expresss.UI.Activity.ContactUsActivity;
import com.aait.expresss.UI.Activity.CurrentOrderActivity;
import com.aait.expresss.UI.Activity.BillsActivity;
import com.aait.expresss.UI.Activity.DetectUserActivity;
import com.aait.expresss.UI.Activity.NotificationsActivity;
import com.aait.expresss.UI.Activity.OldOrderActivity;
import com.aait.expresss.UI.Activity.ProfileActivity;
import com.aait.expresss.UI.Activity.SettingsActivity;
import com.aait.expresss.UI.Activity.ShareAppActivity;
import com.aait.expresss.UI.Adapter.Recycler.NavigationAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.FragmentNavigationBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class NavigationFragment extends Fragment {

    FragmentNavigationBinding navigationBinding;

    GlobalPreference globalPreference;


    NavigationAdapter navigationAdapter;
    //Rec Var
    NavigationItemListener itemListener;
    ArrayList<NavigatiomMenuModel> modelArrayList;


    NavigationCloseDrawerListener navigationCloseDrawerListener;
    public NavigationCloseDrawerListener getNavigationCloseDrawerListener() {
        return navigationCloseDrawerListener;
    }
    public void setNavigationCloseDrawerListener(NavigationCloseDrawerListener navigationCloseDrawerListener) {
        this.navigationCloseDrawerListener = navigationCloseDrawerListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        navigationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_navigation, container, false);


        globalPreference = GlobalPreference.getInstance(getActivity());

        navigationBinding.rvMenu.setLayoutManager(new LinearLayoutManager(getActivity()));

        itemListener = new NavigationItemListener() {
            @Override
            public void onHomeClick() {
                navigationCloseDrawerListener.onDrawerClose(true);
            }

            @Override
            public void onProfileClick() {
                startActivity(new Intent(getActivity(), ProfileActivity.class));
            }

            @Override
            public void onCurrentOrderClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    //Old Order
                    startActivity(new Intent(getActivity(), OldOrderActivity.class));
                }else{
                    startActivity(new Intent(getActivity(), CurrentOrderActivity.class));
                }
            }

            @Override
            public void onOldOrderClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    //Settings
                    startActivity(new Intent(getActivity(), BillsActivity.class));
                }else{
                    startActivity(new Intent(getActivity(), OldOrderActivity.class));
                }
            }

            @Override
            public void onNotificationClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    //Share app
                    startActivity(new Intent(getActivity(), SettingsActivity.class));
                }else{
                    startActivity(new Intent(getActivity(), NotificationsActivity.class));
                }
            }

            @Override
            public void onBillsClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    //Share app
                    startActivity(new Intent(getActivity(), ShareAppActivity.class));
                }else{
                    startActivity(new Intent(getActivity(), BillsActivity.class));
                }
            }


            @Override
            public void onSettingsClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    //Contact us
                    startActivity(new Intent(getActivity(), ContactUsActivity.class));


                }else{
                    startActivity(new Intent(getActivity(), SettingsActivity.class));
                }

            }

            @Override
            public void onShareAppClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    //Conditions
                    startActivity(new Intent(getActivity(), ConditionActivity.class));


                }else{
                    startActivity(new Intent(getActivity(), ShareAppActivity.class));
                }
            }

            @Override
            public void onContactUsClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    logout();
                }else{
                    startActivity(new Intent(getActivity(), ContactUsActivity.class));
                }


            }

            @Override
            public void onConditionsClick() {
                if (globalPreference.getUser().getData().getDelegate()==1){
                    logout();
                }else{
                    startActivity(new Intent(getActivity(), ConditionActivity.class));
                }


            }

            @Override
            public void onLogoutClick() {
                logout();
            }
        };


        setMenuData();
        return navigationBinding.getRoot();
    }

    private void logout() {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.are_sure))
                .setContentText(getString(R.string.you_want_exit))
                .setConfirmText(getString(R.string.yes))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sDialog) {

                        RetrofitCall.getInstance().getApi()
                                .logout(globalPreference.getUser().getData().getId(),
                                        globalPreference.getAppLanguage())
                                .enqueue(new Callback<LogoutModel>() {
                                    @Override
                                    public void onResponse(Call<LogoutModel> call, Response<LogoutModel> response) {
                                        if (response.body().getKey().equals("1")){
                                            globalPreference.logout();
                                            startActivity(new Intent(getContext(), DetectUserActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));

//                                            if (isMyServiceRunning(ScheduledService.class)){
//                                                getActivity().stopService(new Intent(getActivity(),ScheduledService.class));
//                                            }

                                            sDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                            sDialog.dismissWithAnimation();

                                            getActivity().finish();
                                        }else{
                                            sDialog.changeAlertType(SweetAlertDialog.WARNING_TYPE);
                                            sDialog.hide();
                                            Toasty.error(getActivity(),response.body().getMassage(), Toast.LENGTH_SHORT,true).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<LogoutModel> call, Throwable t) {
                                        Timber.wtf(t);
                                    }
                                });
                    }
                })
                .show();
    }

    private void setMenuData() {

        modelArrayList = new ArrayList<>();
        if (globalPreference.getUser().getData().getDelegate() == 0) {
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.home), R.drawable.home));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.profile), R.drawable.user));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.current_order), R.drawable.order));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.previous_order), R.drawable.clock));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.notifications), R.drawable.notificationn));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.bills), R.drawable.receipt));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.settings), R.drawable.settings));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.share_app), R.drawable.share));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.contact_us), R.drawable.phone));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.conditions), R.drawable.invoice));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.logout), R.drawable.exit));
        } else if (globalPreference.getUser().getData().getDelegate() == 1) {
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.home), R.drawable.home));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.profile), R.drawable.user));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.previous_order), R.drawable.clock));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.bills), R.drawable.receipt));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.settings), R.drawable.settings));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.share_app), R.drawable.share));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.contact_us), R.drawable.phone));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.conditions), R.drawable.invoice));
            modelArrayList.add(new NavigatiomMenuModel(getString(R.string.logout), R.drawable.exit));

        }


        navigationAdapter=new NavigationAdapter(modelArrayList,itemListener);
        navigationBinding.rvMenu.setAdapter(navigationAdapter);
        navigationBinding.tvUserName.setText(globalPreference.getUser().getData().getName());
        Picasso.get().load(globalPreference.getUser().getData().getAvatar_path())
                .placeholder(R.drawable.profile)
                .into(navigationBinding.ivUserImg);




    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager)getActivity(). getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
