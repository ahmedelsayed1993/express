package com.aait.expresss.UI.Adapter.Recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.expresss.Callbacks.MyCarListener;
import com.aait.expresss.Models.CarsModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.RecycleCarBinding;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MyCarsAdapter extends RecyclerView.Adapter<MyCarsAdapter.ViewHolder> {

    private List<CarsModel.CarsData> carsDataList;
    private MyCarListener myCarListener;
    private Context context;
    private SweetAlertDialog progressDialog;

    public MyCarsAdapter(List<CarsModel.CarsData> carsDataList, MyCarListener myCarListener, Context context) {
        this.carsDataList = carsDataList;
        this.myCarListener = myCarListener;
        this.context = context;

        progressDialog=new SweetAlertDialog(context,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(context.getString(R.string.colorDialog)));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_car,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CarsModel.CarsData carsData=carsDataList.get(position);
        holder.carBinding.tvCarModel.setText(carsData.getCar_brand_name()+"");
        holder.carBinding.tvCarPlate.setText(carsData.getCar_plate_number()+"");
        holder.carBinding.tvEngineType.setText(carsData.getCar_engine_name()+"");

        if (ViewCompat.getLayoutDirection(holder.carBinding.tvTimeLeft)==ViewCompat.LAYOUT_DIRECTION_LTR){
            holder.carBinding.tvTimeLeft.setText(carsData.getCar_oil_expired_date()+" : " +carsData.getCar_oil_expired_time());

        }else{
            holder.carBinding.tvTimeLeft.setText(carsData.getCar_oil_expired_time()+" : "+carsData.getCar_oil_expired_date());

        }


        //Edit Car Info
        holder.carBinding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCarListener.myCarClick(carsData);
            }
        });

        //Delete Car Info
        holder.carBinding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCarListener.myCarDelete(carsData);
            }
        });

    }

    public void deleteCar(CarsModel.CarsData carsData) {
        carsDataList.remove(carsData);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return carsDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleCarBinding carBinding;
        public ViewHolder(View itemView) {
            super(itemView);
            carBinding= DataBindingUtil.bind(itemView);
            assert carBinding!=null;
            carBinding.notifyChange();
        }
    }
}
