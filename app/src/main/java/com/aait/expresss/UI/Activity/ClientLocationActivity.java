package com.aait.expresss.UI.Activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.ActivityClientLocationBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ClientLocationActivity extends BaseActivity implements OnMapReadyCallback {
    ActivityClientLocationBinding clientLocationBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;


    private GoogleMap gmap;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    //Rec var
    String lat;
    String lng;

    @Override
    protected void init(Bundle savedInstanceState) {
        clientLocationBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        if (getIntent().getExtras()!=null){
            lat=getIntent().getExtras().getString(FollowOrderActivity.LAT);
            lng=getIntent().getExtras().getString(FollowOrderActivity.LNG);
        }

        setupToolbarConfig();

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        clientLocationBinding.mapView3.onCreate(mapViewBundle);
        clientLocationBinding.mapView3.getMapAsync(this);

    }

    private void setupToolbarConfig() {
        setSupportActionBar(clientLocationBinding.toolbar.titleToolbar);
        clientLocationBinding.toolbar.ivNotify.setVisibility(View.GONE);
        clientLocationBinding.toolbar.tvTitle.setText(R.string.client_location);
        clientLocationBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }
        clientLocationBinding.mapView3.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        clientLocationBinding.mapView3.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        clientLocationBinding.mapView3.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        clientLocationBinding.mapView3.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        clientLocationBinding.mapView3.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        clientLocationBinding.mapView3.onLowMemory();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_client_location;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setMinZoomPreference(10);
        gmap.setIndoorEnabled(true);

        UiSettings uiSettings = gmap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);//Indoor level picker is applicable for building with floor plans
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true); // Map tool bar contains actions which take you to directions screen, google map app etc.
        uiSettings.setCompassEnabled(true); //add compass to map
        uiSettings.setZoomControlsEnabled(true); // add zoom control

        //---disable tilt, pinch to stretch, zoom, rotate and scroll map
        uiSettings.setRotateGesturesEnabled(false);
        uiSettings.setScrollGesturesEnabled(false);
        uiSettings.setTiltGesturesEnabled(false);
        uiSettings.setZoomGesturesEnabled(false);

        //lat lng and marker settings
        LatLng ny=new LatLng(Double.parseDouble(lat),Double.parseDouble(lat));


        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(ny);
        googleMap.addMarker(markerOptions);

        googleMap.animateCamera(CameraUpdateFactory.newLatLng(ny));
    }
}
