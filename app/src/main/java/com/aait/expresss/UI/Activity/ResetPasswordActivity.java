package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.ActivateCodeModel;
import com.aait.expresss.Models.SetNewPasswordModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityResetPasswordBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ResetPasswordActivity extends BaseActivity {

    ActivityResetPasswordBinding resetPasswordBinding;

    SweetAlertDialog progressDialog;
    GlobalPreference globalPreference;

    //Rec Var
    ActivateCodeModel.ActivateData activateData;

    @Override
    protected void init(Bundle savedInstanceState) {
        resetPasswordBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        if (getIntent().getExtras()!=null){
            activateData=getIntent().getExtras().getParcelable(PhoneActivateCodeActivity.ACTIVATE_DATA);
        }

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        resetPasswordBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* startActivity(new Intent(getApplicationContext(),LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();*/

               if (Utils.isNetworkAvailable(getApplicationContext())){
                   resetPassword();
               }else{
                   Toasty.warning(getApplicationContext(),getString(R.string.check_internet_connection), Toast.LENGTH_SHORT,true).show();

               }

            }
        });
    }

    private void resetPassword() {
        if (Utils.checkError(resetPasswordBinding.etNewPass)&&Utils.checkError(resetPasswordBinding.etConfPass)
                &&Utils.checkPasswordSize(resetPasswordBinding.etNewPass)
                &&Utils.checkMatch(resetPasswordBinding.etNewPass,resetPasswordBinding.etConfPass)){

                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();

            RetrofitCall.getInstance().getApi()
                    .resetPassword(activateData.getId(),resetPasswordBinding.etNewPass.getText().toString(),
                            globalPreference.getAppLanguage())
                    .enqueue(new Callback<SetNewPasswordModel>() {
                        @Override
                        public void onResponse(Call<SetNewPasswordModel> call, Response<SetNewPasswordModel> response) {
                            if (response.body().getKey().equals("1")){
                                progressDialog.dismissWithAnimation();
                                Toasty.success(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();

                                startActivity(new Intent(getApplicationContext(),LoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();

                            }else{
                                progressDialog.hide();
                                Toasty.error(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<SetNewPasswordModel> call, Throwable t) {
                            Timber.wtf(t);
                        }
                    });


        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_reset_password;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
