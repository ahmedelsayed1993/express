package com.aait.expresss.UI.Activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.DelegateOrderListener;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Models.NewOrderShowModel;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.OrderNowAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.ViewModel.OrderNowViewModel;
import com.aait.expresss.databinding.ActivityOrderNowBinding;

import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class OrderNowActivity extends BaseActivity implements DelegateOrderListener {
    ActivityOrderNowBinding orderNowBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    OrderNowAdapter ordersAdapter;

    //Rec Var
    int orderId;
    String carId;
    String dateLater;
    String timeLater;

    @Override
    protected void init(Bundle savedInstanceState) {
        orderNowBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        if (getIntent().getExtras() != null) {
            carId = getIntent().getExtras().getString(NewOrderActivity.CAR_ID);
        }

        setupToolbarConfig();

        orderNowBinding.rvOrders.setLayoutManager(new LinearLayoutManager(this));
        fetchData();

        setupControllers();


    }

    private void setupControllers() {
        //order now
        orderNowBinding.btnOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    orderNow();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT, true).show();
                }
            }
        });

        //order later
        orderNowBinding.btnOrderLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    orderLater();
                } else {
                    Toasty.warning(getApplicationContext(), getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT, true).show();
                }
            }
        });
    }

    private void orderLater() {
        Calendar mcurrentDate = Calendar.getInstance();
        int year = mcurrentDate.get(Calendar.YEAR);
        int month = mcurrentDate.get(Calendar.MONTH);
        final int data = mcurrentDate.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog mDatePicker;
        mDatePicker =
                new DatePickerDialog(OrderNowActivity.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        int m=month+1;
                        dateLater=year + "-" + m + "-" + dayOfMonth;
                        selectTime();


                    }
                }, year, month, data);
        mDatePicker.setTitle(getString(R.string.choose_time));
        //to ignore old date
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        mDatePicker.show();
    }

    private void selectTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        final TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(OrderNowActivity.this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                timeLater=selectedHour + ":" + selectedMinute;

                startOrderLater();


            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle(getString(R.string.select_hour));

        mTimePicker.show();
    }

    private void startOrderLater() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        RetrofitCall.getInstance().getApi()
                .confirmOrder(orderId,dateLater,timeLater, globalPreference.getAppLanguage())
                .enqueue(new Callback<MainResponseModel>() {
                    @Override
                    public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                        if (response.body().getKey().equals("1")) {
                            progressDialog.dismissWithAnimation();
                            startActivity(new Intent(getApplicationContext(), OrderSentActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        } else {
                            progressDialog.hide();
                            Toasty.error(getApplicationContext(), response.body().getMassage(),
                                    Toast.LENGTH_SHORT, true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MainResponseModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });

    }

    private void orderNow() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        RetrofitCall.getInstance().getApi()
                .confirmOrder(orderId, globalPreference.getAppLanguage())
                .enqueue(new Callback<MainResponseModel>() {
                    @Override
                    public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                        if (response.body().getKey().equals("1")) {
                            progressDialog.dismissWithAnimation();
                            startActivity(new Intent(getApplicationContext(), OrderSentActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        } else {
                            progressDialog.hide();
                            Toasty.error(getApplicationContext(), response.body().getMassage(),
                                    Toast.LENGTH_SHORT, true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MainResponseModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });

    }

    private void fetchData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        OrderNowViewModel viewModel = ViewModelProviders.of(this)
                .get(OrderNowViewModel.class);
        viewModel.getOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),
                carId, globalPreference.getAppLanguage())
                .observe(this, new Observer<NewOrderShowModel>() {
                    @Override
                    public void onChanged(@Nullable NewOrderShowModel ordersModel) {
                        progressDialog.dismiss();
                        ordersAdapter=new OrderNowAdapter(ordersModel.getData(),OrderNowActivity.this);
                        orderNowBinding.rvOrders.setAdapter(ordersAdapter);
                        orderId = ordersModel.getData().getOrder_number();

                    }
                });

    }

    private void setupToolbarConfig() {
        setSupportActionBar(orderNowBinding.toolbar.titleToolbar);
        orderNowBinding.toolbar.tvTitle.setText(R.string.order_now);
        orderNowBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        orderNowBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_order_now;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onDelegateOrderClick(OrdersModel.OrderData currentOrderData) {

    }
}
