package com.aait.expresss.UI.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.GPS.GPSTracker;
import com.aait.expresss.GPS.GPSTrakerListner;
import com.aait.expresss.Models.LatLngModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.ActivitySelectLocationBinding;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;


public class SelectLocationActivity extends BaseActivity implements OnMapReadyCallback,GPSTrakerListner{

    ActivitySelectLocationBinding selectLocationBinding;

    private GoogleMap gmap;

    //GPS
    GPSTracker gpsTracker;
    private FusedLocationProviderClient mFusedLocationClient;
    //Rec Var
    LatLngModel latLngModel;
    public static final int RESULT_CODE=20;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    @Override
    protected void init(Bundle savedInstanceState) {
        selectLocationBinding= DataBindingUtil.setContentView(this,getLayoutRes());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        setUpToolbar();
        latLngModel = new LatLngModel(24.821410,
                46.737192);
        configMapGps(savedInstanceState);




        selectLocationBinding.btnSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=getIntent();
                if (latLngModel.getLat()==0.0||latLngModel.getLng()==0){
                    intent.putExtra("lat",24.821410);
                    intent.putExtra("lng",46.737192);
                }else{
                    intent.putExtra("lat",latLngModel.getLat());
                    intent.putExtra("lng",latLngModel.getLng());
                }

                setResult(RESULT_CODE,intent);
                Log.e(getClass().getSimpleName(),new Gson().toJson(latLngModel));
                finish();
            }
        });
    }
    private void setUpToolbar() {
        setSupportActionBar(selectLocationBinding.toolbar.titleToolbar);
        selectLocationBinding.toolbar.ivNotify.setVisibility(View.GONE);
        selectLocationBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
        selectLocationBinding.toolbar.tvTitle.setText(R.string.select_location);
    }
    private void configMapGps(Bundle savedInstanceState) {
        gpsTracker=new GPSTracker(this,this);
        latLngModel=new LatLngModel();
        //map
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        selectLocationBinding.mapView.onCreate(mapViewBundle);
        selectLocationBinding.mapView.getMapAsync(this);

    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        selectLocationBinding.mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        selectLocationBinding.mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        selectLocationBinding.mapView.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        selectLocationBinding.mapView.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

        selectLocationBinding.mapView.onResume();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        selectLocationBinding.mapView.onLowMemory();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_select_location;
    }
    @Override
    protected boolean fullScreen() {
        return true;
    }
    @Override
    protected boolean hideInputType() {
        return false;
    }
    @Override
    protected void startNotificationActivity() {

    }
    @Override
    protected void startSearchActivity() {

    }
    @Override
    protected void navigate() {
        super.onBackPressed();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        //gmap.setMinZoomPreference(16);
        gmap.setIndoorEnabled(true);

        UiSettings uiSettings = gmap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);//Indoor level picker is applicable for building with floor plans
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true); // Map tool bar contains actions which take you to directions screen, google map app etc.
        uiSettings.setCompassEnabled(false); //add compass to map
        uiSettings.setZoomControlsEnabled(true); // add zoom control

//        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
//            @Override
//            public void onSuccess(Location location) {
//                if (location != null) {
//                    latLngModel = new LatLngModel(location.getLatitude(),
//                            location.getLongitude());
//
//
//                }
//
//
//            }
//        }).addOnFailureListener(this, new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Timber.wtf(e);
//            }
//        });


        LatLng ny = new LatLng(24.821410,46.737192);
        //gmap.moveCamera(CameraUpdateFactory.newLatLng(ny));
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(ny);
        googleMap.addMarker(markerOptions);

        gmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                gmap.clear();

                latLngModel.setLat(latLng.latitude);
                latLngModel.setLng(latLng.longitude);
                markerOptions.position(latLng);
                gmap.addMarker(markerOptions);


            }
        });


        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ny, 8f));
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onTrakerSuccess(Double lat, Double log) {

    }

    @Override
    public void onStartTraker() {

    }
}
