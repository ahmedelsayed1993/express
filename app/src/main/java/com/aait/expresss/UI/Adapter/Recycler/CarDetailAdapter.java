package com.aait.expresss.UI.Adapter.Recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.RecycleCarDetailBinding;

import java.util.List;

public class CarDetailAdapter extends RecyclerView.Adapter<CarDetailAdapter.ViewHolder> {
    private List<OrdersModel.OrderData.CarsData> carsDataList;
    private Context context;

    public CarDetailAdapter(List<OrdersModel.OrderData.CarsData> carsDataList, Context context) {
        this.carsDataList = carsDataList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_car_detail,parent,false);

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrdersModel.OrderData.CarsData carsData=carsDataList.get(position);
        holder.carDetailBinding.tvOrderNumber.setText(context.getString(R.string.car_number)+" "+(position+1));
        holder.carDetailBinding.tvCarType.setText(carsData.getCar_brand_name());
        holder.carDetailBinding.tvOilType.setText(carsData.getCar_oil_name());
        holder.carDetailBinding.tvCarPlate.setText(carsData.getCar_plate_number());
        holder.carDetailBinding.tvPrize.setText(carsData.getCar_oil_price()+" "+context.getString(R.string.sar));

    }


    @Override
    public int getItemCount() {
        return carsDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleCarDetailBinding carDetailBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            carDetailBinding= DataBindingUtil.bind(itemView);
            assert carDetailBinding!=null;
            carDetailBinding.notifyChange();
        }
    }
}
