package com.aait.expresss.UI.Adapter.Recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aait.expresss.Models.NewOrderShowModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.RecycleCarDetailBinding;

import java.util.List;

public class CarDetailNowAdapter extends RecyclerView.Adapter<CarDetailNowAdapter.ViewHolder> {
    private List<NewOrderShowModel.DataBean.CarsData> carsDataBeanList;
    private Context context;

    public CarDetailNowAdapter(List<NewOrderShowModel.DataBean.CarsData> carsDataBeanList, Context context) {
        this.carsDataBeanList = carsDataBeanList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_car_detail,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NewOrderShowModel.DataBean.CarsData carsData=carsDataBeanList.get(position);
        holder.carDetailBinding.tvOrderNumber.setText(context.getString(R.string.car_number)+" "+(position+1));
        holder.carDetailBinding.tvCarType.setText(carsData.getCar_brand_name());
        holder.carDetailBinding.tvOilType.setText(carsData.getCar_oil_name());
        holder.carDetailBinding.tvCarPlate.setText(carsData.getCar_plate_number());
        holder.carDetailBinding.tvPrize.setText(carsData.getCar_oil_price()+" "+context.getString(R.string.sar));
    }

    @Override
    public int getItemCount() {
        return carsDataBeanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleCarDetailBinding carDetailBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            carDetailBinding= DataBindingUtil.bind(itemView);
            assert carDetailBinding!=null;
            carDetailBinding.notifyChange();
        }
    }
}
