package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.ChangePasswordDialog;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityProfileBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import es.dmoral.toasty.Toasty;
import timber.log.Timber;

public class ProfileActivity extends BaseActivity implements OnMapReadyCallback {

    ActivityProfileBinding profileBinding;

    GlobalPreference globalPreference;

    private GoogleMap gmap;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";


    @Override
    protected void init(Bundle savedInstanceState) {
        profileBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);


        setUpToolbar();

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        profileBinding.mapView2.onCreate(mapViewBundle);
        profileBinding.mapView2.getMapAsync(this);

        setProfileData();


        //Edit Profile
        profileBinding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ProfileEditActivity.class));
                finish();
            }
        });
        //Change Password
        profileBinding.btnChgPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())){
                    changePassword();
                }else{
                    Toasty.warning(getApplicationContext(),getString(R.string.check_internet_connection), Toast.LENGTH_SHORT,true).show();

                }
            }
        });

    }

    private void changePassword() {
        ChangePasswordDialog changePasswordDialog=new ChangePasswordDialog();
        changePasswordDialog.show(getSupportFragmentManager(),"CHANGE PASSWORD");
    }

    private void setUpToolbar() {
        setSupportActionBar(profileBinding.toolbar.titleToolbar);
        profileBinding.toolbar.tvTitle.setText(getString(R.string.profile));
        if (globalPreference.getUser().getData().getDelegate()==1){
            profileBinding.toolbar.ivNotify.setVisibility(View.GONE);
        }else{
            profileBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNotificationActivity();
                }
            });
        }

        profileBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    private void setProfileData() {
        profileBinding.ivUserName.setText(globalPreference.getUser().getData().getName());
        profileBinding.ivUserPhone.setText(globalPreference.getUser().getData().getPhone());
        Picasso.get().load(globalPreference.getUser().getData().getAvatar_path())
                .placeholder(R.drawable.profile)
                .into(profileBinding.ivUserImage);
        Timber.e(globalPreference.getUser().getData().getLat()+" "+globalPreference.getUser().getData().getLng());

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }
        profileBinding.mapView2.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        profileBinding.mapView2.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
        profileBinding.mapView2.onStop();
    }
    @Override
    protected void onPause() {
        super.onPause();
        profileBinding.mapView2.onPause();
    }
    @Override
    protected void onResume() {
        super.onResume();
        profileBinding.mapView2.onResume();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        profileBinding.mapView2.onLowMemory();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_profile;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        if (globalPreference.getUser().getData().getDelegate()==1){
            startActivity(new Intent(this,MainDelegateActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }else if (globalPreference.getUser().getData().getDelegate()==0){
            startActivity(new Intent(this,MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (globalPreference.getUser().getData().getDelegate()==1){
            startActivity(new Intent(this,MainDelegateActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }else if (globalPreference.getUser().getData().getDelegate()==0){
            startActivity(new Intent(this,MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setMinZoomPreference(6);
        gmap.setIndoorEnabled(true);

        UiSettings uiSettings = gmap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);//Indoor level picker is applicable for building with floor plans
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true); // Map tool bar contains actions which take you to directions screen, google map app etc.
        uiSettings.setCompassEnabled(true); //add compass to map
        uiSettings.setZoomControlsEnabled(true); // add zoom control

        //---disable tilt, pinch to stretch, zoom, rotate and scroll map
        uiSettings.setRotateGesturesEnabled(false);
        uiSettings.setScrollGesturesEnabled(false);
        uiSettings.setTiltGesturesEnabled(false);
        uiSettings.setZoomGesturesEnabled(false);

        double lat=Double.parseDouble(globalPreference.getUser().getData().getLat());
        double lng=Double.parseDouble(globalPreference.getUser().getData().getLng());
        LatLng latLng=new LatLng(lat,lng);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        googleMap.addMarker(markerOptions);

        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));


    }
}
