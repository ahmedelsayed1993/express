package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.SendNotificationModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.NotifyAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.SwipeController;
import com.aait.expresss.Utils.SwipeControllerActions;
import com.aait.expresss.ViewModel.NotificationsViewModel;
import com.aait.expresss.databinding.ActivityNotificationsBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class NotificationsActivity extends BaseActivity  {

    ActivityNotificationsBinding notificationsBinding;
    SweetAlertDialog progressDialog;
    GlobalPreference globalPreference;

    //Recycler
    SwipeController swipeController = null;
    NotifyAdapter notifyAdapter;

    @Override
    protected void init(Bundle savedInstanceState) {
        notificationsBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        setupToolbarConfig();

        setupRecyclerViewConfig();
        fetchData();

    }

    private void setupRecyclerViewConfig() {
        notificationsBinding.rvNotifications.setLayoutManager(new LinearLayoutManager(this));
        swipeController=new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                notifyAdapter.deleteNotify(position);
            }
        },NotificationsActivity.this);

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(notificationsBinding.rvNotifications);

        notificationsBinding.rvNotifications.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
    }

    private void fetchData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        NotificationsViewModel viewModel= ViewModelProviders.of(this).get(NotificationsViewModel.class);
        viewModel.getSendNotificationModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage())
                .observe(this, new Observer<SendNotificationModel>() {
                    @Override
                    public void onChanged(@Nullable SendNotificationModel sendNotificationModel) {
                        if (sendNotificationModel.getData().size()>0){
                            notifyAdapter=new NotifyAdapter(sendNotificationModel.getData(),
                                    NotificationsActivity.this);
                            notificationsBinding.rvNotifications.setAdapter(notifyAdapter);
                        }else{
                            notificationsBinding.rvNotifications.setVisibility(View.GONE);
                            notificationsBinding.tvNoNotify.setVisibility(View.VISIBLE);
                        }

                        progressDialog.dismissWithAnimation();

                    }
                });
    }

    private void setupToolbarConfig() {
        setSupportActionBar(notificationsBinding.toolbar.titleToolbar);
        notificationsBinding.toolbar.tvTitle.setText(getString(R.string.notifications));
        notificationsBinding.toolbar.ivNotify.setVisibility(View.GONE);
        notificationsBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_notifications;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,MainActivity.class));
        finish();

    }
}
