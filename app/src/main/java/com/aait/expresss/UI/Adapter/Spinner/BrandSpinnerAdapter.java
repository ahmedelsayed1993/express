package com.aait.expresss.UI.Adapter.Spinner;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aait.expresss.Models.AllDataModel;
import com.aait.expresss.R;

import java.util.List;

public class BrandSpinnerAdapter extends ArrayAdapter<AllDataModel.AllData.BrandData> {

    private Context context;
    private  List<AllDataModel.AllData.BrandData> brandDataList;

    public BrandSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<AllDataModel.AllData.BrandData> objects) {
        super(context, resource, objects);
        this.context=context;
        this.brandDataList=objects;
    }


    @Override
    public int getCount() {
        return brandDataList.size();
    }

    @Nullable
    @Override
    public AllDataModel.AllData.BrandData getItem(int position) {
        return brandDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        label.setTextSize(18);
        label.setText(brandDataList.get(position).getName());
        return label;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(context.getResources().getColor(android.R.color.black));
        label.setText(brandDataList.get(position).getName());

        return label;
    }
}
