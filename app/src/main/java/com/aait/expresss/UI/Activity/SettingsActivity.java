package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.UserSettingsModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.databinding.ActivitySettingsBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class SettingsActivity extends BaseActivity {

    ActivitySettingsBinding settingsBinding;

    SweetAlertDialog progressDialog;
    GlobalPreference globalPreference;

    @Override
    protected void init(Bundle savedInstanceState) {
        settingsBinding= DataBindingUtil.setContentView(this,getLayoutRes());



        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));


        globalPreference=GlobalPreference.getInstance(this);

        setUpToolbarConfig();

        getSettings();
    }

    private void getSettings() {
        //get previous settings
        if (globalPreference.isNotificationEnabled()){
            settingsBinding.swbtnNotify.setChecked(true);
        }else{
            settingsBinding.swbtnNotify.setChecked(false);
        }

        if (globalPreference.getAppLanguage().equals("ar")){
            settingsBinding.rdbtnAr.setChecked(true);
        }else{
            settingsBinding.rdbtnEn.setChecked(true);
        }

        if (globalPreference.getUser().getData().getDelegate()==1){
            settingsBinding.notifyLayout.setVisibility(View.GONE);
            settingsBinding.textView36.setVisibility(View.GONE);
        }


        //change settings
            //english
        settingsBinding.rdbtnEn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    globalPreference.setAppLanguage("en");
                    changeSettings("en",globalPreference.getNotificationAccess(globalPreference.isNotificationEnabled()));

                }
            }
        });
            //arabic
        settingsBinding.rdbtnAr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    globalPreference.setAppLanguage("ar");
                    changeSettings("ar",globalPreference.getNotificationAccess(globalPreference.isNotificationEnabled()));

                }
            }
        });

            //notification
        settingsBinding.swbtnNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    globalPreference.setNotificationAccess(true);
                    changeSettings(globalPreference.getAppLanguage()
                            ,globalPreference.getNotificationAccess(true));

                }else{
                    globalPreference.setNotificationAccess(false);
                    changeSettings(globalPreference.getAppLanguage()
                            ,globalPreference.getNotificationAccess(false));
                }
            }
        });

    }


    private void changeSettings(String lang,int notify){
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        RetrofitCall.getInstance().getApi()
                .settings(globalPreference.getUser().getData().getId(),lang,notify,lang)
                .enqueue(new Callback<UserSettingsModel>() {
                    @Override
                    public void onResponse(Call<UserSettingsModel> call, Response<UserSettingsModel> response) {
                        if (response.body().getKey().equals("1")){
                            progressDialog.dismiss();
                            Toasty.success(getApplicationContext(),response.body().getMassage(), Toast.LENGTH_SHORT,true).show();

                            if (globalPreference.getUser().getData().getDelegate()==1){
                                startActivity(new Intent(getApplicationContext(),MainDelegateActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }else if (globalPreference.getUser().getData().getDelegate()==0){
                                startActivity(new Intent(getApplicationContext(),MainActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                finish();
                            }
                        }else{
                            progressDialog.hide();
                            Toasty.error(getApplicationContext(),response.body().getMassage(), Toast.LENGTH_SHORT,true).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserSettingsModel> call, Throwable t) {
                        Timber.wtf(t);
                    }
                });
    }


    private void setUpToolbarConfig() {
        setSupportActionBar(settingsBinding.toolbar.titleToolbar);
        settingsBinding.toolbar.tvTitle.setText(getString(R.string.settings));

        if (globalPreference.getUser().getData().getDelegate()==1){
            settingsBinding.toolbar.ivNotify.setVisibility(View.GONE);
        }else{
            settingsBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startNotificationActivity();
                }
            });
        }
        settingsBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_settings;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

      super.onBackPressed();

    }


}
