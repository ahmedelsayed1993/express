package com.aait.expresss.UI.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Callbacks.DelegateOrderListener;
import com.aait.expresss.Models.OrdersModel;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Recycler.OrdersAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.ViewModel.ClientOldOrderViewModel;
import com.aait.expresss.ViewModel.DelegateOldOrderViewModel;
import com.aait.expresss.databinding.ActivityOldOrderBinding;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class OldOrderActivity extends BaseActivity implements DelegateOrderListener {

    ActivityOldOrderBinding oldOrderBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    OrdersAdapter ordersAdapter;

    @Override
    protected void init(Bundle savedInstanceState) {
        oldOrderBinding = DataBindingUtil.setContentView(this, getLayoutRes());

        globalPreference = GlobalPreference.getInstance(this);

        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        setUpToolbarConfig();

        oldOrderBinding.rvOldOrders.setLayoutManager(new LinearLayoutManager(this));
        fetchData();


    }

    private void fetchData() {
        if (globalPreference.getUser().getData().getDelegate() == 1) {
            getDelegateData();
        } else {
            getClientData();
        }
    }

    private void getClientData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        ClientOldOrderViewModel viewModel=ViewModelProviders.of(this).get(ClientOldOrderViewModel.class);
        viewModel.getOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage())
                .observe(this, new Observer<OrdersModel>() {
                    @Override
                    public void onChanged(@Nullable OrdersModel ordersModel) {
                        if (ordersModel.getData().size() > 0) {
                            ordersAdapter = new OrdersAdapter(ordersModel.getData(),
                                    OldOrderActivity.this, OldOrderActivity.this);
                            oldOrderBinding.rvOldOrders.setAdapter(ordersAdapter);
                            progressDialog.dismissWithAnimation();
                        } else {
                            oldOrderBinding.tvNoOldOrders.setVisibility(View.VISIBLE);
                            oldOrderBinding.rvOldOrders.setVisibility(View.GONE);
                            progressDialog.dismissWithAnimation();
                        }
                    }
                });
    }

    private void getDelegateData() {
        progressDialog.setContentText(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        DelegateOldOrderViewModel viewModel = ViewModelProviders.of(this).get(DelegateOldOrderViewModel.class);
        viewModel.getDelegateCurrentOrdersModelMutableLiveData(globalPreference.getUser().getData().getId(),
                globalPreference.getAppLanguage())
                .observe(this, new Observer<OrdersModel>() {
                    @Override
                    public void onChanged(@Nullable OrdersModel ordersModel) {
                        if (ordersModel.getData().size() > 0) {
                            ordersAdapter = new OrdersAdapter(ordersModel.getData(),
                                    OldOrderActivity.this, OldOrderActivity.this);
                            oldOrderBinding.rvOldOrders.setAdapter(ordersAdapter);
                            progressDialog.dismissWithAnimation();
                        } else {
                            oldOrderBinding.tvNoOldOrders.setVisibility(View.VISIBLE);
                            oldOrderBinding.rvOldOrders.setVisibility(View.GONE);
                            progressDialog.dismissWithAnimation();
                        }

                    }
                });
    }

    private void setUpToolbarConfig() {
        setSupportActionBar(oldOrderBinding.toolbar.titleToolbar);
        oldOrderBinding.toolbar.tvTitle.setText(R.string.old_orders);
        oldOrderBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });

        if (globalPreference.getUser().getData().getDelegate() == 1) {
            oldOrderBinding.toolbar.ivNotify.setVisibility(View.GONE);
        }
        oldOrderBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_old_order;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onDelegateOrderClick(OrdersModel.OrderData currentOrderData) {
       /* Bundle bundle = new Bundle();
        bundle.putBoolean(DELEGATE_STATE, true);
        bundle.putInt(ORDER_ID, currentOrderData.getOrder_number());
        startActivity(new Intent(this, FollowOrderActivity.class)
                .putExtras(bundle));
        finish();*/
    }
}
