package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.CarAddUpdateModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Adapter.Spinner.BrandSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.EngineSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.MonthsTimeSpinnerAdapter;
import com.aait.expresss.UI.Adapter.Spinner.OilSpinnerAdapter;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityCarAddBinding;

import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CarAddActivity extends BaseActivity {

    ActivityCarAddBinding carAddBinding;

    GlobalPreference globalPreference;

    SweetAlertDialog progressDialog;

    //Spinners Adapters
    BrandSpinnerAdapter brandSpinnerAdapter;
    OilSpinnerAdapter oilSpinnerAdapter;
    EngineSpinnerAdapter engineSpinnerAdapter;
    MonthsTimeSpinnerAdapter monthsAdapter;

    //Rec Var
    int brandName;
    int oilType;
    int engineType;
    String monthsTime;

    @Override
    protected void init(Bundle savedInstanceState) {
        carAddBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        globalPreference=GlobalPreference.getInstance(this);

        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        setupToolbarConfig();
        setUpSpinners();

        //Add New Car
        carAddBinding.btnCarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())){
                    startAddingCar();
                }else{
                    Toasty.warning(getApplicationContext(),getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT,true).show();
                }
            }
        });

    }

    private void startAddingCar() {
        if (Utils.checkError(carAddBinding.etCarModel) &&Utils.checkError(carAddBinding.etPackageNumber)&&
                Utils.checkError(carAddBinding.etPlateNumber)
                &&Utils.checkError(carAddBinding.etViscostyDegree)){

            if (monthsTime.equals(getString(R.string.time_to_change_oil))){
                Toasty.warning(getApplicationContext(),getString(R.string.detect_time),Toast.LENGTH_SHORT,true).show();

            }else{
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi()
                        .addCar(globalPreference.getUser().getData().getId(),
                                brandName,carAddBinding.etCarModel.getText().toString(),
                                carAddBinding.etPlateNumber.getText().toString(),oilType,engineType,
                                carAddBinding.etPackageNumber.getText().toString(),carAddBinding.etViscostyDegree.getText().toString(),

                                monthsTime,globalPreference.getAppLanguage())
                        .enqueue(new Callback<CarAddUpdateModel>() {
                            @Override
                            public void onResponse(Call<CarAddUpdateModel> call, Response<CarAddUpdateModel> response) {
                                if (response.body().getKey().equals("1")){
                                    progressDialog.dismissWithAnimation();
                                    Toasty.success(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                                }else{
                                    progressDialog.hide();
                                    Toasty.error(getApplicationContext(),response.body().getMassage(),Toast.LENGTH_SHORT,true).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CarAddUpdateModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });
            }



        }
    }

    private void setUpSpinners() {



        brandSpinnerAdapter=new BrandSpinnerAdapter(this,R.layout.spinner_text,MainActivity.allDataModel.getData().getBrand_data());
        oilSpinnerAdapter=new OilSpinnerAdapter(this,R.layout.spinner_text,MainActivity.allDataModel.getData().getOil_data());
        engineSpinnerAdapter=new EngineSpinnerAdapter(this,R.layout.spinner_text,MainActivity.allDataModel.getData().getEngine_data());

        monthsAdapter=new MonthsTimeSpinnerAdapter(this, R.layout.spinner_text
                , Arrays.asList(getResources().getStringArray(R.array.months_array)));


        carAddBinding.spinnerCarType.setAdapter(brandSpinnerAdapter);
        carAddBinding.spinnerOilType.setAdapter(oilSpinnerAdapter);
        carAddBinding.spinnerEngineType.setAdapter(engineSpinnerAdapter);
        carAddBinding.etChangeOilTime.setAdapter(monthsAdapter);

        //get Brand Name
        carAddBinding.spinnerCarType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brandName=MainActivity.allDataModel.getData().getBrand_data().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //get Oil Type
        carAddBinding.spinnerOilType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                oilType=MainActivity.allDataModel.getData().getOil_data().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //get Engine type

        carAddBinding.spinnerEngineType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                engineType=MainActivity.allDataModel.getData().getEngine_data().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //get months
        carAddBinding.etChangeOilTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthsTime=parent.getItemAtPosition(position).toString();
                //Toast.makeText(CarAddActivity.this, ""+monthsTime, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void setupToolbarConfig() {
        setSupportActionBar(carAddBinding.toolbar.titleToolbar);
        carAddBinding.toolbar.tvTitle.setText(getString(R.string.add_car));
        carAddBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigate();
            }
        });

        carAddBinding.toolbar.ivNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNotificationActivity();
            }
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_car_add;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {
        startActivity(new Intent(this,NotificationsActivity.class));
    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

        startActivity(new Intent(this,CarsActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,CarsActivity.class));
        finish();
    }
}
