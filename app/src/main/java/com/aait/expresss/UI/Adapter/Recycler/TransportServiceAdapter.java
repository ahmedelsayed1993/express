package com.aait.expresss.UI.Adapter.Recycler;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.aait.expresss.Models.TransferModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.RecycleTransportBinding;

import java.util.ArrayList;
import java.util.List;

public class TransportServiceAdapter extends RecyclerView.Adapter<TransportServiceAdapter.TransportServiceHolder> {
    private List<TransferModel.TransferData.Services> dataList = new ArrayList<>();
    private TransportListener transportListener;

    public TransportServiceAdapter(TransportListener transportListener) {
        this.transportListener = transportListener;
    }

    @Override
    public TransportServiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecycleTransportBinding transportBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.recycle_transport, parent, false );
        return new TransportServiceHolder(transportBinding);
    }

    @Override
    public void onBindViewHolder(final TransportServiceHolder holder, final int position) {
        holder.bind(dataList.get(position),position);
        holder.transportBinding.chbTransport.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    int value=Integer.parseInt(holder.transportBinding.etWheel2.getText().toString());
                    transportListener.onItemAdded(dataList.get(position),position,value);
                    holder.transportBinding.group2.setVisibility(View.VISIBLE);
                }else{
                    int value=Integer.parseInt(holder.transportBinding.etWheel2.getText().toString());
                    transportListener.onItemDeleted(dataList.get(position),position,value);
                    holder.transportBinding.group2.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<TransferModel.TransferData.Services> dataList) {
        this.dataList.addAll(dataList);

        notifyDataSetChanged();
    }

    class TransportServiceHolder extends RecyclerView.ViewHolder {
        RecycleTransportBinding transportBinding;
        TransportServiceHolder(RecycleTransportBinding  itemView) {
            super(itemView.getRoot());
            this.transportBinding=itemView;
        }

        public void bind(final TransferModel.TransferData.Services services, final int position) {
            transportBinding.chbTransport.setText(services.getName());


            transportBinding.tvAdd3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int value=Integer.parseInt(transportBinding.etWheel2.getText().toString());
                    value=value+1;
                    transportBinding.etWheel2.setText(String.valueOf(value));
                    transportListener.onItemIncrease(services,position,value);
                }
            });

            transportBinding.tvRemove3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int value=Integer.parseInt(transportBinding.etWheel2.getText().toString());
                    if (value!=1){
                        value=value-1;
                        transportBinding.etWheel2.setText(String.valueOf(value));
                        transportListener.onItemReduce(services,position,value);
                    }
                }
            });




        }
    }


   public interface TransportListener{

        void onItemAdded(TransferModel.TransferData.Services services,int position,int value);
        void onItemReduce(TransferModel.TransferData.Services services,int position,int value);
        void onItemIncrease(TransferModel.TransferData.Services services,int position,int value);
        void onItemDeleted(TransferModel.TransferData.Services services,int position,int value);
    }
}