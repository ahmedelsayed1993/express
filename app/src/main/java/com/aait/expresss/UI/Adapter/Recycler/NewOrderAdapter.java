package com.aait.expresss.UI.Adapter.Recycler;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.aait.expresss.Callbacks.NewOrderListener;
import com.aait.expresss.Models.NewOrderModel;
import com.aait.expresss.R;
import com.aait.expresss.databinding.RecycleNewOrderBinding;

import java.util.List;

public class NewOrderAdapter extends RecyclerView.Adapter<NewOrderAdapter.ViewHolder> {
    private List<NewOrderModel.NewOrderData> newOrderDataList;
    private Context context;
    private NewOrderListener newOrderListener;

    public NewOrderAdapter(List<NewOrderModel.NewOrderData> newOrderDataList, Context context, NewOrderListener newOrderListener) {
        this.newOrderDataList = newOrderDataList;
        this.context = context;
        this.newOrderListener = newOrderListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_new_order,parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final NewOrderModel.NewOrderData newOrderData=newOrderDataList.get(position);
        holder.newOrderBinding.tvCarModel.setText(newOrderData.getCar_brand_name()+"");
        holder.newOrderBinding.tvCarPlate.setText(newOrderData.getCar_plate_number()+"");
        holder.newOrderBinding.tvEngineType.setText(newOrderData.getCar_engine_name()+"");
        holder.newOrderBinding.textView25.setText(context.getString(R.string.car_number)+" "+newOrderData.getId());

        holder.newOrderBinding.chboxSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    newOrderListener.onNewOrderClickAdd(newOrderData.getId());
                }else{
                    newOrderListener.onNewOrderClickRemove(newOrderData.getId());
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return newOrderDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecycleNewOrderBinding newOrderBinding;
        public ViewHolder(View itemView) {
            super(itemView);
            newOrderBinding= DataBindingUtil.bind(itemView);
            assert newOrderBinding!=null;
            newOrderBinding.notifyChange();
        }
    }
}
