package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.Models.MainResponseModel;
import com.aait.expresss.Network.RetrofitCall;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.Utils.GlobalPreference;
import com.aait.expresss.Utils.Utils;
import com.aait.expresss.databinding.ActivityRateBinding;
import com.squareup.picasso.Picasso;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class RateActivity extends BaseActivity {

    ActivityRateBinding rateBinding;
    GlobalPreference globalPreference;
    SweetAlertDialog progressDialog;

    //Rec Var
    int orderId;
    boolean isDelegate;
    float noteRate;
    @Override
    protected void init(Bundle savedInstanceState) {
        rateBinding= DataBindingUtil.setContentView(this,getLayoutRes());
        globalPreference=GlobalPreference.getInstance(this);
        progressDialog=new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor(getString(R.string.colorDialog)));

        if (getIntent().getExtras()!=null){
            isDelegate = getIntent().getExtras().getBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE);
            orderId = getIntent().getExtras().getInt(AcceptRefuseOrderDialog.ORDER_ID);
        }

        setUpToolbar();

        fetchData();

        rateBinding.btnAddRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())){
                    addRateToOrder();
                }else{
                    Toasty.warning(getApplicationContext(),getString(R.string.check_internet_connection),
                            Toast.LENGTH_SHORT,true).show();
                }
            }
        });


    }

    private void addRateToOrder() {
        if (isDelegate){
            delegateRate();
        }else{
            clientRate();
        }
    }

    private void delegateRate() {
        if (Utils.checkError(rateBinding.etNote)){
            if (noteRate!=0){
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi()
                        .delegateRate(globalPreference.getUser().getData().getId(),
                                orderId,(int)noteRate,rateBinding.etNote.getText().toString(),
                                globalPreference.getAppLanguage())
                        .enqueue(new Callback<MainResponseModel>() {
                            @Override
                            public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                                if (response.body().getKey().equals("1")){
                                    progressDialog.dismissWithAnimation();
                                    Bundle bundle=new Bundle();
                                    bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,isDelegate);
                                    startActivity(new Intent(getApplicationContext(),SuccessRateActivity.class)
                                    .putExtras(bundle)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    finish();

                                }else{
                                    progressDialog.hide();
                                    Toasty.error(getApplicationContext(),response.body().getMassage(),
                                            Toast.LENGTH_SHORT,true).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<MainResponseModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });


            }else{
                Toasty.warning(getApplicationContext(),getString(R.string.add_rate_to_note),
                        Toast.LENGTH_SHORT,true).show();
            }
        }
    }

    private void clientRate() {
        if (Utils.checkError(rateBinding.etNote)){
            if (noteRate!=0){
                progressDialog.setContentText(getString(R.string.loading));
                progressDialog.setCancelable(false);
                progressDialog.show();
                RetrofitCall.getInstance().getApi()
                        .clientRate(globalPreference.getUser().getData().getId(),
                                orderId,(int)noteRate,rateBinding.etNote.getText().toString(),
                                globalPreference.getAppLanguage())
                        .enqueue(new Callback<MainResponseModel>() {
                            @Override
                            public void onResponse(Call<MainResponseModel> call, Response<MainResponseModel> response) {
                                if (response.body().getKey().equals("1")){
                                    progressDialog.dismissWithAnimation();
                                    Bundle bundle=new Bundle();
                                    bundle.putBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE,isDelegate);
                                    startActivity(new Intent(getApplicationContext(),SuccessRateActivity.class)
                                            .putExtras(bundle)
                                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    finish();

                                }else{
                                    progressDialog.hide();
                                    Toasty.error(getApplicationContext(),response.body().getMassage(),
                                            Toast.LENGTH_SHORT,true).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<MainResponseModel> call, Throwable t) {
                                Timber.wtf(t);
                            }
                        });


            }else{
                Toasty.warning(getApplicationContext(),getString(R.string.add_rate_to_note),
                        Toast.LENGTH_SHORT,true).show();
            }
        }
    }

    private void fetchData() {
        Picasso.get().load(globalPreference.getUser().getData().getAvatar_path())
                .placeholder(R.drawable.profile).into(rateBinding.ivUserImg);
        rateBinding.tvUserName.setText(globalPreference.getUser().getData().getName());

        rateBinding.rbNote.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                noteRate=rating;
            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(rateBinding.toolbar.titleToolbar);
        rateBinding.toolbar.tvTitle.setText(R.string.add_rate);
        rateBinding.toolbar.ivNotify.setVisibility(View.GONE);
        rateBinding.toolbar.ivBack.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_rate;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
