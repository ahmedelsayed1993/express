package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.UI.Fragment.AcceptRefuseOrderDialog;
import com.aait.expresss.databinding.ActivityDeleteOrderBinding;

public class DeleteOrderActivity extends BaseActivity {

        ActivityDeleteOrderBinding deleteOrderBinding;

        boolean isDelegate;

    @Override
    protected void init(Bundle savedInstanceState) {
        deleteOrderBinding= DataBindingUtil.setContentView(this,getLayoutRes());

        if (getIntent().getExtras()!=null){
            isDelegate=getIntent().getExtras().getBoolean(AcceptRefuseOrderDialog.DELEGATE_STATE);
        }

        Thread thread=new Thread(){

            @Override
            public void run() {
                try {
                    sleep(1000);

                    if (isDelegate) {
                        startActivity(new Intent(getApplicationContext(), MainDelegateActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }else{
                        startActivity(new Intent(getApplicationContext(), MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_delete_order;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
