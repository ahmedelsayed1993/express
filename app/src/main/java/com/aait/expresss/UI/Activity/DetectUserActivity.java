package com.aait.expresss.UI.Activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

import com.aait.expresss.Base.BaseActivity;
import com.aait.expresss.R;
import com.aait.expresss.databinding.ActivityDetectUserBinding;

public class DetectUserActivity extends BaseActivity {

    ActivityDetectUserBinding detectUserBinding;

    //GPS
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;

    //Rec Var
    public static final String USER_TYPE="user_type";


    @Override
    protected void init(Bundle savedInstanceState) {
        detectUserBinding= DataBindingUtil.setContentView(this,getLayoutRes());




        //Client = 0
            detectUserBinding.btnClient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle=new Bundle();
                    bundle.putInt(USER_TYPE,0);
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class)
                            .putExtras(bundle));
//                    Dexter.withActivity(DetectUserActivity.this)
//                            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION)
//                            .withListener(new MultiplePermissionsListener() {
//                                @Override
//                                public void onPermissionsChecked(MultiplePermissionsReport report) {
//                                    if (report.areAllPermissionsGranted()){
//
//
//                                        }
//
//
//                                    }else if (report.isAnyPermissionPermanentlyDenied()){
//                                        Utils.goToLocationPermissionSettings(DetectUserActivity.this);
//                                    }
//                                }
//
//                                @Override
//                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                                    token.continuePermissionRequest();
//                                }
//                            })
//                            .withErrorListener(new PermissionRequestErrorListener() {
//                                @Override
//                                public void onError(DexterError error) {
//                                    Timber.wtf(error.toString());
//                                }
//                            })
//                            .onSameThread()
//                            .check();

                }
            });

        //Delegate = 1
            detectUserBinding.btnDelegate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(USER_TYPE, 1);
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class)
                            .putExtras(bundle));
//                    Dexter.withActivity(DetectUserActivity.this)
//                            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION)
//                            .withListener(new MultiplePermissionsListener() {
//                                @Override
//                                public void onPermissionsChecked(MultiplePermissionsReport report) {
//                                    if (report.areAllPermissionsGranted()){
//
//
//                                        }
//
//
//                                    }
//
//                                @Override
//                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//
//                                }else if (report.isAnyPermissionPermanentlyDenied()){
//                                        Utils.goToLocationPermissionSettings(DetectUserActivity.this);
//                                    }
//                                }
//
//                                @Override
//                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                                    token.continuePermissionRequest();
//                                }
//                            })
//                            .withErrorListener(new PermissionRequestErrorListener() {
//                                @Override
//                                public void onError(DexterError error) {
//                                    Timber.wtf(error.toString());
//                                }
//                            })
//                            .onSameThread()
//                            .check();
                }
           });




    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_detect_user;
    }

    @Override
    protected boolean fullScreen() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }



    @Override
    protected void startNotificationActivity() {

    }

    @Override
    protected void startSearchActivity() {

    }

    @Override
    protected void navigate() {

    }
}
