package com.aait.expresss.Controler;

import android.app.Application;

import com.aait.expresss.BuildConfig;
import com.aait.expresss.Utils.ReleaseTree;

import timber.log.Timber;

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new ReleaseTree());
        }


    }

}
